#include "vutils.h"
#include <QFile>
#include <QTextStream>

QString VUtils::getFileSource(const QString &filename)
{
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly))
    {
        Q_ASSERT(false);
        return QString();
    }
    QTextStream in(&file);
    return in.readAll();
}
