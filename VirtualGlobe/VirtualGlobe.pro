#-------------------------------------------------
#
# Project created by QtCreator 2019-04-28T12:57:25
#
#-------------------------------------------------

QT       += core gui positioning

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VirtualGlobe
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
    Renderer/vglfactory.cpp \
    Renderer/Shaders/vshaderprogram.cpp \
    Core/Geometry/vmesh.cpp \
    Core/Geometry/VertexAttributes/vvertexattributecollection.cpp \
    vutils.cpp \
    Renderer/Shaders/vshadervertexattribute.cpp \
    Renderer/Scene/vcamera.cpp \
    Core/Geometry/vellipsoid.cpp \
    vmathutils.cpp \
    Renderer/Scene/vscenestate.cpp \
    vvirtualglobe.cpp \
    Renderer/DrawingContext/vdrawingcontext.cpp \
    Renderer/vclearstate.cpp \
    Renderer/vdrawstate.cpp \
    Renderer/Mesh/vmeshbuffers.cpp \
    Renderer/DrawingContext/Buffers/vindexbuffer.cpp \
    Renderer/VertexArray/vvertexbufferattribute.cpp \
    Renderer/Mesh/vmeshvertexbufferattributes.cpp \
    Renderer/vdevice.cpp \
    Core/Tessellation/vsubdivisionellipsoidtessellator.cpp \
    Core/Tessellation/vsubdivisionutility.cpp \
    Renderer/DrawingContext/Buffers/vbuffer.cpp \
    Renderer/DrawingContext/Buffers/vvertexbuffer.cpp \
    Renderer/VertexArray/vvertexarray.cpp \
    Renderer/VertexArray/vvertexbufferattributesimpl.cpp \
    Renderer/Textures/vtexture2d.cpp \
    Renderer/Textures/vtextureunits.cpp \
    Renderer/Shaders/viuniform.cpp \
    Renderer/Shaders/vuniformcollection.cpp \
    Renderer/VertexArray/vvertexarrayobject.cpp \
    Renderer/Scene/vcameralookatpoint.cpp

HEADERS += \
        vvirtualglobe.h \
    Renderer/vglfactory.h \
    Renderer/Shaders/vshaderprogram.h \
    Renderer/Shaders/vuniform.h \
    Core/Geometry/vmesh.h \
    vpointers.h \
    venmus.h \
    Core/Geometry/VertexAttributes/vvertexattribute.h \
    Core/Geometry/VertexAttributes/vvertexattributecollection.h \
    Core/Geometry/Indices/viindices.h \
    Renderer/VertexArray/vvertexlocations.h \
    Core/vtrig.h \
    vutils.h \
    Renderer/Shaders/vshadervertexattribute.h \
    Core/Vectors/vvector2.h \
    Core/Vectors/vvector3.h \
    Renderer/Scene/vcamera.h \
    Core/Matrices/vmatrix3.h \
    Core/Matrices/vmatrix4.h \
    Core/Geometry/vellipsoid.h \
    vmathutils.h \
    Renderer/Scene/vscenestate.h \
    vvirtualglobe.h \
    Renderer/DrawingContext/vdrawingcontext.h \
    Renderer/vclearstate.h \
    Renderer/vrenderstructs.h \
    Renderer/RenderState/vrenderstate.h \
    Renderer/DrawingContext/vtypeconvertergl.h \
    Renderer/vdrawstate.h \
    Renderer/vshadervertexattributecollection.h \
    Renderer/Mesh/vmeshbuffers.h \
    Renderer/DrawingContext/Buffers/vindexbuffer.h \
    Renderer/VertexArray/vvertexbufferattribute.h \
    Renderer/VertexArray/vvertexarraysizes.h \
    Renderer/VertexArray/vvertexbufferattributes.h \
    Renderer/Mesh/vmeshvertexbufferattributes.h \
    Renderer/vdevice.h \
    Core/Tessellation/vsubdivisionellipsoidtessellator.h \
    Core/Tessellation/vsubdivisionutility.h \
    Renderer/DrawingContext/Buffers/vbuffer.h \
    Renderer/DrawingContext/Buffers/vvertexbuffer.h \
    Renderer/VertexArray/vvertexarray.h \
    Renderer/VertexArray/vvertexbufferattributesimpl.h \
    Renderer/Textures/vtexture2d.h \
    Renderer/Textures/vtextureunits.h \
    Renderer/Shaders/viuniform.h \
    Renderer/Shaders/vuniformcollection.h \
    Renderer/Shaders/LinkAutomaticUniforms/vlinkautomaticuniforms.h \
    Renderer/Shaders/LinkAutomaticUniforms/vlinkautomaticuniform.h \
    Renderer/Shaders/DrawAutomaticUniforms/vdrawautomaticuniforms.h \
    Renderer/Shaders/DrawAutomaticUniforms/vdrawautomaticuniformfactory.h \
    Renderer/Shaders/DrawAutomaticUniforms/vdrawautomaticuniformfactorycollection.h \
    Renderer/VertexArray/vvertexarrayobject.h \
    Renderer/Scene/vcameralookatpoint.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Resources/res.qrc
