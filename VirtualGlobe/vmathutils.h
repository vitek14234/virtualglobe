#ifndef VMATHUTILS_H
#define VMATHUTILS_H

class VMathUtils
{
public:
    template <typename T>
    static int sign(T val)
    {
        return (T(0) < val) - (val < T(0));
    }
};

#endif // VMATHUTILS_H
