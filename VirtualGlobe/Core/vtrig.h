#ifndef VTRIG_H
#define VTRIG_H

#include <QtMath>

class VTrig
{
public:
    Q_DECL_CONSTEXPR static double OneOverPi = 1.0 / M_PI;
    Q_DECL_CONSTEXPR static double PiOverTwo = M_PI * 0.5;
    Q_DECL_CONSTEXPR static double PiOverThree = M_PI / 3.0;
    Q_DECL_CONSTEXPR static double PiOverFour = M_PI / 4.0;
    Q_DECL_CONSTEXPR static double PiOverSix = M_PI / 6.0;
    Q_DECL_CONSTEXPR static double ThreePiOver2 = (3.0 * M_PI) * 0.5;
    Q_DECL_CONSTEXPR static double TwoPi = 2.0 * M_PI;
    Q_DECL_CONSTEXPR static double OneOverTwoPi = 1.0 / (2.0 * M_PI);
    Q_DECL_CONSTEXPR static double RadiansPerDegree = M_PI / 180.0;
};

#endif // VTRIG_H
