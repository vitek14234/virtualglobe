#ifndef VVECTOR2_H
#define VVECTOR2_H

#include <typeinfo>
#include <QtCore/qcompilerdetection.h>
#include <QPointF>
#include <QVector2D>
#include <QDebug>
#include <QDataStream>
#include <qmath.h>

template<typename T>
class VVector2
{
public:
    Q_DECL_CONSTEXPR VVector2()
        : VVector2(0, 0)
    {

    }

    Q_DECL_CONSTEXPR VVector2(T x, T y)
        : data{x, y}
    {

    }

    Q_DECL_CONSTEXPR explicit VVector2(const QPointF& point)
        : data{point.x(), point.y()}
    {

    }

    Q_DECL_CONSTEXPR explicit VVector2(const QVector2D vector)
        : data{vector.x(), vector.y()}
    {

    }

    bool isNull() const
    {
        return qIsNull(data[0]) && qIsNull(data[1]);
    }

    Q_DECL_CONSTEXPR T x() const
    {
        return data[0];
    }

    Q_DECL_CONSTEXPR T y() const
    {
        return data[1];
    }

    void setX(T x)
    {
        data[0] = x;
    }

    void setY(T y)
    {
        data[1] = y;
    }

    T &operator[](int i)
    {
        Q_ASSERT(uint(i) < 2u);
        return data[i];
    }

    T &operator[](int i) const
    {
        Q_ASSERT(uint(i) < 2u);
        return data[i];
    }

    Q_DECL_CONSTEXPR T lengthSquared() const
    {
        return data[0] * data[0] + data[1] * data[1];
    }

    T length() const
    {
        return sqrt(lengthSquared());
    }

    VVector2<T> normalized() const
    {
        VVector2<T> normalized = *this;
        normalized.normalize();
        return normalized;
    }

    void normalize()
    {
        T len = length();
        if (qFuzzyIsNull(len - 1.0) || qFuzzyIsNull(len))
            return;
        len = std::sqrt(len);
        data[0] = data[0] / len;
        data[1] = data[1] / len;
    }

    Q_DECL_CONSTEXPR T dotProduct(const VVector2<T> &rhs) const
    {
        return data[0] * rhs.data[0] + data[1] * rhs.data[1];
    }

    T distanceToVector(const VVector2<T> &vector) const
    {
        return (*this - vector).length();
    }

    VVector2<T> &operator+=(const VVector2<T> &rhs)
    {
        data[0] += rhs.data[0];
        data[1] += rhs.data[1];
    }

    VVector2<T> &operator-=(const VVector2<T> &rhs)
    {
        data[0] -= rhs.data[0];
        data[1] -= rhs.data[1];
    }

    VVector2<T> &operator*=(T factor)
    {
        data[0] *= factor;
        data[1] *= factor;
    }

    VVector2<T> &operator*=(const VVector2<T> &rhs)
    {
        data[0] *= rhs.data[0];
        data[1] *= rhs.data[1];
    }

    VVector2<T> &operator/=(T divisor)
    {
        data[0] /= divisor;
        data[1] /= divisor;
    }

    VVector2<T> &operator/=(const VVector2<T> &rhs)
    {
        data[0] /= rhs.data[0];
        data[1] /= rhs.data[1];
    }

    Q_DECL_CONSTEXPR friend inline bool operator==(const VVector2<T> &v1, const VVector2<T> &v2)
    {
        return v1.data[0] == v2.data[0] && v1.data[1] == v2.data[1];
    }

    Q_DECL_CONSTEXPR friend inline bool operator!=(const VVector2<T> &v1, const VVector2<T> &v2)
    {
        return v1.data[0] != v2.data[0] && v1.data[1] != v2.data[1];
    }

    Q_DECL_CONSTEXPR friend inline const VVector2<T> operator+(const VVector2<T> &v1, const VVector2<T> &v2)
    {
        return VVector2<T>(v1.data[0] + v2.data[0], v1.data[1] + v2.data[1]);
    }

    Q_DECL_CONSTEXPR friend inline const VVector2<T> operator-(const VVector2<T> &v1, const VVector2<T> &v2)
    {
        return VVector2<T>(v1.data[0] - v2.data[0], v1.data[1] - v2.data[1]);
    }

    Q_DECL_CONSTEXPR friend inline const VVector2<T> operator*(T factor, const VVector2<T> &vector)
    {
        return VVector2<T>(vector.data[0] * factor, vector.data[1] * factor);
    }

    Q_DECL_CONSTEXPR friend inline const VVector2<T> operator*(const VVector2<T> &vector, T factor)
    {
        return VVector2<T>(vector.data[0] * factor, vector.data[1] * factor);
    }

    Q_DECL_CONSTEXPR friend inline const VVector2<T> operator*(const VVector2<T> &v1, const VVector2<T> &v2)
    {
        return VVector2<T>(v1.data[0] * v2.data[0], v1.data[1] * v2.data[1]);
    }

    Q_DECL_CONSTEXPR friend inline const VVector2<T> operator-(const VVector2<T> &vector)
    {
        return VVector2<T>(-vector.data[0], -vector.data[1]);
    }

    Q_DECL_CONSTEXPR friend inline const VVector2<T> operator/(const VVector2<T> &vector, T divisor)
    {
        return VVector2<T>(vector.data[0] / divisor, vector.data[1] / divisor);
    }

    Q_DECL_CONSTEXPR friend inline const VVector2<T> operator/(const VVector2<T> &vector, const VVector2<T> &divisor)
    {
        return VVector2<T>(vector.data[0] / divisor.data[0], vector.data[1] / divisor.data[1]);
    }

    Q_DECL_CONSTEXPR friend inline bool qFuzzyCompare(const VVector2<T>& v1, const VVector2<T>& v2)
    {
        return qFuzzyCompare(v1.data[0], v2.data[0]) &&
               qFuzzyCompare(v1.data[1], v2.data[1]);
    }

    Q_DECL_CONSTEXPR QPointF toQPointF() const
    {
        return QPointF(data[0], data[1]);
    }

    Q_DECL_CONSTEXPR QVector2D toQVector2D() const
    {
        return QVector2D(data[0], data[1]);
    }
private:
    T data[2];
};

template<typename T>
QDebug operator<<(QDebug dbg, const VVector2<T> &vector)
{
    dbg.nospace() << "VVector2<" << typeid(T).name() << ">(" << vector.x() << ", " << vector.y() << ')';
    return dbg;
}

template<typename T>
QDataStream &operator<<(QDataStream &stream, const VVector2<T> &vector)
{
    stream << vector.x() << vector.y();
    return stream;
}

template<typename T>
QDataStream &operator>>(QDataStream &stream, VVector2<T> &vector)
{
    T x;
    T y;
    stream >> x >> y;
    vector.setX(x);
    vector.setY(y);
    return stream;
}

using VVector2I = VVector2<int>;
using VVector2F = VVector2<float>;
using VVector2D = VVector2<double>;

#endif // VVECTOR2_H
