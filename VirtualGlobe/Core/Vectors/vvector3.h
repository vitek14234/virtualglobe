#ifndef VVECTOR3_H
#define VVECTOR3_H

#include <typeinfo>
#include <QtCore/qcompilerdetection.h>
#include <QVector3D>
#include <QDebug>
#include <QDataStream>
#include "vvector2.h"

template<typename T>
class VVector3
{
public:
    Q_DECL_CONSTEXPR VVector3()
        : VVector3(0, 0, 0)
    {

    }

    Q_DECL_CONSTEXPR VVector3(T x, T y, T z)
        : m_data{x, y, z}
    {

    }

    Q_DECL_CONSTEXPR explicit VVector3(const QPointF& point)
        : m_data{point.x(), point.y()}
    {

    }

    Q_DECL_CONSTEXPR explicit VVector3(const QVector3D& vector)
        : m_data{vector.x(), vector.y(), vector.z()}
    {

    }

    template<typename T2>
    Q_DECL_CONSTEXPR VVector3(const T2& vector)
        : m_data{T(vector.x()), T(vector.y()), T(vector.z())}
    {

    }

    static Q_DECL_CONSTEXPR VVector3<T> zero()
    {
        return VVector3<T> ((T)0, (T)0, (T)0);
    }

    static Q_DECL_CONSTEXPR VVector3<T> unitX()
    {
        return VVector3<T> ((T)1, (T)0, (T)0);
    }

    static Q_DECL_CONSTEXPR VVector3<T> unitY()
    {
        return VVector3<T> ((T)0, (T)1, (T)0);
    }

    static Q_DECL_CONSTEXPR VVector3<T> unitZ()
    {
        return VVector3<T> ((T)0, (T)0, (T)1);
    }

    bool isNull() const
    {
        return qIsNull(m_data[0]) && qIsNull(m_data[1]) && qIsNull(m_data[2]);
    }

    Q_DECL_CONSTEXPR T x() const
    {
        return m_data[0];
    }

    Q_DECL_CONSTEXPR T y() const
    {
        return m_data[1];
    }

    Q_DECL_CONSTEXPR T z() const
    {
        return m_data[2];
    }

    void setX(T x)
    {
        m_data[0] = x;
    }

    void setY(T y)
    {
        m_data[1] = y;
    }

    void setZ(T z)
    {
        m_data[2] = z;
    }

    T &operator[](int i)
    {
        Q_ASSERT(uint(i) < 3u);
        return m_data[i];
    }

    T &operator[](int i) const
    {
        Q_ASSERT(uint(i) < 3u);
        return m_data[i];
    }

    Q_DECL_CONSTEXPR T lengthSquared() const
    {
        return m_data[0] * m_data[0] + m_data[1] * m_data[1] + m_data[2] * m_data[2];
    }

    T length() const
    {
        return sqrt(lengthSquared());
    }

    VVector3<T> normalized() const
    {
        VVector3<T> normalized = *this;
        normalized.normalize();
        return normalized;
    }

    void normalize()
    {
        T len = length();
        if (qFuzzyIsNull(len - 1.0) || qFuzzyIsNull(len))
            return;
        m_data[0] = m_data[0] / len;
        m_data[1] = m_data[1] / len;
        m_data[2] = m_data[2] / len;
    }

    Q_DECL_CONSTEXPR bool isUndefined() const
    {
        return !(qIsFinite(m_data[0]) && qIsFinite(m_data[1]) && qIsFinite(m_data[2]));
    }

    double angleBetween(const VVector3<T> &other) const
    {
        return acos(normalized().dotProduct(other.normalized()));
    }

    VVector3<T> rotateAroundAxis(const VVector3<T> &axis, double theta) const
    {
        const double u = axis.x();
        const double v = axis.y();
        const double w = axis.z();

        double cosTheta = cos(theta);
        double sinTheta = sin(theta);

        double ms = axis.lengthSquared();
        double m = sqrt(ms);

        return VVector3<T>(
                ((u * (u * m_data[0] + v * m_data[1] + w * m_data[2])) +
                (((m_data[0] * (v * v + w * w)) - (u * (v * m_data[1] + w * m_data[2]))) * cosTheta) +
                (m * ((-w * m_data[1]) + (v * m_data[2])) * sinTheta)) / ms,

                ((v * (u * m_data[0] + v * m_data[1] + w * m_data[2])) +
                (((m_data[1] * (u * u + w * w)) - (v * (u * m_data[0] + w * m_data[2]))) * cosTheta) +
                (m * ((w * m_data[0]) - (u * m_data[2])) * sinTheta)) / ms,

                ((w * (u * m_data[0] + v * m_data[1] + w * m_data[2])) +
                (((m_data[2] * (u * u + v * v)) - (w * (u * m_data[0] + v * m_data[1]))) * cosTheta) +
                (m * (-(v * m_data[0]) + (u * m_data[1])) * sinTheta)) / ms);
    }

    Q_DECL_CONSTEXPR T dotProduct(const VVector3<T> &rhs) const
    {
        return m_data[0] * rhs.m_data[0]
             + m_data[1] * rhs.m_data[1]
             + m_data[2] * rhs.m_data[2];
    }

    Q_DECL_CONSTEXPR const VVector3<T> crossProduct(const VVector3<T> &rhs) const
    {
        return VVector3<T>(
                    m_data[1] * rhs.m_data[2] - m_data[2] * rhs.m_data[1],
                    m_data[2] * rhs.m_data[0] - m_data[0] * rhs.m_data[2],
                    m_data[0] * rhs.m_data[1] - m_data[1] * rhs.m_data[0]);
    }

    Q_DECL_CONSTEXPR const VVector2<T> toVector2() const
    {
        return VVector2<T>(m_data[0], m_data[1]);
    }

    T distanceToVector(const VVector3<T> &vector) const
    {
        return (*this - vector).length();
    }

    VVector3<T> &operator+=(const VVector3<T> &rhs)
    {
        m_data[0] += rhs.m_data[0];
        m_data[1] += rhs.m_data[1];
        m_data[2] += rhs.m_data[2];
    }

    VVector3<T> &operator-=(const VVector3<T> &rhs)
    {
        m_data[0] -= rhs.m_data[0];
        m_data[1] -= rhs.m_data[1];
        m_data[2] -= rhs.m_data[2];
    }

    VVector3<T> &operator*=(T factor)
    {
        m_data[0] *= factor;
        m_data[1] *= factor;
        m_data[2] *= factor;
    }

    VVector3<T> &operator*=(const VVector3<T> &rhs)
    {
        m_data[0] *= rhs.m_data[0];
        m_data[1] *= rhs.m_data[1];
        m_data[2] *= rhs.m_data[2];
    }

    VVector3<T> &operator/=(T divisor)
    {
        m_data[0] /= divisor;
        m_data[1] /= divisor;
        m_data[2] /= divisor;
    }

    VVector3<T> &operator/=(const VVector3<T> &rhs)
    {
        m_data[0] /= rhs.m_data[0];
        m_data[1] /= rhs.m_data[1];
        m_data[2] /= rhs.m_data[2];
    }

    Q_DECL_CONSTEXPR friend inline bool operator==(const VVector3<T> &v1, const VVector3<T> &v2)
    {
        return v1.m_data[0] == v2.m_data[0]
            && v1.m_data[1] == v2.m_data[1]
            && v1.m_data[2] == v2.m_data[2];
    }

    Q_DECL_CONSTEXPR friend inline bool operator!=(const VVector3<T> &v1, const VVector3<T> &v2)
    {
        return v1.m_data[0] != v2.m_data[0]
            && v1.m_data[1] != v2.m_data[1]
            && v1.m_data[2] != v2.m_data[2];
    }

    Q_DECL_CONSTEXPR friend inline const VVector3<T> operator+(const VVector3<T> &v1, const VVector3<T> &v2)
    {
        return VVector3<T>(v1.m_data[0] + v2.m_data[0], v1.m_data[1] + v2.m_data[1], v1.m_data[2] + v2.m_data[2]);
    }

    Q_DECL_CONSTEXPR friend inline const VVector3<T> operator-(const VVector3<T> &v1, const VVector3<T> &v2)
    {
        return VVector3<T>(v1.m_data[0] - v2.m_data[0], v1.m_data[1] - v2.m_data[1], v1.m_data[2] - v2.m_data[2]);
    }

    Q_DECL_CONSTEXPR friend inline const VVector3<T> operator*(T factor, const VVector3<T> &vector)
    {
        return VVector3<T>(vector.m_data[0] * factor, vector.m_data[1] * factor, vector.m_data[2] * factor);
    }

    Q_DECL_CONSTEXPR friend inline const VVector3<T> operator*(const VVector3<T> &vector, T factor)
    {
        return VVector3<T>(vector.m_data[0] * factor, vector.m_data[1] * factor, vector.m_data[2] * factor);
    }

    Q_DECL_CONSTEXPR friend inline const VVector3<T> operator*(const VVector3<T> &v1, const VVector3<T> &v2)
    {
        return VVector3<T>(v1.m_data[0] * v2.m_data[0], v1.m_data[1] * v2.m_data[1], v1.m_data[2] * v2.m_data[2]);
    }

    Q_DECL_CONSTEXPR friend inline const VVector3<T> operator-(const VVector3<T> &vector)
    {
        return VVector3<T>(-vector.m_data[0], -vector.m_data[1], -vector.m_data[2]);
    }

    Q_DECL_CONSTEXPR friend inline const VVector3<T> operator/(const VVector3<T> &vector, T divisor)
    {
        return VVector3<T>(vector.m_data[0] / divisor, vector.m_data[1] / divisor, vector.m_data[2] / divisor);
    }

    Q_DECL_CONSTEXPR friend inline const VVector3<T> operator/(const VVector3<T> &vector, const VVector3<T> &divisor)
    {
        return VVector3<T>(vector.m_data[0] / divisor.m_data[0], vector.m_data[1] / divisor.m_data[1], vector.m_data[2] / divisor.m_data[2]);
    }

    Q_DECL_CONSTEXPR friend inline bool qFuzzyCompare(const VVector3<T>& v1, const VVector3<T>& v2)
    {
        return qFuzzyCompare(v1.m_data[0], v2.m_data[0])
            && qFuzzyCompare(v1.m_data[1], v2.m_data[1])
            && qFuzzyCompare(v1.m_data[2], v2.m_data[2]);
    }

    Q_DECL_CONSTEXPR QVector3D toQVector3D() const
    {
        return QVector3D(m_data[0], m_data[1], m_data[2]);
    }

    const T* data() const
    {
        return m_data;
    }

    T* data()
    {
        return m_data;
    }
private:
    T m_data[3];
};

template<typename T>
QDebug operator<<(QDebug dbg, const VVector3<T> &vector)
{
    dbg.nospace() << "VVector3<" << typeid(T).name() << ">(" << vector.x() << ", " << vector.y() << ", " << vector.z() << ')';
    return dbg;
}

template<typename T>
QDataStream &operator<<(QDataStream &stream, const VVector3<T> &vector)
{
    stream << vector.x() << vector.y() << vector.z();
    return stream;
}

template<typename T>
QDataStream &operator>>(QDataStream &stream, VVector3<T> &vector)
{
    T x;
    T y;
    T z;
    stream >> x >> y >> z;
    vector.setX(x);
    vector.setY(y);
    vector.setZ(z);
    return stream;
}

using VVector3I = VVector3<int>;
using VVector3F = VVector3<float>;
using VVector3D = VVector3<double>;

#endif // VVECTOR3_H
