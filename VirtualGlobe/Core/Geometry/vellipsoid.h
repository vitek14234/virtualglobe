#ifndef VELLIPSOID_H
#define VELLIPSOID_H

#include "Core/Vectors/vvector3.h"
#include <QGeoCoordinate>

class VEllipsoid
{
public:
    /*Q_DECL_CONSTEXPR*/ VEllipsoid(double x, double y, double z)
        : VEllipsoid(VVector3D(x, y, z))
    {

    }

    /*Q_DECL_CONSTEXPR*/ VEllipsoid(const VVector3D &radii)
        : m_radii(radii)
        , m_radiiSquared(radii.x() * radii.x(), radii.y() * radii.y(), radii.z() * radii.z())
    {
        m_radiiToTheFourth = m_radiiSquared * m_radiiSquared;
        m_oneOverRadiiSquared = VVector3D(1.0 / m_radiiSquared.x(),
                                          1.0 / m_radiiSquared.y(),
                                          1.0 / m_radiiSquared.z());
    }

    VVector3D centricSurfaceNormal(const VVector3D &positionOnVEllipsoid) const;
    VVector3D geodeticSurfaceNormal(const VVector3D &positionOnVEllipsoid) const;
    VVector3D geodeticSurfaceNormal(const QGeoCoordinate &geodetic) const;

    const VVector3D &radii() const;
    const VVector3D &radiiSquared() const;
    const VVector3D &oneOverRadiiSquared() const;

    double minimumRadius() const;
    double maximumRadius() const;

    QVector<double> intersections(const VVector3D &origin, const VVector3D &direction) const;

    VVector3D toVector3D(const QGeoCoordinate &geodetic) const;
    QVector<QGeoCoordinate> toGeodetic3D(const QVector<VVector3D> &positions) const;
    QGeoCoordinate toGeodetic3D(const VVector3D &position) const;
    QVector<QGeoCoordinate> toGeodetic2D(const QVector<VVector3D> &positions) const;
    QGeoCoordinate toGeodetic2D(const VVector3D &positionOnVEllipsoid) const;
    VVector3D scaleToGeodeticSurface(const VVector3D &position) const;
    VVector3D scaleToGeocentricSurface(const VVector3D &position) const;
    QVector<VVector3D> computeCurve(
        const VVector3D &start,
        const VVector3D &stop,
        double granularity) const;

    static /*Q_DECL_CONSTEXPR*/ VEllipsoid wgs84()
    {
        return VEllipsoid(6378137.0, 6378137.0, 6356752.314245);
    }
    static /*Q_DECL_CONSTEXPR*/ VEllipsoid scaledWgs84()
    {
        return VEllipsoid(1.0, 1.0, 6356752.314245 / 6378137.0);
    }
private:
    VVector3D m_radii;
    VVector3D m_radiiSquared;
    VVector3D m_radiiToTheFourth;
    VVector3D m_oneOverRadiiSquared;
};

#endif // VELLIPSOID_H
