#ifndef VIINDICES_H
#define VIINDICES_H

#include <array>
#include <QVector>
#include "vpointers.h"
#include "venmus.h"

class VIIndices
{
public:
    virtual VIndicesType datatype() const = 0;
};

template <class T>
class TriangleIndices
{
public:
    TriangleIndices(T ui0, T ui1, T ui2)
    {
        m_indicies[0] = ui0;
        m_indicies[1] = ui1;
        m_indicies[2] = ui2;
    }

    TriangleIndices(const TriangleIndices& rhs)
    {
        m_indicies = rhs.m_indicies;
    }

    friend inline bool operator==(const TriangleIndices<T> &lhs, const TriangleIndices<T> &rhs)
    {
        return lhs.m_indicies == rhs.m_indicies;
    }

    friend inline bool operator!=(const TriangleIndices<T> &lhs, const TriangleIndices<T> &rhs)
    {
        return lhs.m_indicies != rhs.m_indicies;
    }

    T I0() const
    {
        return m_indicies[0];
    }

    T I1() const
    {
        return m_indicies[1];
    }

    T I2() const
    {
        return m_indicies[2];
    }

    const T* data() const
    {
        return m_indicies.data();
    }

    T* data()
    {
        return m_indicies.data();
    }
private:
    std::array<T, 3> m_indicies;
};

template <VIndicesType TypeName, class DataType>
class VIndices : public VIIndices
{
public:
    using this_type = VIndices<TypeName, DataType>;
    using container = QVector<DataType>;

    VIndices(size_t capacity)
    {
        m_values.reserve(capacity);
    }

    static QSharedPointer<this_type> instance(size_t capacity = 0)
    {
        return QSharedPointer<this_type>::create(capacity);
    }

    const container& values() const
    {
        return m_values;
    }

    container& values()
    {
        return m_values;
    }

    void addTriangle(const TriangleIndices<DataType>& triangle)
    {
        m_values.reserve(m_values.size() + 3);
        m_values.push_back(triangle.I0());
        m_values.push_back(triangle.I1());
        m_values.push_back(triangle.I2());
    }

    virtual VIndicesType datatype() const Q_DECL_OVERRIDE
    {
        return (VIndicesType)TypeName;
    }
private:
    container m_values;
};

using VIndicesUnsignedShort = VIndices<VIndicesType::UnsignedShort, unsigned short>;
using VIndicesUnsignedShortPtr = QSharedPointer<VIndicesUnsignedShort>;

using VIndicesUnsignedInt = VIndices<VIndicesType::UnsignedInt, unsigned int>;
using VIndicesUnsignedIntPtr = QSharedPointer<VIndicesUnsignedInt>;
using VTriangleIndicesUnsignedInt = TriangleIndices<unsigned int>;

#endif // VIINDICES_H
