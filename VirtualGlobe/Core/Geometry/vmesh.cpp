#include "vmesh.h"
#include "VertexAttributes/vvertexattributecollection.h"

VMeshPtr VMesh::instance()
{
    return QSharedPointer<VMesh>::create();
}

VMesh::VMesh()
    : m_primitiveType(VPrimitiveType::Points)
    , m_frontFaceWindingOrder(VWindingOrder::Clockwise)
{
    m_attributes = VVertexAttributeCollection::instance();
}

void VMesh::setIndices(const VIIndicesPtr &indices)
{
    Q_ASSERT(indices);
    m_indices = indices;
}

const VVertexAttributeCollectionPtr &VMesh::attributes() const
{
    return m_attributes;
}

const VIIndicesPtr &VMesh::indices() const
{
    return m_indices;
}

void VMesh::setPrimitiveType(VPrimitiveType type)
{
    m_primitiveType = type;
}

VPrimitiveType VMesh::primitiveType() const
{
    return m_primitiveType;
}

void VMesh::setFrontFaceWindingOrder(VWindingOrder order)
{
    m_frontFaceWindingOrder = order;
}

VWindingOrder VMesh::frontFaceWindingOrder() const
{
    return m_frontFaceWindingOrder;
}
