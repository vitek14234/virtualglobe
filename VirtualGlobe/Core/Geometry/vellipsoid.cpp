#include "vellipsoid.h"
#include "vmathutils.h"
#include <qmath.h>

VVector3D VEllipsoid::centricSurfaceNormal(const VVector3D &positionOnEllipsoid) const
{
    return positionOnEllipsoid.normalized();
}

VVector3D VEllipsoid::geodeticSurfaceNormal(const VVector3D &positionOnEllipsoid) const
{
    return (positionOnEllipsoid * m_oneOverRadiiSquared).normalized();
}

VVector3D VEllipsoid::geodeticSurfaceNormal(const QGeoCoordinate &geodetic) const
{
    const double cosLatitude = cos(qDegreesToRadians(geodetic.latitude()));
    return VVector3D(
                cosLatitude * cos(qDegreesToRadians(geodetic.longitude())),
                cosLatitude * sin(qDegreesToRadians(geodetic.longitude())),
                sin(qDegreesToRadians(geodetic.latitude())));
}

const VVector3D &VEllipsoid::radii() const
{
    return m_radii;
}

const VVector3D &VEllipsoid::radiiSquared() const
{
    return m_radiiSquared;
}

const VVector3D &VEllipsoid::oneOverRadiiSquared() const
{
    return m_oneOverRadiiSquared;
}

double VEllipsoid::minimumRadius() const
{
    return std::min(m_radii.x(), std::min(m_radii.y(), m_radii.z()));
}

double VEllipsoid::maximumRadius() const
{
    return std::max(m_radii.x(), std::max(m_radii.y(), m_radii.z()));
}

QVector<double> VEllipsoid::intersections(const VVector3D &origin, const VVector3D &direction) const
{
    const VVector3D normalizedDirection = direction.normalized();

    // By laborious algebraic manipulation....
    const double a = normalizedDirection.x() * normalizedDirection.x() * m_oneOverRadiiSquared.x() +
            normalizedDirection.y() * normalizedDirection.y() * m_oneOverRadiiSquared.y() +
            normalizedDirection.z() * normalizedDirection.z() * m_oneOverRadiiSquared.z();
    const double b = 2.0 *
            (origin.x() * normalizedDirection.x() * m_oneOverRadiiSquared.x() +
             origin.y() * normalizedDirection.y() * m_oneOverRadiiSquared.y() +
             origin.z() * normalizedDirection.z() * m_oneOverRadiiSquared.x());
    const double c = origin.x() * origin.x() * m_oneOverRadiiSquared.x() +
            origin.y() * origin.y() * m_oneOverRadiiSquared.y() +
            origin.z() * origin.z() * m_oneOverRadiiSquared.z() - 1.0;

    // Solve the quadratic equation: ax^2 + bx + c = 0.
    // Algorithm is from Wikipedia's "Quadratic equation" topic, and Wikipedia credits
    // Numerical Recipes in C, section 5.6: "Quadratic and Cubic Equations"
    const double discriminant = b * b - 4 * a * c;
    if (discriminant < 0.0)
    {
        // no intersections
        return QVector<double>();
    }
    else if (discriminant == 0.0)
    {
        // one intersection at a tangent point
        return QVector<double>() << ( -0.5 * b / a );
    }

    const double t = -0.5 * (b + (b > 0.0 ? 1.0 : -1.0) * sqrt(discriminant));
    const double root1 = t / a;
    const double root2 = c / t;

    // Two intersections - return the smallest first.
    if (root1 < root2)
    {
        return QVector<double>() << root1 << root2;
    }
    else
    {
        return QVector<double>() << root2 << root1;
    }
}

VVector3D VEllipsoid::toVector3D(const QGeoCoordinate &geodetic) const
{
    const VVector3D n = geodeticSurfaceNormal(geodetic);
    const VVector3D k = m_radiiSquared * n;
    const double gamma = sqrt(k.dotProduct(n));
    const VVector3D rSurface = k / gamma;
    return rSurface + (geodetic.altitude() * n);
}

QVector<QGeoCoordinate> VEllipsoid::toGeodetic3D(const QVector<VVector3D> &positions) const
{
    QVector<QGeoCoordinate> geodetics;
    geodetics.resize(positions.size());
    for (const VVector3D &position : positions)
    {
        geodetics.push_back(toGeodetic3D(position));
    }

    return geodetics;
}

QGeoCoordinate VEllipsoid::toGeodetic3D(const VVector3D &position) const
{
    const VVector3D p = scaleToGeodeticSurface(position);
    const VVector3D h = position - p;
    const double height = VMathUtils::sign(h.dotProduct(position)) * h.length();
    QGeoCoordinate geodetic2D = toGeodetic2D(p);
    geodetic2D.setAltitude(height);
    return geodetic2D;
}

QVector<QGeoCoordinate> VEllipsoid::toGeodetic2D(const QVector<VVector3D> &positions) const
{
    QVector<QGeoCoordinate> geodetics;
    geodetics.resize(positions.size());
    for (const VVector3D &position : positions)
    {
        geodetics.push_back(toGeodetic2D(position));
    }

    return geodetics;
}

QGeoCoordinate VEllipsoid::toGeodetic2D(const VVector3D &positionOnVEllipsoid) const
{
    const VVector3D n = geodeticSurfaceNormal(positionOnVEllipsoid);
    return QGeoCoordinate(qRadiansToDegrees(atan2(n.y(), n.x())),
                          qRadiansToDegrees(asin(n.z() / n.length())));
}

VVector3D VEllipsoid::scaleToGeodeticSurface(const VVector3D &position) const
{
    const double beta = 1.0 / sqrt(
                (position.x() * position.x()) * m_oneOverRadiiSquared.x() +
                (position.y() * position.y()) * m_oneOverRadiiSquared.y() +
                (position.z() * position.z()) * m_oneOverRadiiSquared.z());

    const double n = (beta * position * m_oneOverRadiiSquared).length();
    double alpha = (1.0 - beta) * (position.length() / n);

    const double x2 = position.x() * position.x();
    const double y2 = position.y() * position.y();
    const double z2 = position.z() * position.z();

    double da = 0.0;
    double db = 0.0;
    double dc = 0.0;

    double s = 0.0;
    double dSdA = 1.0;

    do
    {
        alpha -= (s / dSdA);

        da = 1.0 + (alpha * m_oneOverRadiiSquared.x());
        db = 1.0 + (alpha * m_oneOverRadiiSquared.y());
        dc = 1.0 + (alpha * m_oneOverRadiiSquared.z());

        const double da2 = da * da;
        const double db2 = db * db;
        const double dc2 = dc * dc;

        const double da3 = da * da2;
        const double db3 = db * db2;
        const double dc3 = dc * dc2;

        s = x2 / (m_radiiSquared.x() * da2) +
                y2 / (m_radiiSquared.y() * db2) +
                z2 / (m_radiiSquared.z() * dc2) - 1.0;

        dSdA = -2.0 *
                (x2 / (m_radiiToTheFourth.x() * da3) +
                 y2 / (m_radiiToTheFourth.y() * db3) +
                 z2 / (m_radiiToTheFourth.z() * dc3));
    }
    while (fabs(s) > 1e-10);

    return  VVector3D(position.x() / da, position.y() / db, position.z() / dc);
}

VVector3D VEllipsoid::scaleToGeocentricSurface(const VVector3D &position) const
{
    const double beta = 1.0 / sqrt((position * position).dotProduct(m_oneOverRadiiSquared));
    return beta * position;
}

QVector<VVector3D> VEllipsoid::computeCurve(const VVector3D &start, const VVector3D &stop, double granularity) const
{
    const VVector3D normal = start.crossProduct(stop).normalized();
    double theta = start.angleBetween(stop);
    const int n = std::max((int)(theta / granularity) - 1, 0);

    QVector<VVector3D> positions;//(2 + n);
    positions.push_back(start);

    for (int i = 1; i <= n; ++i)
    {
        const double phi = (i * granularity);
        positions.push_back(scaleToGeocentricSurface(start.rotateAroundAxis(normal, phi)));
    }

    positions.push_back(stop);

    return positions;
}
