#ifndef VMESH_H
#define VMESH_H

#include "vpointers.h"
#include "venmus.h"

class VMesh
{
public:
    static VMeshPtr instance();

    VMesh();

    const VVertexAttributeCollectionPtr &attributes() const;

    void setIndices(const VIIndicesPtr& indices);
    const VIIndicesPtr &indices() const;

    void setPrimitiveType(VPrimitiveType type);
    VPrimitiveType primitiveType() const;

    void setFrontFaceWindingOrder(VWindingOrder order);
    VWindingOrder frontFaceWindingOrder() const;
private:
    VVertexAttributeCollectionPtr m_attributes;
    VIIndicesPtr m_indices;
    VPrimitiveType m_primitiveType;
    VWindingOrder m_frontFaceWindingOrder;
};

#endif // VMESH_H
