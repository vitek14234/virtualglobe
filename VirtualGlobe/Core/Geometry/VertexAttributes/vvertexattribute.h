#ifndef VVERTEXATTRIBUTE_H
#define VVERTEXATTRIBUTE_H

#include <QVector>
#include <QVector3D>
#include <QString>
#include "Core/Vectors/vvector3.h"
#include "Core/Vectors/vvector2.h"
#include <vpointers.h>
#include <venmus.h>

class VIVertexAttribute
{
protected:
    VIVertexAttribute(const QString& name)
        : m_name(name)
    {

    }
public:
    const QString& name() const
    {
        return m_name;
    }

    virtual VVertexAttributeType datatype() const = 0;
private:
    const QString m_name;
};

template <VVertexAttributeType TypeName, class DataType>
class VVertexAttribute : public VIVertexAttribute
{
public:
    using this_type = VVertexAttribute<TypeName, DataType>;
    using container = QVector<DataType>;
    static QSharedPointer<this_type> instance(const QString& name, size_t capacity = 0)
    {
        return QSharedPointer<this_type>::create(name, capacity);
    }

    VVertexAttribute(const QString& name, size_t capacity)
        : VIVertexAttribute(name)
    {
        m_values.reserve(capacity);
    }

    VVertexAttribute(const QString& name)
        : VVertexAttribute(name, 0)
    {

    }

    VVertexAttributeType datatype() const Q_DECL_OVERRIDE
    {
        return (VVertexAttributeType)TypeName;
    }

    const container& values() const
    {
        return m_values;
    }

    container& values()
    {
        return m_values;
    }
private:
    QVector<DataType> m_values;
};

using VVertexAttributeFloatVector3 = VVertexAttribute<VVertexAttributeType::FloatVector3, VVector3F>;
using VVertexAttributeFloatVector3Ptr = QSharedPointer<VVertexAttributeFloatVector3>;

using VVertexAttributeFloatVector2 = VVertexAttribute<VVertexAttributeType::FloatVector2, VVector2F>;
using VVertexAttributeFloatVector2Ptr = QSharedPointer<VVertexAttributeFloatVector2>;

using VVertexAttributeDoubleVector3 = VVertexAttribute<VVertexAttributeType::EmulatedDoubleVector3, VVector3D>;
using VVertexAttributeDoubleVector3Ptr = QSharedPointer<VVertexAttributeDoubleVector3>;

#endif // VVERTEXATTRIBUTE_H
