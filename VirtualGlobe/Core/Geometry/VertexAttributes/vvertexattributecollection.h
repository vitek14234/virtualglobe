#ifndef VVERTEXATTRIBUTECOLLECTION_H
#define VVERTEXATTRIBUTECOLLECTION_H

#include <QHash>
#include <vpointers.h>

class VVertexAttributeCollection
{
public:
    static VVertexAttributeCollectionPtr instance();

    VVertexAttributeCollection();
    void add(const VIVertexAttributePtr& vertexAttribute);
    void clear();
    bool contains(const VIVertexAttributePtr& vertexAttribute) const;
    bool contains(const QString& vertexAttributeName) const;
    size_t count() const;
    bool remove(const VIVertexAttributePtr& vertexAttribute);
    bool remove(const QString& vertexAttributeName);
    VIVertexAttributePtr operator[](const QString& vertexAttributeName);
    const VIVertexAttributePtr operator[](const QString& vertexAttributeName) const;
private:
    QHash<QString, VIVertexAttributePtr> m_attributes;
};

#endif // VVERTEXATTRIBUTECOLLECTION_H
