#include "vvertexattributecollection.h"
#include "vvertexattribute.h"

VVertexAttributeCollectionPtr VVertexAttributeCollection::instance()
{
    return QSharedPointer<VVertexAttributeCollection>::create();
}

VVertexAttributeCollection::VVertexAttributeCollection()
{

}

void VVertexAttributeCollection::add(const VIVertexAttributePtr &vertexAttribute)
{
    Q_ASSERT(vertexAttribute);
    if (vertexAttribute)
    {
        m_attributes[vertexAttribute->name()] = vertexAttribute;
    }
}

void VVertexAttributeCollection::clear()
{
    m_attributes.clear();
}

bool VVertexAttributeCollection::contains(const VIVertexAttributePtr &vertexAttribute) const
{
    Q_ASSERT(vertexAttribute);
    if (vertexAttribute)
        return contains(vertexAttribute->name());
    return false;
}

bool VVertexAttributeCollection::contains(const QString &vertexAttributeName) const
{
    return m_attributes.contains(vertexAttributeName);
}

size_t VVertexAttributeCollection::count() const
{
    return m_attributes.size();
}

bool VVertexAttributeCollection::remove(const VIVertexAttributePtr &vertexAttribute)
{
    Q_ASSERT(vertexAttribute);
    if (vertexAttribute)
        return remove(vertexAttribute->name());
    return false;
}

bool VVertexAttributeCollection::remove(const QString &vertexAttributeName)
{
    return !!m_attributes.remove(vertexAttributeName);
}

const VIVertexAttributePtr VVertexAttributeCollection::operator[](const QString &vertexAttributeName) const
{
    return m_attributes.value(vertexAttributeName, Q_NULLPTR);
}

VIVertexAttributePtr VVertexAttributeCollection::operator[](const QString &vertexAttributeName)
{
    return m_attributes.value(vertexAttributeName, Q_NULLPTR);
}
