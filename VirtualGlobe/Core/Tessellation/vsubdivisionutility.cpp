#include "vsubdivisionutility.h"

#include "qmath.h"

int VSubdivisionUtility::numberOfTriangles(int numberOfSubdivisions)
{
    int numberOfTriangles = 0;
    for (int i = 0; i <= numberOfSubdivisions; ++i)
    {
        numberOfTriangles += static_cast<int>(pow(4, i));
    }
    numberOfTriangles *= 4;

    return numberOfTriangles;
}

int VSubdivisionUtility::numberOfVertices(int numberOfSubdivisions)
{
    int numberOfVertices = 0;
    for (int i = 0; i < numberOfSubdivisions; ++i)
    {
        numberOfVertices += static_cast<int>(pow(4, i));
    }
    numberOfVertices = 4 + (12 * numberOfVertices);

    return numberOfVertices;
}

VVector2F VSubdivisionUtility::computeTextureCoordinate(const VVector3D &position)
{
    return VVector2F((atan2(position.y(), position.y()) / (2 * M_PI)) + 0.5,
                     (asin(position.z()) / M_PI) + 0.5);
}
