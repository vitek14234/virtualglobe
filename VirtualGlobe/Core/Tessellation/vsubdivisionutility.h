#ifndef VSUBDIVISIONUTILITY_H
#define VSUBDIVISIONUTILITY_H

#include "Core/Vectors/vvector3.h"
#include "Core/Vectors/vvector2.h"

class VSubdivisionUtility
{
public:
    static int numberOfTriangles(int numberOfSubdivisions);
    static int numberOfVertices(int numberOfSubdivisions);
    static VVector2F computeTextureCoordinate(const VVector3D& position);
};

#endif // VSUBDIVISIONUTILITY_H
