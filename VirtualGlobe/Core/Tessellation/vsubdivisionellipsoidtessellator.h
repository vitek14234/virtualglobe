#ifndef VSUBDIVISIONELLIPSOIDTESSELLATOR_H
#define VSUBDIVISIONELLIPSOIDTESSELLATOR_H

#include "vpointers.h"
#include "venmus.h"
#include "Core/Geometry/vellipsoid.h"

class VSubdivisionEllipsoidTessellator
{
public:
    static VMeshPtr compute(const VEllipsoid& ellipsoid, int numberOfSubdivisions, VSubdivisionEllipsoidVertexAttributes vertexAttributes);
};

#endif // VSUBDIVISIONELLIPSOIDTESSELLATOR_H
