#include "vsubdivisionellipsoidtessellator.h"

#include <qmath.h>

#include "Core/Geometry/vmesh.h"
#include "Core/Geometry/VertexAttributes/vvertexattribute.h"
#include "Core/Geometry/VertexAttributes/vvertexattributecollection.h"
#include "Core/Geometry/Indices/viindices.h"
#include "Core/Tessellation/vsubdivisionutility.h"

struct SubdivisionMesh
{
    SubdivisionMesh() {};
    VEllipsoid ellipsoid = VEllipsoid::wgs84();
    QVector<VVector3D> *positions = Q_NULLPTR;
    QVector<VVector3F> *normals = Q_NULLPTR;
    QVector<VVector2F> *textureCoordinate = Q_NULLPTR;
    VIndicesUnsignedInt *indices = Q_NULLPTR;
};

namespace
{
static void subdivide(SubdivisionMesh& subdivisionMesh, const VTriangleIndicesUnsignedInt& triangle, int level)
{
    if (level > 0)
    {
        QVector<VVector3D>& positions = *subdivisionMesh.positions;
        const VVector3D n01 = ((positions[triangle.I0()] + positions[triangle.I1()]) * 0.5).normalized();
        const VVector3D n12 = ((positions[triangle.I1()] + positions[triangle.I2()]) * 0.5).normalized();
        const VVector3D n20 = ((positions[triangle.I2()] + positions[triangle.I0()]) * 0.5).normalized();

        const VVector3D p01 = n01 * subdivisionMesh.ellipsoid.radii();
        const VVector3D p12 = n12 * subdivisionMesh.ellipsoid.radii();
        const VVector3D p20 = n20 * subdivisionMesh.ellipsoid.radii();

        positions.push_back(p01);
        positions.push_back(p12);
        positions.push_back(p20);

        int i01 = positions.size() - 3;
        int i12 = positions.size() - 2;
        int i20 = positions.size() - 1;

        if ((subdivisionMesh.normals != Q_NULLPTR) || (subdivisionMesh.textureCoordinate != Q_NULLPTR))
        {
            const VVector3D d01 = subdivisionMesh.ellipsoid.geodeticSurfaceNormal(p01);
            const VVector3D d12 = subdivisionMesh.ellipsoid.geodeticSurfaceNormal(p12);
            const VVector3D d20 = subdivisionMesh.ellipsoid.geodeticSurfaceNormal(p20);

            if (subdivisionMesh.normals != Q_NULLPTR)
            {
                subdivisionMesh.normals->push_back(d01);
                subdivisionMesh.normals->push_back(d12);
                subdivisionMesh.normals->push_back(d20);
            }

            if (subdivisionMesh.textureCoordinate != Q_NULLPTR)
            {
                subdivisionMesh.textureCoordinate->push_back(VSubdivisionUtility::computeTextureCoordinate(d01));
                subdivisionMesh.textureCoordinate->push_back(VSubdivisionUtility::computeTextureCoordinate(d12));
                subdivisionMesh.textureCoordinate->push_back(VSubdivisionUtility::computeTextureCoordinate(d20));
            }
        }

        //
        // Subdivide input triangle into four triangles
        //
        --level;
        subdivide(subdivisionMesh, VTriangleIndicesUnsignedInt(triangle.I0(), i01, i20), level);
        subdivide(subdivisionMesh, VTriangleIndicesUnsignedInt(i01, triangle.I1(), i12), level);
        subdivide(subdivisionMesh, VTriangleIndicesUnsignedInt(i01, i12, i20), level);
        subdivide(subdivisionMesh, VTriangleIndicesUnsignedInt(i20, i12, triangle.I2()), level);
    }
    else
    {
        subdivisionMesh.indices->addTriangle(triangle);
    }
}
}

VMeshPtr VSubdivisionEllipsoidTessellator::compute(const VEllipsoid &ellipsoid, int numberOfSubdivisions, VSubdivisionEllipsoidVertexAttributes vertexAttributes)
{
    if (numberOfSubdivisions < 0)
    {
        Q_ASSERT(false);
        return nullptr;
    }

    VMeshPtr mesh = VMesh::instance();
    mesh->setPrimitiveType(VPrimitiveType::Triangles);
    mesh->setFrontFaceWindingOrder(VWindingOrder::Counterclockwise);

    int numberOfVertices = VSubdivisionUtility::numberOfVertices(numberOfSubdivisions);
    VVertexAttributeDoubleVector3Ptr positionsAttribute =  VVertexAttributeDoubleVector3::instance("position", numberOfVertices);
    mesh->attributes()->add(positionsAttribute);

    VIndicesUnsignedIntPtr indices = VIndicesUnsignedInt::instance(3 * VSubdivisionUtility::numberOfTriangles(numberOfSubdivisions));
    mesh->setIndices(indices);

    SubdivisionMesh subdivisionMesh;
    subdivisionMesh.ellipsoid = ellipsoid;
    subdivisionMesh.positions = &positionsAttribute->values();
    subdivisionMesh.indices = indices.get();

    if ((vertexAttributes & VSubdivisionEllipsoidVertexAttributes::Normal) == VSubdivisionEllipsoidVertexAttributes::Normal)
    {
        VVertexAttributeFloatVector3Ptr normalsAttribute = VVertexAttributeFloatVector3::instance("normal", numberOfVertices);
        mesh->attributes()->add(normalsAttribute);
        subdivisionMesh.normals = &normalsAttribute->values();
    }

    if ((vertexAttributes & VSubdivisionEllipsoidVertexAttributes::TextureCoordinate) == VSubdivisionEllipsoidVertexAttributes::TextureCoordinate)
    {
        VVertexAttributeFloatVector2Ptr textureCoordinateAttribute = VVertexAttributeFloatVector2::instance("textureCoordinate", numberOfVertices);
        mesh->attributes()->add(textureCoordinateAttribute);
        subdivisionMesh.textureCoordinate = &textureCoordinateAttribute->values();
    }

    //
    // Initial tetrahedron
    //
    const double negativeRootTwoOverThree = -sqrt(2.0) / 3.0;
    const double negativeOneThird = -1.0 / 3.0;
    const double rootSixOverThree = sqrt(6.0) / 3.0;

    const VVector3D n0(0, 0, 1);
    const VVector3D n1(0, (2.0 * sqrt(2.0)) / 3.0, negativeOneThird);
    const VVector3D n2(-rootSixOverThree, negativeRootTwoOverThree, negativeOneThird);
    const VVector3D n3(rootSixOverThree, negativeRootTwoOverThree, negativeOneThird);

    const VVector3D p0 = n0 * ellipsoid.radii();
    const VVector3D p1 = n1 * ellipsoid.radii();
    const VVector3D p2 = n2 * ellipsoid.radii();
    const VVector3D p3 = n3 * ellipsoid.radii();

    subdivisionMesh.positions->push_back(p0);
    subdivisionMesh.positions->push_back(p1);
    subdivisionMesh.positions->push_back(p2);
    subdivisionMesh.positions->push_back(p3);

    if ((subdivisionMesh.normals != Q_NULLPTR) || (subdivisionMesh.textureCoordinate != Q_NULLPTR))
    {
        const VVector3D d0 = ellipsoid.geodeticSurfaceNormal(p0);
        const VVector3D d1 = ellipsoid.geodeticSurfaceNormal(p1);
        const VVector3D d2 = ellipsoid.geodeticSurfaceNormal(p2);
        const VVector3D d3 = ellipsoid.geodeticSurfaceNormal(p3);

        if (subdivisionMesh.normals != Q_NULLPTR)
        {
            subdivisionMesh.normals->push_back(d0);
            subdivisionMesh.normals->push_back(d1);
            subdivisionMesh.normals->push_back(d2);
            subdivisionMesh.normals->push_back(d3);
        }

        if (subdivisionMesh.textureCoordinate != Q_NULLPTR)
        {
            subdivisionMesh.textureCoordinate->push_back(VSubdivisionUtility::computeTextureCoordinate(d0));
            subdivisionMesh.textureCoordinate->push_back(VSubdivisionUtility::computeTextureCoordinate(d1));
            subdivisionMesh.textureCoordinate->push_back(VSubdivisionUtility::computeTextureCoordinate(d2));
            subdivisionMesh.textureCoordinate->push_back(VSubdivisionUtility::computeTextureCoordinate(d3));
        }
    }

    subdivide(subdivisionMesh, VTriangleIndicesUnsignedInt(0, 1, 2), numberOfSubdivisions);
    subdivide(subdivisionMesh, VTriangleIndicesUnsignedInt(0, 2, 3), numberOfSubdivisions);
    subdivide(subdivisionMesh, VTriangleIndicesUnsignedInt(0, 3, 1), numberOfSubdivisions);
    subdivide(subdivisionMesh, VTriangleIndicesUnsignedInt(1, 3, 2), numberOfSubdivisions);

    return mesh;
}
