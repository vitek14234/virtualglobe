#ifndef VMatrix4_H
#define VMatrix4_H

#include <type_traits>
#include <QtCore/qcompilerdetection.h>
#include <QMatrix4x4>
#include <qmath.h>
#include "Core/Vectors/vvector3.h"

template<typename T>
class VMatrix4
{
public:
    VMatrix4()
    {
        setToIdentity();
    }

    Q_DECL_CONSTEXPR VMatrix4(int)
    {
    }

    VMatrix4(const QMatrix4x4 &matrix)
    {
        for (int row = 0; row < 4; ++row)
            for (int col = 0; col < 4; ++col)
        m[row][col] = matrix(row, col);
    }

    VMatrix4(const VMatrix4<T> &matrix)
    {
        for (int row = 0; row < 4; ++row)
                for (int col = 0; col < 4; ++col)
        m[row][col] = matrix.m[row][col];
    }

    template<typename D>
    static VMatrix4<T> fromMatrix(const VMatrix4<D> &matrix)
    {
        VMatrix4<T> result;
        for (int row = 0; row < 4; ++row)
                for (int col = 0; col < 4; ++col)
        result.m[row][col] = matrix(row, col);
        return result;
    }

    VMatrix4(
             T m11, T m12, T m13, T m14,
             T m21, T m22, T m23, T m24,
             T m31, T m32, T m33, T m34,
             T m41, T m42, T m43, T m44)
    {
        m[0][0] = m11; m[0][1] = m21; m[0][2] = m31; m[0][3] = m41;
        m[1][0] = m12; m[1][1] = m22; m[1][2] = m32; m[1][3] = m42;
        m[2][0] = m13; m[2][1] = m23; m[2][2] = m33; m[2][3] = m43;
        m[3][0] = m14; m[3][1] = m24; m[3][2] = m34; m[3][3] = m44;
    }

    inline const T& operator()(int row, int column) const
    {
        Q_ASSERT(row >= 0 && row < 4 && column >= 0 && column < 4);
        return m[row][column];
    }

    inline T& operator()(int row, int column)
    {
        Q_ASSERT(row >= 0 && row < 4 && column >= 0 && column < 4);
        return m[row][column];
    }

    inline void setToIdentity()
    {
        for(int i = 0; i < 4; ++i)
            for(int j = 0; j < 4; ++j)
            {
                if(i == j)
                    m[i][j] = 1;
                else
                    m[i][j] = 0;
            }
    }

    inline void fill(T value)
    {
        for(int i = 0; i < 4; ++i)
            for(int j = 0; j < 4; ++j)
            {
                m[i][j] = value;
            }
    }

    VMatrix4<T> transposed() const
    {
        VMatrix4<T> newMatrix;
        for(int i = 0; i < 4; ++i)
            for(int j = 0; j < 4; ++j)
            {
                newMatrix.m[i][j] = m[j][i];
            }
        return newMatrix;
    }

    inline T *data()
    {
        return *m;
    }

    inline const T *data() const
    {
        return *m;
    }

    Q_DECL_CONSTEXPR inline const T *constData() const
    {
        return *m;
    }

    void perspective(T verticalAngle, T aspectRatio, T nearPlane, T farPlane)
    {
        VMatrix4<T> m;
        // Bail out if the projection volume is zero-sized.
        if (nearPlane == farPlane || aspectRatio == 0.0f)
            return;
        // Construct the projection.
        T radians = qDegreesToRadians(verticalAngle / 2.0f);
        T sine = std::sin(radians);
        if (sine == 0.0f)
            return;
        T cotan = std::cos(radians) / sine;
        T clip = farPlane - nearPlane;
        m.m[0][0] = cotan / aspectRatio;
        m.m[1][0] = 0.0f;
        m.m[2][0] = 0.0f;
        m.m[3][0] = 0.0f;
        m.m[0][1] = 0.0f;
        m.m[1][1] = cotan;
        m.m[2][1] = 0.0f;
        m.m[3][1] = 0.0f;
        m.m[0][2] = 0.0f;
        m.m[1][2] = 0.0f;
        m.m[2][2] = -(nearPlane + farPlane) / clip;
        m.m[3][2] = -(2.0f * nearPlane * farPlane) / clip;
        m.m[0][3] = 0.0f;
        m.m[1][3] = 0.0f;
        m.m[2][3] = -1.0f;
        m.m[3][3] = 0.0f;

        // Apply the projection.
        *this *= m;
    }

    void ortho(T left, T right, T bottom, T top, T nearPlane, T farPlane)
    {
        VMatrix4<T> m;
        // Bail out if the projection volume is zero-sized.
        if (left == right || bottom == top || nearPlane == farPlane)
            return;
        // Construct the projection.
        T width = right - left;
        T invheight = top - bottom;
        T clip = farPlane - nearPlane;
        m.m[0][0] = 2.0f / width;
        m.m[1][0] = 0.0f;
        m.m[2][0] = 0.0f;
        m.m[3][0] = -(left + right) / width;
        m.m[0][1] = 0.0f;
        m.m[1][1] = 2.0f / invheight;
        m.m[2][1] = 0.0f;
        m.m[3][1] = -(top + bottom) / invheight;
        m.m[0][2] = 0.0f;
        m.m[1][2] = 0.0f;
        m.m[2][2] = -2.0f / clip;
        m.m[3][2] = -(nearPlane + farPlane) / clip;
        m.m[0][3] = 0.0f;
        m.m[1][3] = 0.0f;
        m.m[2][3] = 0.0f;
        m.m[3][3] = 1.0f;
        // Apply the projection.
        *this *= m;
    }

    void lookAt(const VVector3<T>& eye,const VVector3<T>& center, const VVector3<T>& up)
    {
        VMatrix4<T> m;
        VVector3<T> forward = center - eye;
        if (qFuzzyIsNull(forward.x()) && qFuzzyIsNull(forward.y()) && qFuzzyIsNull(forward.z()))
            return;
        forward.normalize();
        const VVector3<T> side = forward.crossProduct(up).normalized();
        const VVector3<T> upVector = side.crossProduct(forward);
        m.m[0][0] = side.x();
        m.m[1][0] = side.y();
        m.m[2][0] = side.z();
        m.m[3][0] = 0.0f;
        m.m[0][1] = upVector.x();
        m.m[1][1] = upVector.y();
        m.m[2][1] = upVector.z();
        m.m[3][1] = 0.0f;
        m.m[0][2] = -forward.x();
        m.m[1][2] = -forward.y();
        m.m[2][2] = -forward.z();
        m.m[3][2] = 0.0f;
        m.m[0][3] = 0.0f;
        m.m[1][3] = 0.0f;
        m.m[2][3] = 0.0f;
        m.m[3][3] = 1.0f;
        *this *= m;

        this->m[3][0] += this->m[0][0] * -eye.x() + this->m[1][0] * -eye.y();
        this->m[3][1] += this->m[0][1] * -eye.x() + this->m[1][1] * -eye.y();
        this->m[3][2] += this->m[2][2] * -eye.z();

        translate(-eye);
    }

    void translate(const VVector3<T> &vector)
    {
        const T vx = vector.x();
        const T vy = vector.y();
        const T vz = vector.z();

        m[3][0] += m[0][0] * vx + m[1][0] * vy + m[2][0] * vz;
        m[3][1] += m[0][1] * vx + m[1][1] * vy + m[2][1] * vz;
        m[3][2] += m[0][2] * vx + m[1][2] * vy + m[2][2] * vz;
        m[3][3] += m[0][3] * vx + m[1][3] * vy + m[2][3] * vz;
    }

    inline VMatrix4<T>& operator*=(const VMatrix4<T>& o)
    {
        const VMatrix4<T> other = o; // prevent aliasing when &o == this ### Qt 6: take o by value
        T m0, m1, m2;
        m0 = m[0][0] * other.m[0][0]
                + m[1][0] * other.m[0][1]
                + m[2][0] * other.m[0][2]
                + m[3][0] * other.m[0][3];
        m1 = m[0][0] * other.m[1][0]
                + m[1][0] * other.m[1][1]
                + m[2][0] * other.m[1][2]
                + m[3][0] * other.m[1][3];
        m2 = m[0][0] * other.m[2][0]
                + m[1][0] * other.m[2][1]
                + m[2][0] * other.m[2][2]
                + m[3][0] * other.m[2][3];
        m[3][0] = m[0][0] * other.m[3][0]
                + m[1][0] * other.m[3][1]
                + m[2][0] * other.m[3][2]
                + m[3][0] * other.m[3][3];
        m[0][0] = m0;
        m[1][0] = m1;
        m[2][0] = m2;
        m0 = m[0][1] * other.m[0][0]
                + m[1][1] * other.m[0][1]
                + m[2][1] * other.m[0][2]
                + m[3][1] * other.m[0][3];
        m1 = m[0][1] * other.m[1][0]
                + m[1][1] * other.m[1][1]
                + m[2][1] * other.m[1][2]
                + m[3][1] * other.m[1][3];
        m2 = m[0][1] * other.m[2][0]
                + m[1][1] * other.m[2][1]
                + m[2][1] * other.m[2][2]
                + m[3][1] * other.m[2][3];
        m[3][1] = m[0][1] * other.m[3][0]
                + m[1][1] * other.m[3][1]
                + m[2][1] * other.m[3][2]
                + m[3][1] * other.m[3][3];
        m[0][1] = m0;
        m[1][1] = m1;
        m[2][1] = m2;
        m0 = m[0][2] * other.m[0][0]
                + m[1][2] * other.m[0][1]
                + m[2][2] * other.m[0][2]
                + m[3][2] * other.m[0][3];
        m1 = m[0][2] * other.m[1][0]
                + m[1][2] * other.m[1][1]
                + m[2][2] * other.m[1][2]
                + m[3][2] * other.m[1][3];
        m2 = m[0][2] * other.m[2][0]
                + m[1][2] * other.m[2][1]
                + m[2][2] * other.m[2][2]
                + m[3][2] * other.m[2][3];
        m[3][2] = m[0][2] * other.m[3][0]
                + m[1][2] * other.m[3][1]
                + m[2][2] * other.m[3][2]
                + m[3][2] * other.m[3][3];
        m[0][2] = m0;
        m[1][2] = m1;
        m[2][2] = m2;
        m0 = m[0][3] * other.m[0][0]
                + m[1][3] * other.m[0][1]
                + m[2][3] * other.m[0][2]
                + m[3][3] * other.m[0][3];
        m1 = m[0][3] * other.m[1][0]
                + m[1][3] * other.m[1][1]
                + m[2][3] * other.m[1][2]
                + m[3][3] * other.m[1][3];
        m2 = m[0][3] * other.m[2][0]
                + m[1][3] * other.m[2][1]
                + m[2][3] * other.m[2][2]
                + m[3][3] * other.m[2][3];
        m[3][3] = m[0][3] * other.m[3][0]
                + m[1][3] * other.m[3][1]
                + m[2][3] * other.m[3][2]
                + m[3][3] * other.m[3][3];
        m[0][3] = m0;
        m[1][3] = m1;
        m[2][3] = m2;
        return *this;
    }

    static VMatrix4<T> identity()
    {
        return VMatrix4<T>(1.0, 0.0, 0.0, 0.0,
                           0.0, 1.0, 0.0, 0.0,
                           0.0, 0.0, 1.0, 0.0,
                           0.0, 0.0, 0.0, 1.0);
    }

    friend inline VMatrix4<T> operator*(const VMatrix4<T>& m1, const VMatrix4<T>& m2)
    {
        VMatrix4<T> m(1);
        m.m[0][0] = m1.m[0][0] * m2.m[0][0]
                 + m1.m[1][0] * m2.m[0][1]
                 + m1.m[2][0] * m2.m[0][2]
                 + m1.m[3][0] * m2.m[0][3];
        m.m[0][1] = m1.m[0][1] * m2.m[0][0]
                 + m1.m[1][1] * m2.m[0][1]
                 + m1.m[2][1] * m2.m[0][2]
                 + m1.m[3][1] * m2.m[0][3];
        m.m[0][2] = m1.m[0][2] * m2.m[0][0]
                 + m1.m[1][2] * m2.m[0][1]
                 + m1.m[2][2] * m2.m[0][2]
                 + m1.m[3][2] * m2.m[0][3];
        m.m[0][3] = m1.m[0][3] * m2.m[0][0]
                 + m1.m[1][3] * m2.m[0][1]
                 + m1.m[2][3] * m2.m[0][2]
                 + m1.m[3][3] * m2.m[0][3];
        m.m[1][0] = m1.m[0][0] * m2.m[1][0]
                 + m1.m[1][0] * m2.m[1][1]
                 + m1.m[2][0] * m2.m[1][2]
                 + m1.m[3][0] * m2.m[1][3];
        m.m[1][1] = m1.m[0][1] * m2.m[1][0]
                 + m1.m[1][1] * m2.m[1][1]
                 + m1.m[2][1] * m2.m[1][2]
                 + m1.m[3][1] * m2.m[1][3];
        m.m[1][2] = m1.m[0][2] * m2.m[1][0]
                 + m1.m[1][2] * m2.m[1][1]
                 + m1.m[2][2] * m2.m[1][2]
                 + m1.m[3][2] * m2.m[1][3];
        m.m[1][3] = m1.m[0][3] * m2.m[1][0]
                 + m1.m[1][3] * m2.m[1][1]
                 + m1.m[2][3] * m2.m[1][2]
                 + m1.m[3][3] * m2.m[1][3];
        m.m[2][0] = m1.m[0][0] * m2.m[2][0]
                 + m1.m[1][0] * m2.m[2][1]
                 + m1.m[2][0] * m2.m[2][2]
                 + m1.m[3][0] * m2.m[2][3];
        m.m[2][1] = m1.m[0][1] * m2.m[2][0]
                 + m1.m[1][1] * m2.m[2][1]
                 + m1.m[2][1] * m2.m[2][2]
                 + m1.m[3][1] * m2.m[2][3];
        m.m[2][2] = m1.m[0][2] * m2.m[2][0]
                 + m1.m[1][2] * m2.m[2][1]
                 + m1.m[2][2] * m2.m[2][2]
                 + m1.m[3][2] * m2.m[2][3];
        m.m[2][3] = m1.m[0][3] * m2.m[2][0]
                 + m1.m[1][3] * m2.m[2][1]
                 + m1.m[2][3] * m2.m[2][2]
                 + m1.m[3][3] * m2.m[2][3];
        m.m[3][0] = m1.m[0][0] * m2.m[3][0]
                 + m1.m[1][0] * m2.m[3][1]
                 + m1.m[2][0] * m2.m[3][2]
                 + m1.m[3][0] * m2.m[3][3];
        m.m[3][1] = m1.m[0][1] * m2.m[3][0]
                 + m1.m[1][1] * m2.m[3][1]
                 + m1.m[2][1] * m2.m[3][2]
                 + m1.m[3][1] * m2.m[3][3];
        m.m[3][2] = m1.m[0][2] * m2.m[3][0]
                 + m1.m[1][2] * m2.m[3][1]
                 + m1.m[2][2] * m2.m[3][2]
                 + m1.m[3][2] * m2.m[3][3];
        m.m[3][3] = m1.m[0][3] * m2.m[3][0]
                 + m1.m[1][3] * m2.m[3][1]
                 + m1.m[2][3] * m2.m[3][2]
                 + m1.m[3][3] * m2.m[3][3];
       return m;
    }

    QMatrix4x4 toQMatrix4x4() const
    {
        QMatrix4x4 result;
        for (int row = 0; row < 4; ++row)
                for (int col = 0; col < 4; ++col)
        result(row, col) = m[row][col];
        return result;
    }
private:
    T m[4][4];
};

using VMatrix4F = VMatrix4<float>;
using VMatrix4D = VMatrix4<double>;

#endif // VMatrix4_H
