#ifndef VMATRIX_H
#define VMATRIX_H

#include <type_traits>
#include <QtCore/qcompilerdetection.h>
#include <QMatrix3x3>
#include <qmath.h>
#include "Core/Vectors/vvector3.h"

template<typename T>
class VMatrix3
{
public:
    VMatrix3()
    {
        setToIdentity();
    }

    inline const T& operator()(int row, int column) const
    {
        Q_ASSERT(row >= 0 && row < 3 && column >= 0 && column < 3);
        return m[row][column];
    }

    inline T& operator()(int row, int column)
    {
        Q_ASSERT(row >= 0 && row < 3 && column >= 0 && column < 3);
        return m[row][column];
    }

    inline void setToIdentity()
    {
        for(int i = 0; i < 3; ++i)
            for(int j = 0; j < 3; ++j)
            {
                if(i == j)
                    m[i][j] = 1;
                else
                    m[i][j] = 0;
            }
    }

    inline void fill(T value)
    {
        for(int i = 0; i < 3; ++i)
            for(int j = 0; j < 3; ++j)
            {
                m[i][j] = value;
            }
    }

    Q_DECL_CONSTEXPR VMatrix3<T> transposed() const
    {
        VMatrix3<T> newMatrix;
        for(int i = 0; i < 3; ++i)
            for(int j = 0; j < 3; ++j)
            {
                newMatrix.m[i][j] = m[j][i];
            }
        return newMatrix;
    }

    Q_DECL_CONSTEXPR inline T *data()
    {
        return *m;
    }

//    Q_DECL_CONSTEXPR inline const T *data() const
//    {
//        return *m;
//    }

    Q_DECL_CONSTEXPR inline const T *constData() const
    {
        return *m;
    }

    friend VVector3<T> operator*(const VMatrix3<T> &matrix, const VVector3<T> &vector)
    {
        const T x = vector.x() * matrix.m[0][0] +
                    vector.y() * matrix.m[0][1] +
                    vector.z() * matrix.m[0][2];

        const T y = vector.x() * matrix.m[1][0] +
                    vector.y() * matrix.m[1][1] +
                    vector.z() * matrix.m[1][2];

        const T z = vector.x() * matrix.m[2][0] +
                    vector.y() * matrix.m[2][1] +
                    vector.z() * matrix.m[2][2];

        return VVector3<T>(x, y, z);
    }

    void translate(const VVector3<T> &vector)
    {
        const T vx = vector.x();
        const T vy = vector.y();
        const T vz = vector.z();

        m[3][0] += m[0][0] * vx + m[1][0] * vy + m[2][0] * vz;
        m[3][1] += m[0][1] * vx + m[1][1] * vy + m[2][1] * vz;
        m[3][2] += m[0][2] * vx + m[1][2] * vy + m[2][2] * vz;
        m[3][3] += m[0][3] * vx + m[1][3] * vy + m[2][3] * vz;
    }

    QMatrix3x3 toQMatrix3x3() const
    {
        QMatrix3x3 result;
        for (int row = 0; row < 3; ++row)
                for (int col = 0; col < 3; ++col)
        result(row, col) = m[row][col];
        return result;
    }
private:
    T m[3][3];
};

using VMatrix3F = VMatrix3<float>;
using VMatrix3D = VMatrix3<double>;

#endif // VMATRIX_H
