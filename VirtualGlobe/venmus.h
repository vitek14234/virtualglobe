#ifndef VENMUS_H
#define VENMUS_H

enum class VVertexAttributeType
{
    UnsignedByte,
    HalfFloat,
    HalfFloatVector2,
    HalfFloatVector3,
    HalfFloatVector4,
    Float,
    FloatVector2,
    FloatVector3,
    FloatVector4,
    EmulatedDoubleVector3,
};

enum class VPrimitiveType
{
    Points,
    Lines,
    LineLoop,
    LineStrip,
    Triangles,
    TriangleStrip,
    TriangleFan,
    LinesAdjacency,
    LineStripAdjacency,
    TrianglesAdjacency,
    TriangleStripAdjacency,
};

enum class VWindingOrder
{
    Clockwise,
    Counterclockwise,
};

enum class VIndicesType
{
    UnsignedShort,
    UnsignedInt,
};

enum class VShaderVertexAttributeType
{
    Float,
    FloatVector2,
    FloatVector3,
    FloatVector4,
    FloatMatrix22,
    FloatMatrix33,
    FloatMatrix44,
    Int,
    IntVector2,
    IntVector3,
    IntVector4
};

enum class VProgramPointSize
{
    Enabled,
    Disabled
};

enum class VRasterizationMode
{
   Point,
   Line,
   Fill
};

enum class VCullFace
{
    Front,
    Back,
    FrontAndBack
};

enum class VStencilOperation
{
    Zero,
    Invert,
    Keep,
    Replace,
    Increment,
    Decrement,
    IncrementWrap,
    DecrementWrap
};

enum class VStencilTestFunction
{
    Never,
    Less,
    Equal,
    LessThanOrEqual,
    Greater,
    NotEqual,
    GreaterThanOrEqual,
    Always,
};

enum class VDepthTestFunction
{
    Never,
    Less,
    Equal,
    LessThanOrEqual,
    Greater,
    NotEqual,
    GreaterThanOrEqual,
    Always
};

enum class VSourceBlendingFactor
{
    Zero,
    One,
    SourceAlpha,
    OneMinusSourceAlpha,
    DestinationAlpha,
    OneMinusDestinationAlpha,
    DestinationColor,
    OneMinusDestinationColor,
    SourceAlphaSaturate,
    ConstantColor,
    OneMinusConstantColor,
    ConstantAlpha,
    OneMinusConstantAlpha
};

enum class VDestinationBlendingFactor
{
    Zero,
    One,
    SourceColor,
    OneMinusSourceColor,
    SourceAlpha,
    OneMinusSourceAlpha,
    DestinationAlpha,
    OneMinusDestinationAlpha,
    DestinationColor,
    OneMinusDestinationColor,
    SourceAlphaSaturate,
    ConstantColor,
    OneMinusConstantColor,
    ConstantAlpha,
    OneMinusConstantAlpha
};

enum class VBlendEquation
{
    Add,
    Minimum,
    Maximum,
    Subtract,
    ReverseSubtract
};

enum VClearBuffers
{
    ColorBuffer = 1,
    DepthBuffer = 2,
    StencilBuffer = 4,
    ColorAndDepthBuffer = ColorBuffer | DepthBuffer,
    All = ColorBuffer | DepthBuffer | StencilBuffer
};

enum class VComponentDatatype
{
    Byte,
    UnsignedByte,
    Short,
    UnsignedShort,
    Int,
    UnsignedInt,
    Float,
    HalfFloat,
};

enum VSubdivisionEllipsoidVertexAttributes
{
    Position = 1,
    Normal = 2,
    TextureCoordinate = 4,
    AllEllipsoidVertexAttributes = Position | Normal | TextureCoordinate
};

enum class VUniformType
{
    Int,
    Float,
    FloatVector2,
    FloatVector3,
    FloatVector4,
    IntVector2,
    IntVector3,
    IntVector4,
    Bool,
    BoolVector2,
    BoolVector3,
    BoolVector4,
    FloatMatrix22,
    FloatMatrix33,
    FloatMatrix44,
    Sampler1D,
    Sampler2D,
    Sampler2DRectangle,
    Sampler2DRectangleShadow,
    Sampler3D,
    SamplerCube,
    Sampler1DShadow,
    Sampler2DShadow,
    FloatMatrix23,
    FloatMatrix24,
    FloatMatrix32,
    FloatMatrix34,
    FloatMatrix42,
    FloatMatrix43,
    Sampler1DArray,
    Sampler2DArray,
    Sampler1DArrayShadow,
    Sampler2DArrayShadow,
    SamplerCubeShadow,
    IntSampler1D,
    IntSampler2D,
    IntSampler2DRectangle,
    IntSampler3D,
    IntSamplerCube,
    IntSampler1DArray,
    IntSampler2DArray,
    UnsignedIntSampler1D,
    UnsignedIntSampler2D,
    UnsignedIntSampler2DRectangle,
    UnsignedIntSampler3D,
    UnsignedIntSamplerCube,
    UnsignedIntSampler1DArray,
    UnsignedIntSampler2DArray
};

#endif // VENMUS_H
