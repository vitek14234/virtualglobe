layout(location = og_positionVertexLocation) in vec4 position;
out vec3 worldPosition;
out vec3 positionToLight;
out vec3 positionToEye;

uniform mat4 og_modelViewPerspectiveMatrix;
uniform vec3 og_cameraEye;
uniform vec3 og_sunPosition;

void main()                     
{
      gl_Position = og_modelViewPerspectiveMatrix * position; 

      worldPosition = position.xyz;
      positionToLight = og_sunPosition - worldPosition;
      positionToEye = og_cameraEye - worldPosition;
}