in vec3 worldPosition;
in vec3 positionToLight;
in vec3 positionToEye;
out vec3 fragmentColor;

uniform vec4 og_diffuseSpecularAmbientShininess;
uniform sampler2D og_texture0;                    // Day
uniform sampler2D og_texture1;                    // Night

uniform float u_blendDuration;
uniform float u_blendDurationScale;

float LightIntensity(vec3 normal, vec3 toLight, vec3 toEye, float diffuseDot, vec4 diffuseSpecularAmbientShininess)
{
    vec3 toReflectedLight = reflect(-toLight, normal);

    float diffuse = max(diffuseDot, 0.0);
    float specular = max(dot(toReflectedLight, toEye), 0.0);
    specular = pow(specular, diffuseSpecularAmbientShininess.w);

    return (diffuseSpecularAmbientShininess.x * diffuse) +
           (diffuseSpecularAmbientShininess.y * specular) +
            diffuseSpecularAmbientShininess.z;
}

vec2 ComputeTextureCoordinates(vec3 normal)
{
    return vec2(atan(normal.y, normal.x) * og_oneOverTwoPi + 0.5, asin(normal.z) * og_oneOverPi + 0.5);
}

vec3 NightColor(vec3 normal)
{
    return texture(og_texture1, ComputeTextureCoordinates(normal)).rgb;
}

vec3 DayColor(vec3 normal, vec3 toLight, vec3 toEye, float diffuseDot, vec4 diffuseSpecularAmbientShininess)
{
    float intensity = LightIntensity(normal, toLight, toEye, diffuseDot, diffuseSpecularAmbientShininess);
    return intensity * texture(og_texture0, ComputeTextureCoordinates(normal)).rgb;
}

void main()
{
    vec3 normal = normalize(worldPosition);
    vec3 toLight = normalize(positionToLight);
    float diffuse = dot(toLight, normal);

    if (diffuse > u_blendDuration)
    {
        fragmentColor = DayColor(normal, toLight, normalize(positionToEye), diffuse, og_diffuseSpecularAmbientShininess);
    }
    else if (diffuse < -u_blendDuration)
    {
        fragmentColor = NightColor(normal);
    }
    else
    {
        vec3 night = NightColor(normal);
        vec3 day = DayColor(normal, toLight, normalize(positionToEye), diffuse, og_diffuseSpecularAmbientShininess);
        fragmentColor = mix(night, day, (diffuse + u_blendDuration) * u_blendDurationScale);
    }
}