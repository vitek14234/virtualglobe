#ifndef VUTILS_H
#define VUTILS_H

#include <QString>

class VUtils
{
public:
    static QString getFileSource(const QString& filename);
};

#endif // VUTILS_H
