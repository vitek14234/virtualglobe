#ifndef VDRAWSTATE_H
#define VDRAWSTATE_H

#include "vpointers.h"

class VDrawState
{
public:
    VDrawState();
    VDrawState(const VRenderStatePtr &renderState,
               const VShaderProgramPtr &shaderProgram,
               const VVertexArrayPtr& vertexArray);

    VDrawState(const VRenderStatePtr &renderState,
               const VShaderProgramPtr &shaderProgram,
               const VVertexArrayObjectPtr& vertexArrayObject);

    static QSharedPointer<VDrawState> instance();
    static QSharedPointer<VDrawState> instance(const VRenderStatePtr &renderState,
                                               const VShaderProgramPtr &shaderProgram,
                                               const VVertexArrayPtr& vertexArray);
    static QSharedPointer<VDrawState> instance(const VRenderStatePtr &renderState,
                                               const VShaderProgramPtr &shaderProgram,
                                               const VVertexArrayObjectPtr& vertexArrayObject);

    const VRenderStatePtr &renderState() const;
    const VShaderProgramPtr &shaderProgram() const;
    const VVertexArrayPtr &vertexArray() const;
    const VVertexArrayObjectPtr &vertexArrayObject() const;
private:
    VRenderStatePtr m_renderState;
    VShaderProgramPtr m_shaderProgram;
    VVertexArrayPtr m_vertexArray;
    VVertexArrayObjectPtr m_vertexArrayObject;
};

#endif // VDRAWSTATE_H
