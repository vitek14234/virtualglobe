#ifndef VCLEARSTATE_H
#define VCLEARSTATE_H

#include "vrenderstructs.h"
#include <QColor>
#include "vpointers.h"

struct VClearState
{
    static QSharedPointer<VClearState> instance();

    VScissorTest scissorTest;
    VColorMask colorMask = VColorMask(true, true, true, true);
    bool depthMask = true;
    int frontStencilMask = ~0;
    int backStencilMask = ~0;

    VClearBuffers buffers = VClearBuffers::All;
    QColor color = Qt::white;
    float depth = 1.0f;
    int stencil = 0;
};

#endif // VCLEARSTATE_H
