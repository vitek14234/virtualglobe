#ifndef VGLFACTORY_H
#define VGLFACTORY_H

#include "vpointers.h"

class VGLFactory
{
public:
    static VShaderProgramPtr createShader(const QString& vertexShaderSource, const QString& geometryShaderSource, const QString& fragmentShaderSource);
    static VShaderProgramPtr createShader(const QString& vertexShaderSource, const QString& fragmentShaderSource);
    static size_t maximumNumberOfVertexAttributes();
};

#endif // VGLFACTORY_H
