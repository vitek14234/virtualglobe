#ifndef VRENDERSTATE_H
#define VRENDERSTATE_H

#include "Renderer/vrenderstructs.h"
#include "vpointers.h"

struct VRenderState
{
    static QSharedPointer<VRenderState> instance()
    {
        return QSharedPointer<VRenderState>::create();
    }

    VPrimitiveRestart primitiveRestart;
    VFacetCulling facetCulling;
    VProgramPointSize programPointSize = VProgramPointSize::Disabled;
    VRasterizationMode rasterizationMode = VRasterizationMode::Fill;
    VScissorTest scissorTest;
    VStencilTest stencilTest;
    VDepthTest depthTest;
    VDepthRange depthRange;
    VBlending blending;
    VColorMask colorMask = VColorMask(true, true, true, true);
    bool depthMask = true;
};

#endif // VRENDERSTATE_H
