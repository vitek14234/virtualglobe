#ifndef VMESHBUFFERS_H
#define VMESHBUFFERS_H

#include "vpointers.h"

class VMeshBuffers
{
public:
    static QSharedPointer<VMeshBuffers> instance();

    VMeshBuffers();

    VVertexBufferAttributesPtr attributes();

    const VIndexBufferPtr& indexBuffer() const;
    void setIndexBuffer(const VIndexBufferPtr &indexBuffer);
private:
    VIndexBufferPtr m_indexBuffer;
    VMeshVertexBufferAttributesPtr m_attributes;
};

#endif // VMESHBUFFERS_H
