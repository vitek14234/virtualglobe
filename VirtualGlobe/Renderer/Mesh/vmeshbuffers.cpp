#include "vmeshbuffers.h"

#include "Renderer/Mesh/vmeshvertexbufferattributes.h"

QSharedPointer<VMeshBuffers> VMeshBuffers::instance()
{
    return QSharedPointer<VMeshBuffers>::create();
}

VMeshBuffers::VMeshBuffers()
{
    m_attributes = VMeshVertexBufferAttributes::instance();
}

VVertexBufferAttributesPtr VMeshBuffers::attributes()
{
    return m_attributes;
}

const VIndexBufferPtr& VMeshBuffers::indexBuffer() const
{
    return m_indexBuffer;
}

void VMeshBuffers::setIndexBuffer(const VIndexBufferPtr &indexBuffer)
{
    m_indexBuffer = indexBuffer;
}
