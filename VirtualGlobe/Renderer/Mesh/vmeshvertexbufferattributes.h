#ifndef VMESHVERTEXBUFFERATTRIBUTES_H
#define VMESHVERTEXBUFFERATTRIBUTES_H

#include "vpointers.h"
#include "Renderer/VertexArray/vvertexbufferattributes.h"
#include <QVector>

class VMeshVertexBufferAttributes : public VVertexBufferAttributes
{
public:
    static QSharedPointer<VMeshVertexBufferAttributes> instance();

    VMeshVertexBufferAttributes();

    // VVertexBufferAttributes interface
    VVertexBufferAttributePtr get(size_t index) const Q_DECL_OVERRIDE;
    void set(size_t index, const VVertexBufferAttributePtr& attribute) Q_DECL_OVERRIDE;
    size_t count() const Q_DECL_OVERRIDE;
    size_t maximumCount() const Q_DECL_OVERRIDE;
private:
    QVector<VVertexBufferAttributePtr> m_attributes;
    size_t m_count;
};

#endif // VMESHVERTEXBUFFERATTRIBUTES_H
