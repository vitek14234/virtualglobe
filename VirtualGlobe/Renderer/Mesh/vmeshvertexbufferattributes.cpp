#include "vmeshvertexbufferattributes.h"

#include "Renderer/vglfactory.h"

QSharedPointer<VMeshVertexBufferAttributes> VMeshVertexBufferAttributes::instance()
{
    return QSharedPointer<VMeshVertexBufferAttributes>::create();
}

VMeshVertexBufferAttributes::VMeshVertexBufferAttributes()
    : m_count(0)
{
    m_attributes.resize(VGLFactory::maximumNumberOfVertexAttributes());
}

VVertexBufferAttributePtr VMeshVertexBufferAttributes::get(size_t index) const
{
    //Q_ASSERT(index < m_count);
    return m_attributes[index];
}

void VMeshVertexBufferAttributes::set(size_t index, const VVertexBufferAttributePtr &attribute)
{
    if ((m_attributes[index] != Q_NULLPTR) && (attribute == Q_NULLPTR))
    {
        --m_count;
    }
    else if ((m_attributes[index] == Q_NULLPTR) && (attribute != Q_NULLPTR))
    {
        ++m_count;
    }

    m_attributes[index] = attribute;
}

size_t VMeshVertexBufferAttributes::count() const
{
    return m_count;
}

size_t VMeshVertexBufferAttributes::maximumCount() const
{
    return m_attributes.size();
}
