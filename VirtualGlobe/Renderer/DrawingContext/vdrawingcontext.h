#ifndef DRAWINGCONTEXT_H
#define DRAWINGCONTEXT_H

#include <QEnableSharedFromThis>
#include "vpointers.h"
#include "Renderer/vrenderstructs.h"
#include "Renderer/RenderState/vrenderstate.h"

class VVirtualGlobe;

class VDrawingContext : public QEnableSharedFromThis<VDrawingContext>
{
public:
    static QSharedPointer<VDrawingContext> instance(VVirtualGlobe& parent);

    VDrawingContext(VVirtualGlobe& parent);

    const VTextureUnitsPtr &textureUnits() const;
    void setViewport(const QRect& viewport);
    void clear(const VClearStatePtr& clearState);
    void draw(VPrimitiveType primitiveType, const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState);
    void draw2(VPrimitiveType primitiveType, const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState);
private:
    void forceApplyRenderState(const VRenderState& renderState);
    void glEnable(unsigned int flag, bool enable);
    void forceApplyRenderStateStencil(unsigned int face, const VStencilTestFace& test);
    void applyFramebuffer();
    void applyScissorTest(const VScissorTest& scissorTest);
    void applyColorMask(const VColorMask& mask);
    void applyDepthMask(bool depthMask);
    void verifyDraw(const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState);
    void applyBeforeDraw(const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState);
    void applyRenderState(const VRenderStatePtr &renderState);
    void applyPrimitiveRestart(VPrimitiveRestart primitiveRestart);
    void applyFacetCulling(VFacetCulling facetCulling);
    void applyProgramPointSize(VProgramPointSize programPointSize);
    void applyRasterizationMode(VRasterizationMode rasterizationMode);
    void applyStencilTest(VStencilTest stencilTest);
    void applyDepthTest(VDepthTest depthTest);
    void applyDepthRange(VDepthRange depthRange);
    void applyBlending(VBlending blending);
    void applyVertexArray(const VVertexArrayPtr& vertexArray);
    void applyShaderProgram(const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState);

    VVirtualGlobe& m_parent;
    VRenderState m_renderState;
    VTextureUnitsPtr m_textureUnits;
    QColor m_clearColor;
    float m_clearDepth;
    int m_clearStencil;
    QRect m_viewport;
};

#endif // DRAWINGCONTEXT_H
