#include "vdrawingcontext.h"
#include "vtypeconvertergl.h"
#include <QOpenGLFunctions>
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLFunctions_4_0_Core>
#include <QOpenGLFunctions_4_3_Core>
#include "vvirtualglobe.h"
#include "Renderer/vclearstate.h"
#include "Renderer/vdrawstate.h"
#include "Renderer/Shaders/vshaderprogram.h"
#include "Renderer/Textures/vtextureunits.h"
#include "Renderer/VertexArray/vvertexarray.h"
#include "Renderer/VertexArray/vvertexbufferattributes.h"
#include "Renderer/VertexArray/vvertexarrayobject.h"
#include "Renderer/DrawingContext/Buffers/vindexbuffer.h"

QSharedPointer<VDrawingContext> VDrawingContext::instance(VVirtualGlobe &parent)
{
    return QSharedPointer<VDrawingContext>::create(parent);
}

VDrawingContext::VDrawingContext(VVirtualGlobe &parent)
    : m_parent(parent)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();

    float clearColor[4];
    gl->glGetFloatv(GL_COLOR_CLEAR_VALUE, clearColor);
    m_clearColor.setRgbF(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);

    gl->glGetFloatv(GL_DEPTH_CLEAR_VALUE, &m_clearDepth);
    gl->glGetIntegerv(GL_STENCIL_CLEAR_VALUE, &m_clearStencil);

    m_textureUnits = VTextureUnits::instance();

    //
    // Sync GL state with default render state.
    //
    forceApplyRenderState(m_renderState);

    setViewport(QRect(0, 0, m_parent.width(), m_parent.height()));
}

const VTextureUnitsPtr &VDrawingContext::textureUnits() const
{
    return m_textureUnits;
}

void VDrawingContext::clear(const VClearStatePtr &clearState)
{
    Q_ASSERT(clearState);
    if (!clearState)
        return;

    applyFramebuffer();
    applyScissorTest(clearState->scissorTest);
    applyColorMask(clearState->colorMask);
    applyDepthMask(clearState->depthMask);

    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    if (m_clearColor != clearState->color)
    {
        gl->glClearColor(clearState->color.redF(),
                         clearState->color.greenF(),
                         clearState->color.blueF(),
                         clearState->color.alphaF());
        m_clearColor = clearState->color;
    }

    if (m_clearDepth != clearState->depth)
    {
        gl->glClearDepthf(clearState->depth);
        m_clearDepth = clearState->depth;
    }

    if (m_clearStencil != clearState->stencil)
    {
        gl->glClearStencil(clearState->stencil);
        m_clearStencil = clearState->stencil;
    }

    gl->glClear(VTypeConverterGL::To(clearState->buffers));
}

void VDrawingContext::draw(VPrimitiveType primitiveType, const VDrawStatePtr &drawState, const VSceneStatePtr &sceneState)
{
   // verifyDraw(drawState, sceneState);
    applyBeforeDraw(drawState, sceneState);

    const VVertexArrayPtr &vertexArray = drawState->vertexArray();
    const VIndexBufferPtr &indexBuffer = vertexArray->indexBuffer();

    vertexArray->bind();
    drawState->shaderProgram()->bind();

//    VVertexArray

    auto *gl = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_3_3_Core>();

    if (indexBuffer)
    {
        gl->glDrawRangeElements(
                    VTypeConverterGL::To(primitiveType),
                    0,
                    vertexArray->maximumArrayIndex(),
                    indexBuffer->count(),
                    VTypeConverterGL::To(indexBuffer->dataType()),
                    Q_NULLPTR);
    }
//    else
//    {
//        GL.DrawArrays(TypeConverterGL3x.To(primitiveType), 0,
//            vertexArray.MaximumArrayIndex() + 1);
    //    }
}

void VDrawingContext::draw2(VPrimitiveType primitiveType, const VDrawStatePtr &drawState, const VSceneStatePtr &sceneState)
{
    verifyDraw(drawState, sceneState);
    applyRenderState(drawState->renderState());
    applyShaderProgram(drawState, sceneState);
    m_textureUnits->clean();

    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();

    const VVertexArrayObjectPtr &vertexArrayObject = drawState->vertexArrayObject();
    QOpenGLVertexArrayObject::Binder vaoBinder(vertexArrayObject.data());
    const VIndexBufferPtr &indexBuffer = vertexArrayObject->indexBuffer();

    if (indexBuffer)
    {
        gl->glDrawElements(
                    VTypeConverterGL::To(primitiveType),
                    indexBuffer->count(),
                    VTypeConverterGL::To(indexBuffer->dataType()),
                    Q_NULLPTR);
    }

    drawState->shaderProgram()->release();
}

void VDrawingContext::forceApplyRenderState(const VRenderState &renderState)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();

   // gl 4
   // glEnable(GL_PRIMITIVE_RESTART, renderState.primitiveRestart.enabled);
   // gl->glPrimitiveRestartIndex

    glEnable(GL_CULL_FACE, renderState.facetCulling.enabled);
    gl->glCullFace(VTypeConverterGL::To(renderState.facetCulling.face));
    gl->glFrontFace(VTypeConverterGL::To(renderState.facetCulling.frontFaceWindingOrder));

    glEnable(GL_PROGRAM_POINT_SIZE, renderState.programPointSize == VProgramPointSize::Enabled);

    //gl->glPolygonMode(MaterialFace.FrontAndBack, TypeConverterGL3x.To(renderState.RasterizationMode));

    glEnable(GL_SCISSOR_TEST, renderState.scissorTest.enabled);
    const QRect& scissorRectangle = renderState.scissorTest.rectangle;
    gl->glScissor(scissorRectangle.x(), scissorRectangle.y(), scissorRectangle.width(), scissorRectangle.height());

    glEnable(GL_STENCIL_TEST, renderState.scissorTest.enabled);
    forceApplyRenderStateStencil(GL_FRONT, renderState.stencilTest.frontFace);
    forceApplyRenderStateStencil(GL_BACK, renderState.stencilTest.backFace);

    glEnable(GL_DEPTH_TEST, renderState.depthTest.enabled);
    gl->glDepthFunc(VTypeConverterGL::To(renderState.depthTest.function));

    gl->glDepthRangef(renderState.depthRange.m_near, renderState.depthRange.m_far);

    glEnable(GL_BLEND, renderState.blending.enabled);
    gl->glBlendFuncSeparate(VTypeConverterGL::To(renderState.blending.sourceRGBFactor),
                            VTypeConverterGL::To(renderState.blending.destinationRGBFactor),
                            VTypeConverterGL::To(renderState.blending.sourceAlphaFactor),
                            VTypeConverterGL::To(renderState.blending.destinationAlphaFactor));
    gl->glBlendEquationSeparate(VTypeConverterGL::To(renderState.blending.RGBEquation),
                                VTypeConverterGL::To(renderState.blending.alphaEquation));
    gl->glBlendColor(renderState.blending.color.redF(),
                     renderState.blending.color.greenF(),
                     renderState.blending.color.blueF(),
                     renderState.blending.color.alphaF());

    gl->glDepthMask(renderState.depthMask);

    const VColorMask& mask = renderState.colorMask;
    gl->glColorMask(mask.red(), mask.green(), mask.blue(), mask.alpha());
    m_renderState.colorMask = mask;
}

void VDrawingContext::glEnable(unsigned int flag, bool enable)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    if (enable)
        gl->glEnable(flag);
    else
        gl->glDisable(flag);
}

void VDrawingContext::forceApplyRenderStateStencil(unsigned int face, const VStencilTestFace &test)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    gl->glStencilOpSeparate(face,
                            VTypeConverterGL::To(test.stencilFailOperation),
                            VTypeConverterGL::To(test.depthFailStencilPassOperation),
                            VTypeConverterGL::To(test.depthPassStencilPassOperation));

    gl->glStencilFuncSeparate(face, VTypeConverterGL::To(test.function), test.referenceValue, test.mask);
}

void VDrawingContext::applyFramebuffer()
{

}

void VDrawingContext::applyScissorTest(const VScissorTest &scissorTest)
{
    const QRect& rectangle = scissorTest.rectangle;

    if(rectangle.x() < 0 || rectangle.y() < 0)
    {
        Q_ASSERT(false);
        return;
    }

    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    if (m_renderState.scissorTest.enabled != scissorTest.enabled)
    {
        glEnable(GL_SCISSOR_TEST, scissorTest.enabled);
        m_renderState.scissorTest.enabled = scissorTest.enabled;
    }

    if (scissorTest.enabled)
    {
        if (m_renderState.scissorTest.rectangle != scissorTest.rectangle)
        {
            gl->glScissor(rectangle.x(), rectangle.y(), rectangle.width(), rectangle.height());
            m_renderState.scissorTest.rectangle = scissorTest.rectangle;
        }
    }
}

void VDrawingContext::applyColorMask(const VColorMask &mask)
{
    if (m_renderState.colorMask != mask)
    {
        QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
        gl->glColorMask(mask.red(), mask.green(), mask.blue(), mask.alpha());
        m_renderState.colorMask = mask;
    }
}

void VDrawingContext::applyDepthMask(bool depthMask)
{
    if (m_renderState.depthMask != depthMask)
    {
        QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
        gl->glDepthMask(depthMask);
        m_renderState.depthMask = depthMask;
    }
}

void VDrawingContext::setViewport(const QRect &viewport)
{
    if (m_viewport != viewport)
    {
        QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
        gl->glViewport(m_viewport.x(), m_viewport.y(), m_viewport.width(), m_viewport.height());
        m_viewport = viewport;
    }
}

void VDrawingContext::verifyDraw(const VDrawStatePtr &drawState, const VSceneStatePtr &sceneState)
{
    Q_ASSERT(drawState);
    Q_ASSERT(drawState->renderState());
    Q_ASSERT(drawState->shaderProgram());
    //Q_ASSERT(drawState->vertexArray());
    Q_ASSERT(sceneState);

//    if (_setFramebuffer != null)
//    {
//        if (drawState.RenderState.DepthTest.Enabled &&
//            !((_setFramebuffer.DepthAttachment != null) ||
//              (_setFramebuffer.DepthStencilAttachment != null)))
//        {
//            throw new ArgumentException("The depth test is enabled (drawState.RenderState.DepthTest.Enabled) but the context's Framebuffer property doesn't have a depth or depth/stencil attachment (DepthAttachment or DepthStencilAttachment).", "drawState");
//        }
    //    }
}

void VDrawingContext::applyBeforeDraw(const VDrawStatePtr &drawState, const VSceneStatePtr &sceneState)
{
    //applyRenderState(drawState->renderState());
    applyVertexArray(drawState->vertexArray());
    applyShaderProgram(drawState, sceneState);

//    //m_textureUnits.Clean();
//    ApplyFramebuffer();
}

void VDrawingContext::applyRenderState(const VRenderStatePtr &renderState)
{
    applyPrimitiveRestart(renderState->primitiveRestart);
    applyFacetCulling(renderState->facetCulling);
    applyProgramPointSize(renderState->programPointSize);
    applyRasterizationMode(renderState->rasterizationMode);
    applyScissorTest(renderState->scissorTest);
    applyStencilTest(renderState->stencilTest);
    applyDepthTest(renderState->depthTest);
    applyDepthRange(renderState->depthRange);
    applyBlending(renderState->blending);
    applyColorMask(renderState->colorMask);
    applyDepthMask(renderState->depthMask);
}

void VDrawingContext::applyPrimitiveRestart(VPrimitiveRestart primitiveRestart)
{
    if (m_renderState.primitiveRestart.enabled != primitiveRestart.enabled)
    {
        glEnable(GL_PRIMITIVE_RESTART, primitiveRestart.enabled);
        m_renderState.primitiveRestart.enabled = primitiveRestart.enabled;
    }

    if (primitiveRestart.enabled)
    {
        if (m_renderState.primitiveRestart.index != primitiveRestart.index)
        {
            auto *gl = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
            gl->glPrimitiveRestartIndex(primitiveRestart.index);
            m_renderState.primitiveRestart.index = primitiveRestart.index;
        }
    }
}

void VDrawingContext::applyFacetCulling(VFacetCulling facetCulling)
{
    if (m_renderState.facetCulling.enabled != facetCulling.enabled)
    {
        glEnable(GL_CULL_FACE, facetCulling.enabled);
        m_renderState.facetCulling.enabled = facetCulling.enabled;
    }

    if (facetCulling.enabled)
    {
        QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
        if (m_renderState.facetCulling.face != facetCulling.face)
        {
            gl->glCullFace(VTypeConverterGL::To(facetCulling.face));
            m_renderState.facetCulling.face = facetCulling.face;
        }

        if (m_renderState.facetCulling.frontFaceWindingOrder != facetCulling.frontFaceWindingOrder)
        {
            gl->glFrontFace(VTypeConverterGL::To(facetCulling.frontFaceWindingOrder));
            m_renderState.facetCulling.frontFaceWindingOrder = facetCulling.frontFaceWindingOrder;
        }
    }
}

void VDrawingContext::applyProgramPointSize(VProgramPointSize programPointSize)
{
    if (m_renderState.programPointSize != programPointSize)
    {
        glEnable(GL_PROGRAM_POINT_SIZE, programPointSize == VProgramPointSize::Enabled);
        m_renderState.programPointSize = programPointSize;
    }
}

void VDrawingContext::applyRasterizationMode(VRasterizationMode rasterizationMode)
{
    if (m_renderState.rasterizationMode != rasterizationMode)
    {
        auto *gl = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();
        gl->glPolygonMode(GL_FRONT_AND_BACK, VTypeConverterGL::To(rasterizationMode));
        m_renderState.rasterizationMode = rasterizationMode;
    }
}

void VDrawingContext::applyStencilTest(VStencilTest stencilTest)
{
    if (m_renderState.stencilTest.enabled != stencilTest.enabled)
    {
        glEnable(GL_STENCIL_TEST, stencilTest.enabled);
        m_renderState.stencilTest.enabled = stencilTest.enabled;
    }

    if (stencilTest.enabled)
    {
        Q_ASSERT(false);
        auto applyStencil = [&](GLenum face, VStencilTestFace currentTest, VStencilTestFace test)
        {
            if ((currentTest.stencilFailOperation != test.stencilFailOperation) ||
                (currentTest.depthFailStencilPassOperation != test.depthFailStencilPassOperation) ||
                (currentTest.depthPassStencilPassOperation != test.depthPassStencilPassOperation))
            {
//                GL.StencilOpSeparate(face,
//                    TypeConverterGL3x.To(test.stencilFailOperation),
//                    TypeConverterGL3x.To(test.depthFailStencilPassOperation),
//                    TypeConverterGL3x.To(test.depthPassStencilPassOperation));

                currentTest.stencilFailOperation = test.stencilFailOperation;
                currentTest.depthFailStencilPassOperation = test.depthFailStencilPassOperation;
                currentTest.depthPassStencilPassOperation = test.depthPassStencilPassOperation;
            }

            if ((currentTest.function != test.function) ||
                (currentTest.referenceValue != test.referenceValue) ||
                (currentTest.mask != test.mask))
            {
//                GL.StencilFuncSeparate(face,
//                    TypeConverterGL3x.To(test.Function),
//                    test.ReferenceValue,
//                    test.Mask);

                currentTest.function = test.function;
                currentTest.referenceValue = test.referenceValue;
                currentTest.mask = test.mask;
            }
        };

        applyStencil(GL_FRONT, m_renderState.stencilTest.frontFace, stencilTest.frontFace);
        applyStencil(GL_BACK, m_renderState.stencilTest.backFace, stencilTest.backFace);
    }
}

void VDrawingContext::applyDepthTest(VDepthTest depthTest)
{
    if (m_renderState.depthTest.enabled != depthTest.enabled)
    {
        glEnable(GL_DEPTH_TEST, depthTest.enabled);
        m_renderState.depthTest.enabled = depthTest.enabled;
    }

    if (depthTest.enabled)
    {
        if (m_renderState.depthTest.function != depthTest.function)
        {
            QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
            gl->glDepthFunc(VTypeConverterGL::To(depthTest.function));
            m_renderState.depthTest.function = depthTest.function;
        }
    }
}

void VDrawingContext::applyDepthRange(VDepthRange depthRange)
{
    Q_ASSERT(!(depthRange.m_near < 0.0 || depthRange.m_near > 1.0));
    Q_ASSERT(!(depthRange.m_far < 0.0 || depthRange.m_far > 1.0));

    if ((m_renderState.depthRange.m_near != depthRange.m_near) ||
        (m_renderState.depthRange.m_far != depthRange.m_far))
    {
        QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
        gl->glDepthRangef(depthRange.m_near, depthRange.m_far);
        m_renderState.depthRange.m_near = depthRange.m_near;
        m_renderState.depthRange.m_far = depthRange.m_far;
    }
}

void VDrawingContext::applyBlending(VBlending blending)
{
    if (m_renderState.blending.enabled != blending.enabled)
    {
        glEnable(GL_BLEND, blending.enabled);
        m_renderState.blending.enabled = blending.enabled;
    }

    Q_ASSERT(!blending.enabled);
}

void VDrawingContext::applyVertexArray(const VVertexArrayPtr &vertexArray)
{
    vertexArray->bind();
    vertexArray->clean();
}

void VDrawingContext::applyShaderProgram(const VDrawStatePtr &drawState, const VSceneStatePtr &sceneState)
{
    drawState->shaderProgram()->bind();
    drawState->shaderProgram()->clean(sharedFromThis(), drawState, sceneState);
}
