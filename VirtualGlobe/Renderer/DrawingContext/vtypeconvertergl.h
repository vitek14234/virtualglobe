#ifndef VTYPECONVERTERGL_H
#define VTYPECONVERTERGL_H

#include "venmus.h"
#include <QOpenGLFunctions>
#include <QtGui/qopenglext.h>

class VTypeConverterGL
{
public:
    static GLbitfield To(VClearBuffers mask)
    {
        GLbitfield clearMask = 0;

        if ((mask & VClearBuffers::ColorBuffer) != 0)
        {
            clearMask |= GL_COLOR_BUFFER_BIT;
        }

        if ((mask & VClearBuffers::DepthBuffer) != 0)
        {
            clearMask |= GL_DEPTH_BUFFER_BIT;
        }

        if ((mask & VClearBuffers::StencilBuffer) != 0)
        {
            clearMask |= GL_STENCIL_BUFFER_BIT;
        }

        return clearMask;
    }

    static GLenum To(VCullFace face)
    {
        switch (face)
        {
            case VCullFace::Front:
                return GL_FRONT;
            case VCullFace::Back:
                return GL_BACK;
            case VCullFace::FrontAndBack:
                return GL_FRONT_AND_BACK;
        }
        Q_ASSERT(false);
        return 0;
    }

    static GLenum To(VWindingOrder windingOrder)
    {
        switch (windingOrder)
        {
            case VWindingOrder::Clockwise:
                return GL_CW;
            case VWindingOrder::Counterclockwise:
                return GL_CCW;
        }
        Q_ASSERT(false);
        return 0;
    }

    static GLenum To(VStencilOperation operation)
    {
        switch (operation)
        {
            case VStencilOperation::Zero:
                return GL_ZERO;
            case VStencilOperation::Invert:
                return GL_INVERT;
            case VStencilOperation::Keep:
                return GL_KEEP;
            case VStencilOperation::Replace:
                return GL_REPLACE;
            case VStencilOperation::Increment:
                return GL_INCR;
            case VStencilOperation::Decrement:
                return GL_DECR;
            case VStencilOperation::IncrementWrap:
                return GL_INCR_WRAP;
            case VStencilOperation::DecrementWrap:
                return GL_DECR_WRAP;
        }

        Q_ASSERT(false);
        return 0;
    }

    static GLenum To(VStencilTestFunction function)
    {
        switch (function)
        {
            case VStencilTestFunction::Never:
                return GL_NEVER;
            case VStencilTestFunction::Less:
                return GL_LESS;
            case VStencilTestFunction::Equal:
                return GL_EQUAL;
            case VStencilTestFunction::LessThanOrEqual:
                return GL_LEQUAL;
            case VStencilTestFunction::Greater:
                return GL_GREATER;
            case VStencilTestFunction::NotEqual:
                return GL_NOTEQUAL;
            case VStencilTestFunction::GreaterThanOrEqual:
                return GL_GEQUAL;
            case VStencilTestFunction::Always:
                return GL_ALWAYS;
        }

        Q_ASSERT(false);
        return 0;
    }

    static GLenum To(VDepthTestFunction function)
    {
        switch (function)
        {
            case VDepthTestFunction::Never:
                return GL_NEVER;
            case VDepthTestFunction::Less:
                return GL_LESS;
            case VDepthTestFunction::Equal:
                return GL_EQUAL;
            case VDepthTestFunction::LessThanOrEqual:
                return GL_LEQUAL;
            case VDepthTestFunction::Greater:
                return GL_GREATER;
            case VDepthTestFunction::NotEqual:
                return GL_NOTEQUAL;
            case VDepthTestFunction::GreaterThanOrEqual:
                return GL_GEQUAL;
            case VDepthTestFunction::Always:
                return GL_ALWAYS;
        }

        Q_ASSERT(false);
        return 0;
    }

    static GLenum To(VSourceBlendingFactor factor)
    {
        switch (factor)
        {
            case VSourceBlendingFactor::Zero:
                return GL_ZERO;
            case VSourceBlendingFactor::One:
                return GL_ONE;
            case VSourceBlendingFactor::SourceAlpha:
                return GL_SRC_ALPHA;
            case VSourceBlendingFactor::OneMinusSourceAlpha:
                return GL_ONE_MINUS_SRC_ALPHA;
            case VSourceBlendingFactor::DestinationAlpha:
                return  GL_DST_ALPHA;
            case VSourceBlendingFactor::OneMinusDestinationAlpha:
                return GL_ONE_MINUS_DST_ALPHA;
            case VSourceBlendingFactor::DestinationColor:
                return GL_DST_COLOR;
            case VSourceBlendingFactor::OneMinusDestinationColor:
                return GL_ONE_MINUS_DST_COLOR;
            case VSourceBlendingFactor::SourceAlphaSaturate:
                return GL_SRC_ALPHA_SATURATE;
            case VSourceBlendingFactor::ConstantColor:
                return GL_CONSTANT_COLOR;
            case VSourceBlendingFactor::OneMinusConstantColor:
                return GL_ONE_MINUS_CONSTANT_COLOR;
            case VSourceBlendingFactor::ConstantAlpha:
                return GL_CONSTANT_ALPHA;
            case VSourceBlendingFactor::OneMinusConstantAlpha:
                return GL_ONE_MINUS_CONSTANT_ALPHA;
        }

        Q_ASSERT(false);
        return 0;
    }

    static GLenum To(VDestinationBlendingFactor factor)
    {
        switch (factor)
        {
            case VDestinationBlendingFactor::Zero:
                return GL_ZERO;
            case VDestinationBlendingFactor::One:
                return GL_ONE;
            case VDestinationBlendingFactor::SourceAlpha:
                return GL_SRC_ALPHA;
            case VDestinationBlendingFactor::OneMinusSourceAlpha:
                return GL_ONE_MINUS_SRC_ALPHA;
            case VDestinationBlendingFactor::DestinationAlpha:
                return GL_DST_ALPHA;
            case VDestinationBlendingFactor::OneMinusDestinationAlpha:
                return GL_ONE_MINUS_DST_ALPHA;
            case VDestinationBlendingFactor::DestinationColor:
                return GL_DST_COLOR;
            case VDestinationBlendingFactor::OneMinusDestinationColor:
                return GL_ONE_MINUS_DST_COLOR;
            case VDestinationBlendingFactor::SourceAlphaSaturate:
                return GL_SRC_ALPHA_SATURATE;
            case VDestinationBlendingFactor::ConstantColor:
                return GL_CONSTANT_COLOR;
            case VDestinationBlendingFactor::OneMinusConstantColor:
                return GL_ONE_MINUS_CONSTANT_COLOR;
            case VDestinationBlendingFactor::ConstantAlpha:
                return GL_CONSTANT_ALPHA;
            case VDestinationBlendingFactor::OneMinusConstantAlpha:
                return GL_ONE_MINUS_CONSTANT_ALPHA;
            case VDestinationBlendingFactor::SourceColor:
                return GL_SRC_COLOR;
            case VDestinationBlendingFactor::OneMinusSourceColor:
                return GL_ONE_MINUS_SRC_COLOR;
        }

        Q_ASSERT(false);
        return 0;
    }

    static GLenum To(VBlendEquation equation)
    {
        switch (equation)
        {
            case VBlendEquation::Add:
                return GL_FUNC_ADD;
            case VBlendEquation::Minimum:
                return GL_MIN;
            case VBlendEquation::Maximum:
                return GL_MAX;
            case VBlendEquation::Subtract:
                return GL_FUNC_SUBTRACT;
            case VBlendEquation::ReverseSubtract:
                return GL_FUNC_REVERSE_SUBTRACT;
        }

        Q_ASSERT(false);
        return 0;
    }

    static GLenum To(VRasterizationMode mode)
    {
        switch (mode)
        {
            case VRasterizationMode::Point:
                return GL_POINT;
            case VRasterizationMode::Line:
                return GL_LINE;
            case VRasterizationMode::Fill:
                return GL_FILL;
        }

        Q_ASSERT(false);
        return 0;
    }

    static VShaderVertexAttributeType ToShaderVertexAttributeType(GLenum type)
    {
        switch (type)
        {
            case GL_FLOAT:
                return VShaderVertexAttributeType::Float;
            case GL_FLOAT_VEC2:
                return VShaderVertexAttributeType::FloatVector2;
            case GL_FLOAT_VEC3:
                return VShaderVertexAttributeType::FloatVector3;
            case GL_FLOAT_VEC4:
                return VShaderVertexAttributeType::FloatVector4;
            case GL_FLOAT_MAT2:
                return VShaderVertexAttributeType::FloatMatrix22;
            case GL_FLOAT_MAT3:
                return VShaderVertexAttributeType::FloatMatrix33;
            case GL_FLOAT_MAT4:
                return VShaderVertexAttributeType::FloatMatrix44;
            case GL_INT:
                return VShaderVertexAttributeType::Int;
            case GL_INT_VEC2:
                return VShaderVertexAttributeType::IntVector2;
            case GL_INT_VEC3:
                return VShaderVertexAttributeType::IntVector3;
            case GL_INT_VEC4:
                return VShaderVertexAttributeType::IntVector4;
        }

        Q_ASSERT(false);
        return VShaderVertexAttributeType::Float;
    }

    static GLenum To(VPrimitiveType primitiveType)
    {
        switch (primitiveType)
        {
            case VPrimitiveType::Points:
                return GL_POINTS;
            case VPrimitiveType::Lines:
                return GL_LINES;
            case VPrimitiveType::LineLoop:
                return GL_LINE_LOOP;
            case VPrimitiveType::LineStrip:
                return GL_LINE_STRIP;
            case VPrimitiveType::Triangles:
                return GL_TRIANGLES;
            case VPrimitiveType::TriangleStrip:
                return GL_TRIANGLE_STRIP;
            case VPrimitiveType::LinesAdjacency:
                return GL_LINES_ADJACENCY;
            case VPrimitiveType::LineStripAdjacency:
                return GL_LINE_STRIP_ADJACENCY;
            case VPrimitiveType::TrianglesAdjacency:
                return GL_TRIANGLES_ADJACENCY;
            case VPrimitiveType::TriangleStripAdjacency:
                return GL_TRIANGLE_STRIP_ADJACENCY;
            case VPrimitiveType::TriangleFan:
                return GL_TRIANGLE_FAN;
        }

        Q_ASSERT(false);
        return 0;
    }

    static GLenum To(VIndicesType type)
    {
        switch (type)
        {
            case VIndicesType::UnsignedInt:
                return GL_UNSIGNED_INT;
            case VIndicesType::UnsignedShort:
                return GL_UNSIGNED_SHORT;
        }

        Q_ASSERT(false);
        return 0;
    }

    static GLenum To(VComponentDatatype type)
    {
        switch (type)
        {
            case VComponentDatatype::Byte:
                return GL_BYTE;
            case VComponentDatatype::UnsignedByte:
                return GL_UNSIGNED_BYTE;
            case VComponentDatatype::Short:
                return GL_SHORT;
            case VComponentDatatype::UnsignedShort:
                return GL_UNSIGNED_SHORT;
            case VComponentDatatype::Int:
                return GL_INT;
            case VComponentDatatype::UnsignedInt:
                return GL_UNSIGNED_INT;
            case VComponentDatatype::Float:
                return GL_FLOAT;
            case VComponentDatatype::HalfFloat:
                return GL_HALF_FLOAT;
        }

        Q_ASSERT(false);
        return 0;
    }
};

#endif // VTYPECONVERTERGL_H
