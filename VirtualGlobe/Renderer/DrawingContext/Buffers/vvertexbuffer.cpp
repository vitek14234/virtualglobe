#include "vvertexbuffer.h"

#include <QOpenGLFunctions>

VVertexBuffer::VVertexBuffer(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes)
    : VBuffer(QOpenGLBuffer::VertexBuffer)
{
    setUsagePattern(usageHint);
    create();
    allocate(sizeInBytes);
}

VVertexBuffer::VVertexBuffer()
    : VBuffer(QOpenGLBuffer::VertexBuffer)
{

}

QSharedPointer<VVertexBuffer> VVertexBuffer::instance(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes)
{
    return QSharedPointer<VVertexBuffer>::create(usageHint, sizeInBytes);
}

QSharedPointer<VVertexBuffer> VVertexBuffer::instance()
{
    return QSharedPointer<VVertexBuffer>::create();
}

void VVertexBuffer::unBind()
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    gl->glBindBuffer(GL_ARRAY_BUFFER, 0);
}
