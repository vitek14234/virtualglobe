#ifndef VINDEXBUFFER_H
#define VINDEXBUFFER_H

#include "vbuffer.h"
#include "vpointers.h"
#include "venmus.h"

class VIndexBuffer : public VBuffer
{
public:
    VIndexBuffer(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes);

    static QSharedPointer<VIndexBuffer> instance(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes);

    static void unBind();

    int count() const;
    VIndicesType dataType() const;
    void setType(VIndicesType type);
private:
    VIndicesType m_type;
};

#endif // VINDEXBUFFER_H
