#include "vindexbuffer.h"

#include <QOpenGLFunctions>

VIndexBuffer::VIndexBuffer(UsagePattern usageHint, size_t sizeInBytes)
    : VBuffer(QOpenGLBuffer::IndexBuffer)
    , m_type(VIndicesType::UnsignedInt)
{
    create();
    bind();
    setUsagePattern(usageHint);
    if (sizeInBytes > 0)
        allocate(sizeInBytes);
}

QSharedPointer<VIndexBuffer> VIndexBuffer::instance(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes)
{
    return QSharedPointer<VIndexBuffer>::create(usageHint, sizeInBytes);
}

void VIndexBuffer::unBind()
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

int VIndexBuffer::count() const
{
    int result = 0;

    if (m_type == VIndicesType::UnsignedInt)
        result = size() / sizeof(unsigned int);
    else if (m_type == VIndicesType::UnsignedShort)
        result = size() / sizeof(unsigned short);
    else
        Q_ASSERT(false);

    return result;
}

VIndicesType VIndexBuffer::dataType() const
{
    return m_type;
}

void VIndexBuffer::setType(VIndicesType type)
{
    m_type = type;
}
