#ifndef VBUFFER_H
#define VBUFFER_H

#include <QOpenGLBuffer>
#include <vpointers.h>

class VBuffer : public QOpenGLBuffer
{
public:
    VBuffer(QOpenGLBuffer::Type type);
};

#endif // VBUFFER_H
