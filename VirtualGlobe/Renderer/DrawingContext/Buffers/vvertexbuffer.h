#ifndef VVERTEXBUFFER_H
#define VVERTEXBUFFER_H

#include "vbuffer.h"
#include "vpointers.h"

class VVertexBuffer : public VBuffer
{
public:
    VVertexBuffer(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes);
    VVertexBuffer();
    static QSharedPointer<VVertexBuffer> instance(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes);
    static QSharedPointer<VVertexBuffer> instance();
    void unBind();
};

#endif // VVERTEXBUFFER_H
