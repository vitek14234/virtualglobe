#ifndef VDEVICE_H
#define VDEVICE_H

#include <QOpenGLBuffer>
#include "vpointers.h"
#include "Renderer/vshadervertexattributecollection.h"
#include "Renderer/Shaders/LinkAutomaticUniforms/vlinkautomaticuniforms.h"
#include "Renderer/Shaders/DrawAutomaticUniforms/vdrawautomaticuniformfactorycollection.h"

class VDevice
{
public:
    static void init();
    static VMeshBuffersPtr createMeshBuffers(const VMeshPtr& mesh,
                                      const VShaderVertexAttributeCollection& shaderAttributes,
                                      QOpenGLBuffer::UsagePattern usageHint);
    static VVertexArrayPtr createVertexArray(const VMeshPtr& mesh,
                                      const VShaderVertexAttributeCollection& shaderAttributes,
                                      QOpenGLBuffer::UsagePattern usageHint);
    static VVertexArrayPtr createVertexArray(const VMeshBuffersPtr& meshBuffers);
    static VVertexArrayObjectPtr createVertexArrayObject(const VMeshPtr &mesh, const VShaderVertexAttributeCollection &shaderAttributes, QOpenGLBuffer::UsagePattern usageHint);
    static VIndexBufferPtr createIndexBuffer(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes);
    static VVertexBufferPtr createVertexBuffer(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes);
    static VVertexBufferPtr createVertexBuffer(QOpenGLBuffer::UsagePattern usageHint, const VIVertexAttributePtr& attribute);
    static VTexture2DPtr createTexture(const QImage &image);
    static VIndexBufferPtr createIndexBuffer(QOpenGLBuffer::UsagePattern usageHint, const VIIndicesPtr &indices);
    static const VLinkAutomaticUniformCollection &linkAutomaticUniforms();
    static const VDrawAutomaticUniformFactoryCollection &drawAutomaticUniformFactoryCollection();
};

#endif // VDEVICE_H
