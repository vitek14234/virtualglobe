#ifndef RENDERSTRUCTS_H
#define RENDERSTRUCTS_H

#include <venmus.h>
#include <QRect>
#include <QColor>

struct VPrimitiveRestart
{
    bool enabled = false;
    int index = 0;
};

struct VFacetCulling
{
    bool enabled = true;
    VCullFace face = VCullFace::Back;
    VWindingOrder frontFaceWindingOrder = VWindingOrder::Counterclockwise;
};

struct VScissorTest
{
    bool enabled = false;
    QRect rectangle;
};

struct VStencilTestFace
{
    VStencilOperation stencilFailOperation = VStencilOperation::Keep;
    VStencilOperation depthFailStencilPassOperation = VStencilOperation::Keep;
    VStencilOperation depthPassStencilPassOperation = VStencilOperation::Keep;

    VStencilTestFunction function = VStencilTestFunction::Always;
    int referenceValue = 0;
    int mask = 0;
};

struct VStencilTest
{
    bool enabled = false;
    VStencilTestFace frontFace;
    VStencilTestFace backFace;
};

struct VDepthTest
{
    bool enabled = true;
    VDepthTestFunction function = VDepthTestFunction::Less;
};

struct VDepthRange
{
    double m_near = 0.0;
    double m_far = 1.0;
};

struct VBlending
{
    bool enabled = false;
    VSourceBlendingFactor sourceRGBFactor = VSourceBlendingFactor::One;
    VSourceBlendingFactor sourceAlphaFactor = VSourceBlendingFactor::One;
    VDestinationBlendingFactor destinationRGBFactor = VDestinationBlendingFactor::Zero;
    VDestinationBlendingFactor destinationAlphaFactor = VDestinationBlendingFactor::Zero;
    VBlendEquation RGBEquation = VBlendEquation::Add;
    VBlendEquation alphaEquation = VBlendEquation::Add;
    QColor color;
};

class VColorMask
{
public:
    Q_DECL_CONSTEXPR VColorMask(bool red, bool green, bool blue, bool alpha)
        : m_red(red)
        , m_green(green)
        , m_blue(blue)
        , m_alpha(alpha)
    {

    }

    Q_DECL_CONSTEXPR friend inline bool operator==(const VColorMask& lhs, const VColorMask& rhs)
    {
        return lhs.m_red == rhs.m_red &&
               lhs.m_green == rhs.m_green &&
               lhs.m_blue == rhs.m_blue &&
               lhs.m_alpha == rhs.m_alpha;
    }

    Q_DECL_CONSTEXPR friend inline bool operator!=(const VColorMask& lhs, const VColorMask& rhs)
    {
        return !(lhs == rhs);
    }

    bool red() const
    {
        return m_red;
    }

    bool green() const
    {
        return m_green;
    }

    bool blue() const
    {
        return m_blue;
    }

    bool alpha() const
    {
        return m_alpha;
    }
private:
    bool m_red;
    bool m_green;
    bool m_blue;
    bool m_alpha;
};

#endif // RENDERSTRUCTS_H
