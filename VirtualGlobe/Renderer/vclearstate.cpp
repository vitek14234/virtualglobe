#include "vclearstate.h"

QSharedPointer<VClearState> VClearState::instance()
{
    return QSharedPointer<VClearState>::create();
}
