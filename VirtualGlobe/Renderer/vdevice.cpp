#include "vdevice.h"
#include <QOpenGLFunctions>
#include "Core/Geometry/vmesh.h"
#include "Renderer/vdevice.h"
#include "Renderer/Mesh/vmeshbuffers.h"
#include "Renderer/DrawingContext/Buffers/vindexbuffer.h"
#include "Renderer/DrawingContext/Buffers/vvertexbuffer.h"
#include "Renderer/DrawingContext/vtypeconvertergl.h"
#include "Renderer/VertexArray/vvertexarray.h"
#include "Renderer/VertexArray/vvertexbufferattributes.h"
#include "Renderer/VertexArray/vvertexbufferattribute.h"
#include "Renderer/VertexArray/vvertexarrayobject.h"
#include "Renderer/Textures/vtexture2d.h"
#include "Renderer/Shaders/vshadervertexattribute.h"
#include "Renderer/Shaders/LinkAutomaticUniforms/vlinkautomaticuniforms.h"
#include "Renderer/Shaders/LinkAutomaticUniforms/vlinkautomaticuniform.h"
#include "Renderer/Shaders/DrawAutomaticUniforms/vdrawautomaticuniforms.h"
#include "Renderer/Shaders/DrawAutomaticUniforms/vdrawautomaticuniformfactory.h"
#include "Core/Geometry/Indices/viindices.h"
#include "Core/Geometry/VertexAttributes/vvertexattribute.h"
#include "Core/Geometry/VertexAttributes/vvertexattributecollection.h"

namespace
{
struct DeviceData
{
    VLinkAutomaticUniformCollection linkAutomaticUniforms;
    VDrawAutomaticUniformFactoryCollection drawAutomaticUniformFactories;
};

static DeviceData s_deviceData;
}

void VDevice::init()
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    int numberOfTextureUnits = 0;
    gl->glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &numberOfTextureUnits);

    s_deviceData.linkAutomaticUniforms.clear();

    for (int i = 0; i < numberOfTextureUnits; ++i)
    {
        VLinkAutomaticUniformPtr textureUniform = VTextureUniform::instance(i);
        s_deviceData.linkAutomaticUniforms.insert(textureUniform->name(), textureUniform);
    }

    s_deviceData.drawAutomaticUniformFactories.clear();
    s_deviceData.drawAutomaticUniformFactories.insert("og_sunPosition", VSunPositionUniformFactory::instance());
    s_deviceData.drawAutomaticUniformFactories.insert("og_diffuseSpecularAmbientShininess", VLightPropertiesUniformFactory::instance());
    s_deviceData.drawAutomaticUniformFactories.insert("og_cameraEye", VCameraEyeUniformFactory::instance());
    s_deviceData.drawAutomaticUniformFactories.insert("og_modelViewPerspectiveMatrix", VModelViewPerspectiveMatrixUniformFactory::instance());
}

VMeshBuffersPtr VDevice::createMeshBuffers(const VMeshPtr &mesh,
                                           const VShaderVertexAttributeCollection &shaderAttributes,
                                           QOpenGLBuffer::UsagePattern usageHint)
{
    if (!mesh)
    {
        Q_ASSERT(false);
        return Q_NULLPTR;
    }

    VMeshBuffersPtr meshBuffers = VMeshBuffers::instance();

    if (mesh->indices())
    {
        if (mesh->indices()->datatype() == VIndicesType::UnsignedShort)
        {
            const auto& meshIndices = qSharedPointerCast<VIndicesUnsignedShort>(mesh->indices())->values();

            VIndexBufferPtr indexBuffer = VDevice::createIndexBuffer(usageHint, meshIndices.size() * sizeof(unsigned short));
            indexBuffer->write(0, meshIndices.data(), meshIndices.size() * sizeof(unsigned short));
            indexBuffer->setType(VIndicesType::UnsignedShort);
            meshBuffers->setIndexBuffer(indexBuffer);
        }
        else if (mesh->indices()->datatype() == VIndicesType::UnsignedInt)
        {
            const auto& meshIndices = qSharedPointerCast<VIndicesUnsignedInt>(mesh->indices())->values();

            VIndexBufferPtr indexBuffer = VDevice::createIndexBuffer(usageHint, meshIndices.size() * sizeof(unsigned int));
            indexBuffer->write(0, meshIndices.data(), meshIndices.size() * sizeof(unsigned int));
            indexBuffer->setType(VIndicesType::UnsignedInt);
            meshBuffers->setIndexBuffer(indexBuffer);
        }
        else
        {
            Q_ASSERT(false);
        }
    }

    for (const VShaderVertexAttribute& shaderAttribute : shaderAttributes)
    {
//        if (ignoreAttributes.Contains(shaderAttribute.Name))
//        {
//            continue;
//        }

//        if (!mesh.Attributes.Contains(shaderAttribute.Name))
//        {
//            throw new ArgumentException("Shader requires vertex attribute \"" + shaderAttribute.Name + "\", which is not present in mesh.");
//        }

        const VIVertexAttributePtr attribute = (*mesh->attributes().data())[shaderAttribute.name()];

        switch (attribute->datatype()) {
        case VVertexAttributeType::EmulatedDoubleVector3:
        {
            const VVertexAttributeDoubleVector3Ptr& vectorAttribute = attribute.staticCast<VVertexAttributeDoubleVector3>();
            const auto values = vectorAttribute->values();

            QVector<QVector3D> valuesArray(values.size());
            for(int i = 0; i < values.size(); ++i)
            {
                valuesArray[i] = values[i].toQVector3D();
            }

            const VVertexBufferPtr vertexBuffer = VDevice::createVertexBuffer(usageHint, 3 * sizeof(float) * valuesArray.size());
            vertexBuffer->bind();
            vertexBuffer->allocate(valuesArray.data(), 3 * sizeof(float) * valuesArray.size());
            meshBuffers->attributes()->set(shaderAttribute.location(),VVertexBufferAttribute::instance(vertexBuffer, VComponentDatatype::Float, 3));
            break;
        }
        default:
            Q_ASSERT(false);
            break;
        }


    }

    return meshBuffers;
}

VVertexArrayPtr VDevice::createVertexArray(const VMeshPtr &mesh, const VShaderVertexAttributeCollection &shaderAttributes, QOpenGLBuffer::UsagePattern usageHint)
{
    return createVertexArray(createMeshBuffers(mesh, shaderAttributes, usageHint));
}

VVertexArrayPtr VDevice::createVertexArray(const VMeshBuffersPtr &meshBuffers)
{
    VVertexArrayPtr va = VVertexArray::instance();

    //va.DisposeBuffers = true;
    va->setIndexBuffer(meshBuffers->indexBuffer());
    for (size_t i = 0; i < meshBuffers->attributes()->maximumCount(); ++i)
    {
        va->attributes()->set(i, meshBuffers->attributes()->get(i));
    }

    return va;
}

VVertexArrayObjectPtr VDevice::createVertexArrayObject(const VMeshPtr &mesh, const VShaderVertexAttributeCollection &shaderAttributes, QOpenGLBuffer::UsagePattern usageHint)
{
    VVertexArrayObjectPtr vao = VVertexArrayObject::instance();
    vao->create();
    QOpenGLVertexArrayObject::Binder vaoBinder(vao.data());
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();

    for (const VShaderVertexAttribute& shaderAttribute : shaderAttributes)
    {
        const VIVertexAttributePtr attribute = (*mesh->attributes().data())[shaderAttribute.name()];
        VVertexBufferPtr vertexBuffer = createVertexBuffer(usageHint, attribute);

        vao->addBuffer(vertexBuffer);
        VVertexBufferAttributePtr bufferAttribute = VVertexBufferAttribute::instance(vertexBuffer, VComponentDatatype::Float, 3);

        gl->glEnableVertexAttribArray(shaderAttribute.location());
        gl->glVertexAttribPointer(
                    shaderAttribute.location(),
                    bufferAttribute->numberOfComponents(),
                    VTypeConverterGL::To(bufferAttribute->componentDatatype()),
                    bufferAttribute->normalize(),
                    /*bufferAttribute->strideInBytes()*/ 0,
                    /*(void*)bufferAttribute->offsetInBytes()*/ Q_NULLPTR);
    }

    if (mesh->indices())
        vao->setIndexBuffer(createIndexBuffer(usageHint, mesh->indices()));

    return vao;
}

VIndexBufferPtr VDevice::createIndexBuffer(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes)
{
    return VIndexBuffer::instance(usageHint, sizeInBytes);
}

VVertexBufferPtr VDevice::createVertexBuffer(QOpenGLBuffer::UsagePattern usageHint, size_t sizeInBytes)
{
    return VVertexBuffer::instance(usageHint, sizeInBytes);
}

VVertexBufferPtr VDevice::createVertexBuffer(QOpenGLBuffer::UsagePattern usageHint, const VIVertexAttributePtr& attribute)
{
    Q_ASSERT(attribute);
    if (!attribute)
        return Q_NULLPTR;

    switch (attribute->datatype()) {
    case VVertexAttributeType::EmulatedDoubleVector3:
    {
        const VVertexAttributeDoubleVector3Ptr& vectorAttribute = attribute.staticCast<VVertexAttributeDoubleVector3>();
        const auto values = vectorAttribute->values();

        QVector<QVector3D> valuesArray(values.size());
        for(int i = 0; i < values.size(); ++i)
        {
            valuesArray[i] = values[i].toQVector3D();
        }

        const VVertexBufferPtr vertexBuffer = VVertexBuffer::instance();
        vertexBuffer->create();
        vertexBuffer->bind();
        vertexBuffer->setUsagePattern(usageHint);
        vertexBuffer->allocate(valuesArray.data(), 3 * sizeof(GLfloat) * valuesArray.size());
        return vertexBuffer;
    }
    default:
        Q_ASSERT(false);
        return Q_NULLPTR;
    }
}

VTexture2DPtr VDevice::createTexture(const QImage &image)
{
    VTexture2DPtr texture = VTexture2D::instance();
    texture->setData(image);
    return texture;
}

VIndexBufferPtr VDevice::createIndexBuffer(QOpenGLBuffer::UsagePattern usageHint, const VIIndicesPtr &indices)
{
    if (!indices)
        return Q_NULLPTR;

    if (indices->datatype() == VIndicesType::UnsignedShort)
    {
        const auto& meshIndices = qSharedPointerCast<VIndicesUnsignedShort>(indices)->values();

        VIndexBufferPtr indexBuffer = VIndexBuffer::instance(usageHint, 0);
        indexBuffer->allocate(meshIndices.data(), meshIndices.size() * sizeof(unsigned short));
        indexBuffer->setType(VIndicesType::UnsignedShort);
        return indexBuffer;
    }
    else if (indices->datatype() == VIndicesType::UnsignedInt)
    {
        const auto& meshIndices = qSharedPointerCast<VIndicesUnsignedInt>(indices)->values();

        VIndexBufferPtr indexBuffer = VIndexBuffer::instance(usageHint, 0);
        indexBuffer->allocate(meshIndices.data(), meshIndices.size() * sizeof(unsigned int));
        indexBuffer->setType(VIndicesType::UnsignedInt);
        return indexBuffer;
    }

    Q_ASSERT(false);
    return Q_NULLPTR;
}

const VLinkAutomaticUniformCollection &VDevice::linkAutomaticUniforms()
{
    return s_deviceData.linkAutomaticUniforms;
}

const VDrawAutomaticUniformFactoryCollection &VDevice::drawAutomaticUniformFactoryCollection()
{
    return s_deviceData.drawAutomaticUniformFactories;
}
