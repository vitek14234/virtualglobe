#include "vdrawstate.h"
#include "RenderState/vrenderstate.h"

VDrawState::VDrawState()
    : m_renderState(VRenderState::instance())
{

}

VDrawState::VDrawState(const VRenderStatePtr &renderState, const VShaderProgramPtr &shaderProgram, const VVertexArrayPtr &vertexArray)
    : m_renderState(renderState)
    , m_shaderProgram(shaderProgram)
    , m_vertexArray(vertexArray)
{

}

VDrawState::VDrawState(const VRenderStatePtr &renderState, const VShaderProgramPtr &shaderProgram, const VVertexArrayObjectPtr &vertexArrayObject)
    : m_renderState(renderState)
    , m_shaderProgram(shaderProgram)
    , m_vertexArrayObject(vertexArrayObject)
{

}

QSharedPointer<VDrawState> VDrawState::instance()
{
    return QSharedPointer<VDrawState>::create();
}

QSharedPointer<VDrawState> VDrawState::instance(const VRenderStatePtr &renderState, const VShaderProgramPtr &shaderProgram, const VVertexArrayPtr &vertexArray)
{
    return QSharedPointer<VDrawState>::create(renderState, shaderProgram, vertexArray);
}

QSharedPointer<VDrawState> VDrawState::instance(const VRenderStatePtr &renderState, const VShaderProgramPtr &shaderProgram, const VVertexArrayObjectPtr &vertexArrayObject)
{
    return QSharedPointer<VDrawState>::create(renderState, shaderProgram, vertexArrayObject);
}

const VRenderStatePtr &VDrawState::renderState() const
{
    return m_renderState;
}

const VShaderProgramPtr &VDrawState::shaderProgram() const
{
    return m_shaderProgram;
}

const VVertexArrayPtr &VDrawState::vertexArray() const
{
    return m_vertexArray;
}

const VVertexArrayObjectPtr &VDrawState::vertexArrayObject() const
{
    return m_vertexArrayObject;
}
