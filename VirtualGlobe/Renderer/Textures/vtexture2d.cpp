#include "vtexture2d.h"

VTexture2D::VTexture2D()
    : QOpenGLTexture(QOpenGLTexture::Target2D)
{

}

VTexture2DPtr VTexture2D::instance()
{
    return QSharedPointer<VTexture2D>::create();
}
