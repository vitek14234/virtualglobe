#ifndef VTEXTURE2D_H
#define VTEXTURE2D_H

#include "vpointers.h"
#include <QOpenGLTexture>

class VTexture2D : public QOpenGLTexture
{
public:
    VTexture2D();
    static VTexture2DPtr instance();
};

#endif // VTEXTURE2D_H
