#ifndef VTEXTUREUNITS_H
#define VTEXTUREUNITS_H

#include "vpointers.h"
#include <QVector>

class VTextureUnits
{
public:
    VTextureUnits();

    static VTextureUnitsPtr instance();

    QVector<VTexture2DPtr> &textureUnits();
    const QVector<VTexture2DPtr> &textureUnits() const;
    void clean();
private:
    QVector<VTexture2DPtr> m_textureUnits;
};

#endif // VTEXTUREUNITS_H
