#include "vtextureunits.h"

#include <QOpenGLContext>
#include <QOpenGLFunctions>

#include "Renderer/Textures/vtexture2d.h"

VTextureUnits::VTextureUnits()
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    int numberOfTextureUnits = 0;
    gl->glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &numberOfTextureUnits);

    m_textureUnits.resize(numberOfTextureUnits);
    for (int i = 0; i < numberOfTextureUnits; ++i)
    {
        m_textureUnits[i] = VTexture2D::instance();
    }
}

VTextureUnitsPtr VTextureUnits::instance()
{
    return QSharedPointer<VTextureUnits>::create();
}

QVector<VTexture2DPtr> &VTextureUnits::textureUnits()
{
    return m_textureUnits;
}

const QVector<VTexture2DPtr> &VTextureUnits::textureUnits() const
{
    return m_textureUnits;
}

void VTextureUnits::clean()
{
    for(const VTexture2DPtr& texture : m_textureUnits)
    {
        if (texture)
        {
            const bool hasTexture = texture->width() > 2;
            if (hasTexture)
                texture->bind();
        }
    }
}
