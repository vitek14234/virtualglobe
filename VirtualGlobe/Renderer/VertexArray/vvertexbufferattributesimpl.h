#ifndef VVERTEXBUFFERATTRIBUTESIMPL_H
#define VVERTEXBUFFERATTRIBUTESIMPL_H

#include "vpointers.h"
#include "venmus.h"
#include "Renderer/VertexArray/vvertexbufferattributes.h"

#include <QVector>

class VVertexBufferAttributesImpl : public VVertexBufferAttributes
{
public:
    VVertexBufferAttributesImpl();

    static VVertexBufferAttributesPtr instance();

    int maximumArrayIndex() const;
    void clean();
    void attach(int index);
    void detach(int index);

    // VVertexBufferAttributes interface
    VVertexBufferAttributePtr get(size_t index) const Q_DECL_OVERRIDE;
    void set(size_t index, const VVertexBufferAttributePtr &attribute) Q_DECL_OVERRIDE;
    size_t count() const Q_DECL_OVERRIDE;
    size_t maximumCount() const Q_DECL_OVERRIDE;
private:
    static int numberOfVertices(const VVertexBufferAttributePtr &attribute);

    struct VertexBufferAttributeInternal
    {
        VVertexBufferAttributePtr vertexBufferAttribute;
        bool dirty = true;
    };

    QVector<VertexBufferAttributeInternal> m_attributes;
    int m_count;
    int m_maximumArrayIndex;
    bool m_dirty;
};

#endif // VVERTEXBUFFERATTRIBUTESIMPL_H
