#ifndef VVERTEXARRAYOBJECT_H
#define VVERTEXARRAYOBJECT_H

#include "venmus.h"
#include "vpointers.h"
#include <QOpenGLVertexArrayObject>
#include <QVector>

class VVertexArrayObject : public QOpenGLVertexArrayObject
{
public:
    VVertexArrayObject();
    void addBuffer(const VVertexBufferPtr& bufffer);

    static VVertexArrayObjectPtr instance();

    VIndexBufferPtr indexBuffer() const;
    void setIndexBuffer(const VIndexBufferPtr &indexBuffer);
private:
    QVector<VVertexBufferPtr> m_vertexBuffers;
    VIndexBufferPtr m_indexBuffer;
};

#endif // VVERTEXARRAYOBJECT_H
