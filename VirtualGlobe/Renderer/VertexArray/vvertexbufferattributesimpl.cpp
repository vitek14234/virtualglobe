#include "vvertexbufferattributesimpl.h"

#include <QOpenGLFunctions>

#include "Renderer/vglfactory.h"
#include "Renderer/VertexArray/vvertexbufferattribute.h"
#include "Renderer/DrawingContext/Buffers/vvertexbuffer.h"
#include "Renderer/DrawingContext/vtypeconvertergl.h"

VVertexBufferAttributesImpl::VVertexBufferAttributesImpl()
    : m_count(0)
    , m_maximumArrayIndex(0)
    , m_dirty(true)
{
    m_attributes.resize(VGLFactory::maximumNumberOfVertexAttributes());
}

VVertexBufferAttributesPtr VVertexBufferAttributesImpl::instance()
{
    return QSharedPointer<VVertexBufferAttributesImpl>::create();
}

int VVertexBufferAttributesImpl::maximumArrayIndex() const
{
    Q_ASSERT(!m_dirty);
    return m_maximumArrayIndex;
}

void VVertexBufferAttributesImpl::clean()
{
    if (m_dirty)
    {
        int maximumArrayIndex = 0;

        for (int i = 0; i < m_attributes.size(); ++i)
        {
            const VVertexBufferAttributePtr& attribute = m_attributes[i].vertexBufferAttribute;

            if (m_attributes[i].dirty)
            {
                if (attribute != Q_NULLPTR)
                {
                    attach(i);
                }
                else
                {
                    detach(i);
                }

                m_attributes[i].dirty = false;
            }

            if (attribute != Q_NULLPTR)
            {
                maximumArrayIndex = std::max(numberOfVertices(attribute) - 1, maximumArrayIndex);
            }
        }

        m_dirty = false;
        m_maximumArrayIndex = maximumArrayIndex;
    }
}

void VVertexBufferAttributesImpl::attach(int index)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    gl->glEnableVertexAttribArray(index);
    const VVertexBufferAttributePtr& attribute = m_attributes[index].vertexBufferAttribute;
    const VVertexBufferPtr& bufferObject = attribute->vertexBuffer();
    bufferObject->bind();
    gl->glVertexAttribPointer(
                index,
                attribute->numberOfComponents(),
                VTypeConverterGL::To(attribute->componentDatatype()),
                attribute->normalize(),
                attribute->strideInBytes(),
                (void*)attribute->offsetInBytes());
}

void VVertexBufferAttributesImpl::detach(int index)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    gl->glDisableVertexAttribArray(index);
}

VVertexBufferAttributePtr VVertexBufferAttributesImpl::get(size_t index) const
{
    return m_attributes[index].vertexBufferAttribute;
}

void VVertexBufferAttributesImpl::set(size_t index, const VVertexBufferAttributePtr &value)
{
    if ((m_attributes[index].vertexBufferAttribute != Q_NULLPTR) && (value == Q_NULLPTR))
    {
        --m_count;
    }
    else if ((m_attributes[index].vertexBufferAttribute == Q_NULLPTR) && (value != Q_NULLPTR))
    {
        ++m_count;
    }

    m_attributes[index].vertexBufferAttribute = value;
    m_attributes[index].dirty = true;
}

size_t VVertexBufferAttributesImpl::count() const
{
    return m_count;
}

size_t VVertexBufferAttributesImpl::maximumCount() const
{
    return m_attributes.size();
}

int VVertexBufferAttributesImpl::numberOfVertices(const VVertexBufferAttributePtr& attribute)
{
    return attribute->vertexBuffer()->size() / attribute->strideInBytes();
}
