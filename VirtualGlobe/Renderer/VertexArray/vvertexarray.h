#ifndef VVERTEXARRAY_H
#define VVERTEXARRAY_H

#include "venmus.h"
#include "vpointers.h"
#include <QOpenGLVertexArrayObject>

class VVertexArray : public QOpenGLVertexArrayObject
{
public:
    VVertexArray();
    static VVertexArrayPtr instance();

    const VVertexBufferAttributesPtr &attributes() const;
    const VIndexBufferPtr &indexBuffer() const;
    void setIndexBuffer(const VIndexBufferPtr& buffer);
    void clean();
    int maximumArrayIndex() const;
private:
    VVertexBufferAttributesPtr m_attributes;
    VIndexBufferPtr m_indexBuffer;
    bool m_dirtyIndexBuffer;
};

#endif // VVERTEXARRAY_H
