#include "vvertexarray.h"

#include "Renderer/VertexArray/vvertexbufferattributesimpl.h"
#include "Renderer/DrawingContext/Buffers/vindexbuffer.h"

VVertexArray::VVertexArray()
    : QOpenGLVertexArrayObject()
    , m_dirtyIndexBuffer(false)
{
    m_attributes = VVertexBufferAttributesImpl::instance();
}

VVertexArrayPtr VVertexArray::instance()
{
    return QSharedPointer<VVertexArray>::create();
}

const VVertexBufferAttributesPtr &VVertexArray::attributes() const
{
    return m_attributes;
}

const VIndexBufferPtr &VVertexArray::indexBuffer() const
{
    return m_indexBuffer;
}

void VVertexArray::setIndexBuffer(const VIndexBufferPtr &buffer)
{
    Q_ASSERT(buffer);
    m_indexBuffer = buffer;
}

void VVertexArray::clean()
{
    static_cast<VVertexBufferAttributesImpl*>(m_attributes.data())->clean();

    if (true/*m_dirtyIndexBuffer*/)
    {
        if (m_indexBuffer)
        {
            m_indexBuffer->bind();
        }
        else
        {
            VIndexBuffer::unBind();
        }

        m_dirtyIndexBuffer = false;
    }
}

int VVertexArray::maximumArrayIndex() const
{
    return static_cast<VVertexBufferAttributesImpl*>(m_attributes.data())->maximumArrayIndex();
}
