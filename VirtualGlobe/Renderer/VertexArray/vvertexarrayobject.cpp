#include "vvertexarrayobject.h"

VVertexArrayObject::VVertexArrayObject()
    : QOpenGLVertexArrayObject()
{

}

void VVertexArrayObject::addBuffer(const VVertexBufferPtr &bufffer)
{
    m_vertexBuffers.push_back(bufffer);
}

VVertexArrayObjectPtr VVertexArrayObject::instance()
{
    return VVertexArrayObjectPtr::create();
}

VIndexBufferPtr VVertexArrayObject::indexBuffer() const
{
    return m_indexBuffer;
}

void VVertexArrayObject::setIndexBuffer(const VIndexBufferPtr &indexBuffer)
{
    m_indexBuffer = indexBuffer;
}
