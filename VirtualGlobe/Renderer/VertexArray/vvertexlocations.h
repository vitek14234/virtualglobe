#ifndef VVERTEXLOCATIONS_H
#define VVERTEXLOCATIONS_H

class VVertexLocations
{
public:
    Q_DECL_CONSTEXPR static int Position = 0;
    Q_DECL_CONSTEXPR static int Normal = 2;
    Q_DECL_CONSTEXPR static int TextureCoordinate = 3;
    Q_DECL_CONSTEXPR static int Color = 4;
    Q_DECL_CONSTEXPR static int PositionHigh = Position;
    Q_DECL_CONSTEXPR static int PositionLow = 1;
};

#endif // VVERTEXLOCATIONS_H
