#ifndef VVERTEXARRAYSIZES_H
#define VVERTEXARRAYSIZES_H

#include <venmus.h>

class VVertexArraySizes
{
public:
    static int sizeOf(VComponentDatatype type)
    {
        switch (type)
        {
            case VComponentDatatype::Byte:
            case VComponentDatatype::UnsignedByte:
                return sizeof(char);
            case VComponentDatatype::Short:
                return sizeof(short);
            case VComponentDatatype::UnsignedShort:
                return sizeof(unsigned short);
            case VComponentDatatype::Int:
                return sizeof(int);
            case VComponentDatatype::UnsignedInt:
                return sizeof(unsigned int);
            case VComponentDatatype::Float:
                return sizeof(float);
            case VComponentDatatype::HalfFloat:
                Q_ASSERT(false);
                return sizeof(float) / 2;
        }

        Q_ASSERT(false);
        return 0;
    }
};

#endif // VVERTEXARRAYSIZES_H
