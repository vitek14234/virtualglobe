#ifndef VVERTEXBUFFERATTRIBUTE_H
#define VVERTEXBUFFERATTRIBUTE_H

#include "vpointers.h"
#include "venmus.h"

class VVertexBufferAttribute
{
public:
    VVertexBufferAttribute(const VVertexBufferPtr &vertexBuffer,
                           VComponentDatatype componentDatatype,
                           int numberOfComponents);

    VVertexBufferAttribute(const VVertexBufferPtr &vertexBuffer,
                           VComponentDatatype componentDatatype,
                           int numberOfComponents,
                           bool normalize,
                           int offsetInBytes,
                           int strideInBytes);

    static VVertexBufferAttributePtr instance(const VVertexBufferPtr &vertexBuffer,
                                              VComponentDatatype componentDatatype,
                                              int numberOfComponents);

    const VVertexBufferPtr &vertexBuffer() const;
    VComponentDatatype componentDatatype() const;
    int numberOfComponents() const;
    bool normalize() const;
    int offsetInBytes() const;
    int strideInBytes() const;
private:
    VVertexBufferPtr m_vertexBuffer;
    VComponentDatatype m_componentDatatype;
    int m_numberOfComponents;
    bool m_normalize;
    int m_offsetInBytes;
    int m_strideInBytes;
};

#endif // VVERTEXBUFFERATTRIBUTE_H
