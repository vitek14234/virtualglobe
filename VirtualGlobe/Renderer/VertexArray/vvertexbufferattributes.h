#ifndef VVERTEXBUFFERATTRIBUTES_H
#define VVERTEXBUFFERATTRIBUTES_H

#include "vpointers.h"

class VVertexBufferAttributes
{
public:
    virtual VVertexBufferAttributePtr get(size_t index) const = 0;
    virtual void set(size_t index, const VVertexBufferAttributePtr& attribute) = 0;
    virtual size_t count() const = 0;
    virtual size_t maximumCount() const = 0;
};

#endif // VVERTEXBUFFERATTRIBUTES_H
