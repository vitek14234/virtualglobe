#include "vvertexbufferattribute.h"

#include "vvertexarraysizes.h"

VVertexBufferAttribute::VVertexBufferAttribute(const VVertexBufferPtr &vertexBuffer, VComponentDatatype componentDatatype, int numberOfComponents)
    : VVertexBufferAttribute(vertexBuffer, componentDatatype, numberOfComponents, false, 0, 0)
{

}

VVertexBufferAttribute::VVertexBufferAttribute(const VVertexBufferPtr &vertexBuffer, VComponentDatatype componentDatatype, int numberOfComponents, bool normalize, int offsetInBytes, int strideInBytes)
{
    Q_ASSERT(numberOfComponents > 0);
    Q_ASSERT(offsetInBytes >= 0);
    Q_ASSERT(strideInBytes >= 0);

    m_vertexBuffer = vertexBuffer;
    m_componentDatatype = componentDatatype;
    m_numberOfComponents = numberOfComponents;
    m_normalize = normalize;
    m_offsetInBytes = offsetInBytes;

    if (strideInBytes == 0)
    {
        //
        // Tightly packed
        //
        m_strideInBytes = numberOfComponents * VVertexArraySizes::sizeOf(componentDatatype);
    }
    else
    {
        m_strideInBytes = strideInBytes;
    }
}

VVertexBufferAttributePtr VVertexBufferAttribute::instance(const VVertexBufferPtr &vertexBuffer, VComponentDatatype componentDatatype, int numberOfComponents)
{
    return VVertexBufferAttributePtr::create(vertexBuffer, componentDatatype, numberOfComponents);
}

const VVertexBufferPtr &VVertexBufferAttribute::vertexBuffer() const
{
    return m_vertexBuffer;
}

VComponentDatatype VVertexBufferAttribute::componentDatatype() const
{
    return m_componentDatatype;
}

int VVertexBufferAttribute::numberOfComponents() const
{
    return m_numberOfComponents;
}

bool VVertexBufferAttribute::normalize() const
{
    return m_normalize;
}

int VVertexBufferAttribute::offsetInBytes() const
{
    return m_offsetInBytes;
}

int VVertexBufferAttribute::strideInBytes() const
{
    return m_strideInBytes;
}
