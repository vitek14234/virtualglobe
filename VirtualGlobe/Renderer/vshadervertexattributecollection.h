#ifndef VSHADERVERTEXATTRIBUTECOLLECTION_H
#define VSHADERVERTEXATTRIBUTECOLLECTION_H

#include <QHash>
#include "Renderer/Shaders/vshadervertexattribute.h"

class VShaderVertexAttributeCollection : public QHash<QString, VShaderVertexAttribute>
{

};

#endif // VSHADERVERTEXATTRIBUTECOLLECTION_H
