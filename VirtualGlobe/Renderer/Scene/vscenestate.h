#ifndef VSCENESTATE_H
#define VSCENESTATE_H

#include "vpointers.h"
#include "../../Core/Matrices/vmatrix4.h"
#include "vcamera.h"

class QRect;

class VSceneState
{
public:
    VSceneState();

    static QSharedPointer<VSceneState> instance();

    float diffuseIntensity() const;
    void setDiffuseIntensity(float diffuseIntensity);

    float specularIntensity() const;
    void setSpecularIntensity(float specularIntensity);

    float ambientIntensity() const;
    void setAmbientIntensity(float ambientIntensity);

    float shininess() const;
    void setShininess(float shininess);

    VCamera &camera();
    const VCamera &camera() const;

    const VVector3D &sunPosition() const;
    void setSunPosition(const VVector3D &sunPosition);

    double highResolutionSnapScale() const;
    void setHighResolutionSnapScale(double highResolutionSnapScale);

    VVector3D cameraLightPosition() const;

    VMatrix4D computeViewportTransformationMatrix(const QRect &viewport, double nearDepthRange, double farDepthRange);

    static VMatrix4D computeViewportOrthographicMatrix(const QRect &viewport);

    VMatrix4D orthographicMatrix() const;

    VMatrix4D perspectiveMatrix() const;

    VMatrix4D viewMatrix() const;

    const VMatrix4D &modelMatrix() const;
    void setModelMatrix(const VMatrix4D &modelMatrix);

    const VMatrix4D modelViewMatrix() const;

    const VMatrix4D modelViewMatrixRelativeToEye() const;

    const VMatrix4D modelViewPerspectiveMatrixRelativeToEye() const;

    const VMatrix4D modelViewPerspectiveMatrix() const;

    const VMatrix4D modelViewOrthographicMatrix() const;

private:
    float m_diffuseIntensity;
    float m_specularIntensity;
    float m_ambientIntensity;
    float m_shininess;
    VCamera m_camera;
    VVector3D m_sunPosition;
    VMatrix4D m_modelMatrix;
    double m_highResolutionSnapScale;
};

#endif // SCENESTATE_H
