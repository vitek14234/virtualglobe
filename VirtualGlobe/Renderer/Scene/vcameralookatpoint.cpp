#include "vcameralookatpoint.h"

#include <QOpenGLWidget>
#include <QMouseEvent>
#include <cfloat>
#include <qmath.h>
#include <QDebug>

#include "Renderer/Scene/vcamera.h"

QSharedPointer<VCameraLookAtPoint> VCameraLookAtPoint::instance(QOpenGLWidget *window, VCamera &camera, const VEllipsoid &ellipsoid)
{
    return QSharedPointer<VCameraLookAtPoint>::create(window, camera, ellipsoid);
}

VCameraLookAtPoint::VCameraLookAtPoint(QOpenGLWidget *window, VCamera &camera, const VEllipsoid &ellipsoid)
    : m_window(window)
    , m_camera(camera)
    , m_ellipsoid(ellipsoid)
    , m_centerPoint(camera.target())
    , m_zoomFactor(5.0)
    , m_zoomRateRangeAdjustment(ellipsoid.maximumRadius())
    , m_maximumZoomRate(DBL_MAX)
    , m_minimumZoomRate(ellipsoid.maximumRadius() / 100.0)
    , m_rotateFactor(1.0 / ellipsoid.maximumRadius())
    , m_rotateRateRangeAdjustment(ellipsoid.maximumRadius())
    , m_maximumRotateRate(1.0)
    , m_minimumRotateRate(1.0 / 5000.0)
    , m_range(2.0 * ellipsoid.maximumRadius())
    , m_azimuth(0.0)
    , m_elevation(0.0)
{

}

const VCamera &VCameraLookAtPoint::camera() const
{
    return m_camera;
}

QOpenGLWidget *VCameraLookAtPoint::window() const
{
    return m_window;
}

double VCameraLookAtPoint::zoomFactor() const
{
    return m_zoomFactor;
}

void VCameraLookAtPoint::setZoomFactor(double value)
{
    m_zoomFactor = value;
}

double VCameraLookAtPoint::zoomRateRangeAdjustment() const
{
    return m_zoomRateRangeAdjustment;
}

void VCameraLookAtPoint::setZoomRateRangeAdjustment(double value)
{
    m_zoomRateRangeAdjustment = value;
}

double VCameraLookAtPoint::rotateRateRangeAdjustment() const
{
    return m_rotateRateRangeAdjustment;
}

void VCameraLookAtPoint::setRotateRateRangeAdjustment(double value)
{
    m_rotateRateRangeAdjustment = value;
}

double VCameraLookAtPoint::rotateFactor() const
{
    return m_rotateFactor;
}

void VCameraLookAtPoint::setRotateFactor(double value)
{
    m_rotateFactor = value;
}

double VCameraLookAtPoint::minimumRotateRate() const
{
    return m_minimumRotateRate;
}

void VCameraLookAtPoint::setMinimumRotateRate(double value)
{
    m_maximumRotateRate = value;
}

double VCameraLookAtPoint::maximumRotateRate() const
{
    return m_maximumRotateRate;
}

void VCameraLookAtPoint::setMaximumRotateRate(double value)
{
    m_maximumRotateRate = value;
}

double VCameraLookAtPoint::maximumZoomRate() const
{
    return m_maximumZoomRate;
}

void VCameraLookAtPoint::setMaximumZoomRate(double value)
{
    m_maximumZoomRate = value;
}

double VCameraLookAtPoint::minimumZoomRate() const
{
    return m_minimumZoomRate;
}

void VCameraLookAtPoint::setMinimumZoomRate(double value)
{
    m_maximumZoomRate = value;
}

double VCameraLookAtPoint::azimuth() const
{
    return m_azimuth;
}

void VCameraLookAtPoint::setAzimuth(double value)
{
    m_azimuth = value;
    updateCameraFromParameters();
}

double VCameraLookAtPoint::elevation() const
{
    return m_elevation;
}

void VCameraLookAtPoint::setElevation(double value)
{
    m_elevation = value;
    updateCameraFromParameters();
}

double VCameraLookAtPoint::range() const
{
    return m_range;
}

void VCameraLookAtPoint::setRange(double value)
{
    m_range = value;
    updateCameraFromParameters();
}

const VVector3D &VCameraLookAtPoint::centerPoint() const
{
    return m_centerPoint;
}

void VCameraLookAtPoint::setCenterPoint(const VVector3D &point)
{
    m_centerPoint = point;
    updateCameraFromParameters();
}

const VMatrix3D &VCameraLookAtPoint::fixedToLocalRotation() const
{
    return m_fixedToLocalRotation;
}

void VCameraLookAtPoint::setFixedToLocalRotation(const VMatrix3D &matrix)
{
    m_fixedToLocalRotation = matrix;
    updateCameraFromParameters();
}

void VCameraLookAtPoint::viewPoint(const VEllipsoid &ellipsoid, const QGeoCoordinate &geographic)
{
    //TODO remove ellipsoid
    m_centerPoint = ellipsoid.toVector3D(geographic);

    const double cosLon = cos(qDegreesToRadians(geographic.longitude()));
    const double cosLat = cos(qDegreesToRadians(geographic.latitude()));
    const double sinLon = sin(qDegreesToRadians(geographic.longitude()));
    const double sinLat = sin(qDegreesToRadians(geographic.latitude()));
    //TODO
    //        m_fixedToLocalRotation[0][0]
    //            new Matrix3D(-sinLon,            cosLon,             0.0,
    //                         -sinLat * cosLon,   -sinLat * sinLon,   cosLat,
    //                         cosLat * cosLon,    cosLat * sinLon,    sinLat);

    updateCameraFromParameters();
}

void VCameraLookAtPoint::mousePressEvent(QMouseEvent *ev)
{
    m_lastPoint = ev->pos();
}

void VCameraLookAtPoint::mouseMoveEvent(QMouseEvent *ev)
{
    updateParametersFromCamera();

    const QPoint movement = ev->pos() - m_lastPoint;
    qDebug() << "movement" << movement << m_camera;

    if (ev->buttons() & Qt::LeftButton)
        rotate(movement);

    if (ev->buttons() & Qt::RightButton)
        zoom(movement);

    updateCameraFromParameters();

    m_lastPoint = ev->pos();
}

void VCameraLookAtPoint::mouseReleaseEvent(QMouseEvent *ev)
{

}

void VCameraLookAtPoint::updateParametersFromCamera()
{
    const VVector3D eyePosition = m_fixedToLocalRotation * (m_camera.eye() - m_camera.target());
    const VVector3D up = m_fixedToLocalRotation * m_camera.up();

    m_range = sqrt(eyePosition.x() * eyePosition.x() + eyePosition.y() * eyePosition.y() + eyePosition.z() * eyePosition.z());
    m_elevation = asin(eyePosition.z() / m_range);

    if (eyePosition.toVector2().lengthSquared() < up.toVector2().lengthSquared())
    {
        // Near the poles, determine the azimuth from the Up direction instead of from the Eye position.
        if (eyePosition.z() > 0.0)
        {
            m_azimuth = atan2(-up.y(), -up.x());
        }
        else
        {
            m_azimuth = atan2(up.y(), up.x());
        }
    }
    else
    {
        m_azimuth = atan2(eyePosition.y(), eyePosition.x());
    }
}

void VCameraLookAtPoint::updateCameraFromParameters()
{
    m_camera.setTarget(m_centerPoint);

    const double rangeTimesSinElevation = m_range * cos(m_elevation);
    m_camera.setEye(VVector3D(rangeTimesSinElevation * cos(m_azimuth),
                              rangeTimesSinElevation * sin(m_azimuth),
                              m_range * sin(m_elevation)));

    const VVector3D right = m_camera.eye().crossProduct(VVector3D::unitZ());
    m_camera.setUp(right.crossProduct(m_camera.eye()).normalized());

    if (m_camera.up().isUndefined())
    {
        // Up vector is invalid because _camera.Eye is all Z (or very close to it).
        // So compute the Up vector directly assuming no Z component.
        m_camera.setUp(VVector3D(-cos(m_azimuth), -sin(m_azimuth), 0.0));
    }

    const VMatrix3D localToFixed = m_fixedToLocalRotation.transposed();
    m_camera.setEye(localToFixed * m_camera.eye());
    m_camera.setEye(m_camera.eye() + m_centerPoint);
    m_camera.setUp(localToFixed * m_camera.up());
}

void VCameraLookAtPoint::rotate(const QPoint &movement)
{
    double rotateRate = m_rotateFactor * (m_range - m_rotateRateRangeAdjustment);
    if (rotateRate > m_maximumRotateRate)
        rotateRate = m_maximumRotateRate;
    else if (rotateRate < m_minimumRotateRate)
        rotateRate = m_minimumRotateRate;

    const double azimuthWindowRatio = (double)movement.x() / (double)m_window->width();
    const double elevationWindowRatio = (double)movement.y() / (double)m_window->height();

    m_azimuth -= rotateRate * azimuthWindowRatio * 2 * M_PI;
    m_elevation += rotateRate * elevationWindowRatio * M_PI;

    while (m_azimuth > M_PI)
        m_azimuth -= (2 * M_PI);
    while (m_azimuth < -M_PI)
        m_azimuth += (2 * M_PI);

    if (m_elevation < -M_PI_2)
        m_elevation = -M_PI_2;
    else if (m_elevation > M_PI_2)
        m_elevation = M_PI_2;
}

void VCameraLookAtPoint::zoom(const QPoint &movement)
{
    double zoomRate = m_zoomFactor * (m_range - m_zoomRateRangeAdjustment);
    if (zoomRate > m_maximumZoomRate)
        zoomRate = m_maximumZoomRate;
    if (zoomRate < m_minimumZoomRate)
        zoomRate = m_minimumZoomRate;

    double rangeWindowRatio = (double)movement.y() / (double)m_window->height();
    m_range -= zoomRate * rangeWindowRatio;
}
