#include "vcamera.h"

#include "../../Core/Geometry/vellipsoid.h"

#include <cmath>

VCamera::VCamera()
    : m_eye(-VVector3D::unitY())
    , m_target(VVector3D::zero())
    , m_up(VVector3D::unitZ())
    , m_fieldOfViewY(M_PI / 6.0)
    , m_aspectRatio(1.0)
    , m_perspectiveNearPlaneDistance(0.01)
    , m_perspectiveFarPlaneDistance(64.0)
    , m_orthographicNearPlaneDistance(0.0)
    , m_orthographicFarPlaneDistance(1.0)
    , m_orthographicLeft(0.0)
    , m_orthographicRight(1.0)
    , m_orthographicBottom(0.0)
    , m_orthographicTop(1.0)
{

}

const VVector3D &VCamera::eye() const
{
    return m_eye;
}

void VCamera::setEye(const VVector3D &eye)
{
    m_eye = eye;
}

const VVector3D &VCamera::target() const
{
    return m_target;
}

void VCamera::setTarget(const VVector3D &target)
{
    m_target = target;
}

const VVector3D &VCamera::up() const
{
    return m_up;
}

void VCamera::setUp(const VVector3D &up)
{
    m_up = up;
}

const VVector3F VCamera::eyeHigh() const
{
    return VVector3F(m_eye.x(), m_eye.y(), m_eye.z());
}

const VVector3F VCamera::eyeLow() const
{
    return VVector3F(m_eye.x() - (float)m_eye.x(),
                     m_eye.y() - (float)m_eye.y(),
                     m_eye.z() - (float)m_eye.z());
}

const VVector3D VCamera::forward() const
{
    return (m_target - m_eye).normalized();
}

const VVector3D VCamera::right() const
{
    return forward().crossProduct(m_up).normalized();
}

double VCamera::fieldOfViewX() const
{
    return (2.0 * atan(m_aspectRatio * tan(m_fieldOfViewY * 0.5)));
}

double VCamera::fieldOfViewY() const
{
    return m_fieldOfViewY;
}

void VCamera::setFieldOfViewY(double fieldOfViewY)
{
    m_fieldOfViewY = fieldOfViewY;
}

double VCamera::aspectRatio() const
{
    return m_aspectRatio;
}

void VCamera::setAspectRatio(double ratio)
{
    m_aspectRatio = ratio;
}

double VCamera::perspectiveNearPlaneDistance() const
{
    return m_perspectiveNearPlaneDistance;
}

void VCamera::setPerspectiveNearPlaneDistance(double perspectiveNearPlaneDistance)
{
    m_perspectiveNearPlaneDistance = perspectiveNearPlaneDistance;
}

double VCamera::perspectiveFarPlaneDistance() const
{
    return m_perspectiveFarPlaneDistance;
}

void VCamera::setPerspectiveFarPlaneDistance(double perspectiveFarPlaneDistance)
{
    m_perspectiveFarPlaneDistance = perspectiveFarPlaneDistance;
}

double VCamera::orthographicNearPlaneDistance() const
{
    return m_orthographicNearPlaneDistance;
}

double VCamera::orthographicFarPlaneDistance() const
{
    return m_orthographicFarPlaneDistance;
}

double VCamera::orthographicDepth() const
{
    return fabs(m_orthographicFarPlaneDistance - m_orthographicNearPlaneDistance);
}

double VCamera::orthographicLeft() const
{
    return m_orthographicLeft;
}

double VCamera::orthographicRight() const
{
    return m_orthographicRight;
}

double VCamera::orthographicBottom() const
{
    return m_orthographicBottom;
}

double VCamera::orthographicTop() const
{
    return m_orthographicTop;
}

void VCamera::zoomToTarget(double radius)
{
    const VVector3D toEye = (m_eye - m_target).normalized();
    const double sin = std::sin(std::min(fieldOfViewX(), m_fieldOfViewY) * 0.5);
    double distance = (radius / sin);
    m_eye = m_target + (distance * toEye);
}

double VCamera::height(const VEllipsoid &shape) const
{
    return shape.toGeodetic3D(m_eye).altitude();
}

QDebug operator<<(QDebug d, const VCamera &camera)
{
    d << "eye" << camera.eye()
    << "target" << camera.target()
    << "up" << camera.up();
    return d;
}
