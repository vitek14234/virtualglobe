#include "vscenestate.h"

#include <QRect>

VSceneState::VSceneState()
    : m_diffuseIntensity(0.65f)
    , m_specularIntensity(0.25f)
    , m_ambientIntensity(0.10f)
    , m_shininess(12.0f)
    , m_sunPosition(200000.0, 0.0, 0.0)
    , m_modelMatrix(VMatrix4D::identity())
    , m_highResolutionSnapScale(1.0)
{

}

QSharedPointer<VSceneState> VSceneState::instance()
{
    return QSharedPointer<VSceneState>::create();
}

float VSceneState::diffuseIntensity() const
{
    return m_diffuseIntensity;
}

void VSceneState::setDiffuseIntensity(float diffuseIntensity)
{
    m_diffuseIntensity = diffuseIntensity;
}

float VSceneState::specularIntensity() const
{
    return m_specularIntensity;
}

void VSceneState::setSpecularIntensity(float specularIntensity)
{
    m_specularIntensity = specularIntensity;
}

float VSceneState::ambientIntensity() const
{
    return m_ambientIntensity;
}

void VSceneState::setAmbientIntensity(float ambientIntensity)
{
    m_ambientIntensity = ambientIntensity;
}

float VSceneState::shininess() const
{
    return m_shininess;
}

void VSceneState::setShininess(float shininess)
{
    m_shininess = shininess;
}

VCamera &VSceneState::camera()
{
    return m_camera;
}

const VCamera &VSceneState::camera() const
{
    return m_camera;
}

const VVector3D &VSceneState::sunPosition() const
{
    return m_sunPosition;
}

void VSceneState::setSunPosition(const VVector3D &sunPosition)
{
    m_sunPosition = sunPosition;
}

double VSceneState::highResolutionSnapScale() const
{
    return m_highResolutionSnapScale;
}

void VSceneState::setHighResolutionSnapScale(double highResolutionSnapScale)
{
    m_highResolutionSnapScale = highResolutionSnapScale;
}

VVector3D VSceneState::cameraLightPosition() const
{
    return m_camera.eye();
}

VMatrix4D VSceneState::computeViewportTransformationMatrix(const QRect &viewport, double nearDepthRange, double farDepthRange)
{
    const double halfWidth = viewport.width() * 0.5;
    const double halfHeight = viewport.height() * 0.5;
    const double halfDepth = (farDepthRange - nearDepthRange) * 0.5;

    //
    // Bottom and top swapped:  MS -> OpenGL
    //
    return VMatrix4D(
                halfWidth, 0.0,        0.0,       viewport.left() + halfWidth,
                0.0,       halfHeight, 0.0,       viewport.top() + halfHeight,
                0.0,       0.0,        halfDepth, nearDepthRange + halfDepth,
                0.0,       0.0,        0.0,       1.0);
}

VMatrix4D VSceneState::computeViewportOrthographicMatrix(const QRect &viewport)
{
    VMatrix4D orthographic;
    //
    // Bottom and top swapped:  MS -> OpenGL
    //
    orthographic.ortho(viewport.left(), viewport.right(), viewport.top(), viewport.bottom(), 0.0, 1.0);
    return orthographic;
}

VMatrix4D VSceneState::orthographicMatrix() const
{
    VMatrix4D orthographic;
    //
    // Bottom and top swapped:  MS -> OpenGL
    //
    orthographic.ortho(m_camera.orthographicLeft(),
                       m_camera.orthographicRight(),
                       m_camera.orthographicTop(),
                       m_camera.orthographicBottom(),
                       m_camera.orthographicNearPlaneDistance(),
                       m_camera.orthographicFarPlaneDistance());
    return orthographic;
}

VMatrix4D VSceneState::perspectiveMatrix() const
{
    VMatrix4D perspective;
    perspective.perspective(qRadiansToDegrees(m_camera.fieldOfViewY()),
                            m_camera.aspectRatio(),
                            m_camera.perspectiveNearPlaneDistance(),
                            m_camera.perspectiveFarPlaneDistance());

    return perspective;
}

VMatrix4D VSceneState::viewMatrix() const
{    
    VMatrix4D viewMatrix;
    viewMatrix.lookAt(m_camera.eye(), m_camera.target(), m_camera.up());

    return viewMatrix;
}

const VMatrix4D &VSceneState::modelMatrix() const
{
    return m_modelMatrix;
}

void VSceneState::setModelMatrix(const VMatrix4D &modelMatrix)
{
    m_modelMatrix = modelMatrix;
}

const VMatrix4D VSceneState::modelViewMatrix() const
{
    return viewMatrix() * m_modelMatrix;
}

const VMatrix4D VSceneState::modelViewMatrixRelativeToEye() const
{
    VMatrix4D m = modelViewMatrix();
    m(0,3) = 0.0;
    m(1,3) = 0.0;
    m(2,3) = 0.0;
    return m;
}

const VMatrix4D VSceneState::modelViewPerspectiveMatrixRelativeToEye() const
{
    return perspectiveMatrix() * modelViewMatrixRelativeToEye();
}

const VMatrix4D VSceneState::modelViewPerspectiveMatrix() const
{
    const VMatrix4D result = perspectiveMatrix() * modelViewMatrix();
    return result.transposed();
}

const VMatrix4D VSceneState::modelViewOrthographicMatrix() const
{
    return modelViewMatrix() * orthographicMatrix();
}
