#ifndef CAMERALOOKATPOINT_H
#define CAMERALOOKATPOINT_H

#include <QPoint>
#include "vpointers.h"
#include "Core/Vectors/vvector3.h"
#include "Core/Matrices/vmatrix3.h"
#include "Core/Geometry/vellipsoid.h"

class QOpenGLWidget;
class QGeoCoordinate;
class QMouseEvent;
class VCamera;

class VCameraLookAtPoint
{
public:
    static QSharedPointer<VCameraLookAtPoint> instance(QOpenGLWidget *window, VCamera &camera, const VEllipsoid &ellipsoid);

    VCameraLookAtPoint(QOpenGLWidget *window, VCamera &camera, const VEllipsoid &ellipsoid);

    const VCamera &camera() const;
    QOpenGLWidget *window() const;

    double zoomFactor() const;
    void setZoomFactor(double value);

    double zoomRateRangeAdjustment() const;
    void setZoomRateRangeAdjustment(double value);

    double rotateRateRangeAdjustment() const;
    void setRotateRateRangeAdjustment(double value);

    double rotateFactor() const;
    void setRotateFactor(double value);

    double minimumRotateRate() const;
    void setMinimumRotateRate(double value);

    double maximumRotateRate() const;
    void setMaximumRotateRate(double value);

    double maximumZoomRate() const;
    void setMaximumZoomRate(double value);

    double minimumZoomRate() const;
    void setMinimumZoomRate(double value);

    double azimuth() const;
    void setAzimuth(double value);

    double elevation() const;
    void setElevation(double value);

    double range() const;
    void setRange(double value);

    const VVector3D &centerPoint() const;
    void setCenterPoint(const VVector3D &point);

    const VMatrix3D &fixedToLocalRotation() const;
    void setFixedToLocalRotation(const VMatrix3D &matrix);

    void viewPoint(const VEllipsoid &ellipsoid, const QGeoCoordinate &geographic);

    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
private:
    void updateParametersFromCamera();
    void updateCameraFromParameters();
    void rotate(const QPoint &movement);
    void zoom(const QPoint &movement);

    QOpenGLWidget *m_window;
    VCamera &m_camera;
    VEllipsoid m_ellipsoid;

    double m_zoomFactor;
    double m_zoomRateRangeAdjustment;
    double m_maximumZoomRate;
    double m_minimumZoomRate;

    double m_rotateFactor;
    double m_rotateRateRangeAdjustment;
    double m_minimumRotateRate;
    double m_maximumRotateRate;

    QPoint m_lastPoint;

    double m_azimuth;
    double m_elevation;
    double m_range;

    VVector3D m_centerPoint;
    VMatrix3D m_fixedToLocalRotation;
};

#endif // CAMERALOOKATPOINT_H
