#ifndef VCAMERA_H
#define VCAMERA_H

#include <QDebug>
#include "Core/Vectors/vvector3.h"

class VEllipsoid;

class VCamera
{
public:
    VCamera();

    const VVector3D &eye() const;
    void setEye(const VVector3D &eye);

    const VVector3D &target() const;
    void setTarget(const VVector3D& target);

    const VVector3D &up() const;
    void setUp(const VVector3D &up);

    const VVector3F eyeHigh() const;
    //TODO check
    const VVector3F eyeLow() const;
    const VVector3D forward() const;
    const VVector3D right() const;

    double fieldOfViewX() const;
    double fieldOfViewY() const;
    void setFieldOfViewY(double fieldOfViewY);
    double aspectRatio() const;
    void setAspectRatio(double ratio);

    double perspectiveNearPlaneDistance() const;
    void setPerspectiveNearPlaneDistance(double perspectiveNearPlaneDistance);
    double perspectiveFarPlaneDistance() const;
    void setPerspectiveFarPlaneDistance(double perspectiveFarPlaneDistance);
    double orthographicNearPlaneDistance() const;
    double orthographicFarPlaneDistance() const;
    double orthographicDepth() const;
    double orthographicLeft() const;
    double orthographicRight() const;
    double orthographicBottom() const;
    double orthographicTop() const;

    void zoomToTarget(double radius);
    double height(const VEllipsoid &shape) const;
private:
    VVector3D m_eye;
    VVector3D m_target;
    VVector3D m_up;

    double m_fieldOfViewY;
    double m_aspectRatio;

    double m_perspectiveNearPlaneDistance;
    double m_perspectiveFarPlaneDistance;

    double m_orthographicNearPlaneDistance;
    double m_orthographicFarPlaneDistance;

    double m_orthographicLeft;
    double m_orthographicRight;
    double m_orthographicBottom;
    double m_orthographicTop;
};

QDebug operator<<(QDebug d, const VCamera &camera);

#endif // VCAMERA_H
