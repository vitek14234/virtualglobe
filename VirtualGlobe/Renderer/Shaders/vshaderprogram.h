#ifndef VSHADERPROGRAM_H
#define VSHADERPROGRAM_H

#include <QOpenGLShaderProgram>
#include "vpointers.h"
#include "Renderer/vshadervertexattributecollection.h"
#include "Renderer/Shaders/vuniformcollection.h"

class VShaderProgram : public QOpenGLShaderProgram
{
public:
    VShaderProgram(const QString& vertexShaderSource, const QString& fragmentShaderSource);
    VShaderProgram(const QString& vertexShaderSource, const QString& geometryShaderSource, const QString& fragmentShaderSource);
    const VShaderVertexAttributeCollection &vertexAttributes() const;
    void clean(const VDrawingContextPtr& context, const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState);
private:
    void setDrawAutomaticUniforms(const VDrawingContextPtr& context, const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState);
    void initializeAutomaticUniforms();
    void createVertexAttributes();
    void createUniforms();
    QString modifySource(const QString& source) const;
    VIUniformPtr createUniform(const QString& name, VShaderProgram& shader, GLenum type) const;

    VShaderVertexAttributeCollection m_vertexAttributes;
    VUniformCollection m_uniforms;
    QVector<VDrawAutomaticUniformPtr> m_drawAutomaticUniforms;
};

#endif // VSHADERPROGRAM_H
