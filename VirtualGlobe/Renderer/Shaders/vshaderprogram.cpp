#include "vshaderprogram.h"
#include <limits>
#include <QDebug>
#include <QtMath>
#include <QOpenGLFunctions>
#include "vutils.h"
#include "Renderer/vdevice.h"
#include "Renderer/VertexArray/vvertexlocations.h"
#include "Renderer/Shaders/viuniform.h"
#include "Renderer/Shaders/LinkAutomaticUniforms/vlinkautomaticuniform.h"
#include "Renderer/Shaders/DrawAutomaticUniforms/vdrawautomaticuniformfactory.h"
#include "Renderer/DrawingContext/vtypeconvertergl.h"
#include "Core/vtrig.h"
#include "Core/Geometry/VertexAttributes/vvertexattributecollection.h"

VShaderProgram::VShaderProgram(const QString &vertexShaderSource,
                               const QString &fragmentShaderSource)
    : VShaderProgram(vertexShaderSource, QString(), fragmentShaderSource)
{

}

VShaderProgram::VShaderProgram(const QString &vertexShaderSource,
                               const QString &geometryShaderSource,
                               const QString &fragmentShaderSource)
    : QOpenGLShaderProgram()
{
    addShaderFromSourceCode(QOpenGLShader::Vertex, modifySource(vertexShaderSource));

    if (!geometryShaderSource.isEmpty())
    {
        addShaderFromSourceCode(QOpenGLShader::Geometry, modifySource(geometryShaderSource));
    }

    addShaderFromSourceCode(QOpenGLShader::Fragment, modifySource(fragmentShaderSource));

    const bool linkStatus = link();
    Q_ASSERT(linkStatus);

    createVertexAttributes();
    createUniforms();
    initializeAutomaticUniforms();
}

const VShaderVertexAttributeCollection &VShaderProgram::vertexAttributes() const
{
    return m_vertexAttributes;
}

void VShaderProgram::clean(const VDrawingContextPtr& context, const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState)
{
    setDrawAutomaticUniforms(context, drawState, sceneState);

//    foreach (VIUniformPtr uniform, m_uniforms)
//    {
//        uniform->
//    }
}

void VShaderProgram::setDrawAutomaticUniforms(const VDrawingContextPtr &context, const VDrawStatePtr &drawState, const VSceneStatePtr &sceneState)
{
    for (const VDrawAutomaticUniformPtr& uniform: m_drawAutomaticUniforms)
    {
        uniform->set(context, drawState, sceneState);
    }
}

void VShaderProgram::initializeAutomaticUniforms()
{
    const VLinkAutomaticUniformCollection &linkAutomaticUniforms = VDevice::linkAutomaticUniforms();
    const VDrawAutomaticUniformFactoryCollection &drawAutomaticUniformFactoryCollection = VDevice::drawAutomaticUniformFactoryCollection();
    foreach (VIUniformPtr uniform, m_uniforms)
    {
        if (linkAutomaticUniforms.contains(uniform->name()))
        {
            linkAutomaticUniforms.value(uniform->name())->set(uniform);
        }
        else if (drawAutomaticUniformFactoryCollection.contains(uniform->name()))
        {
            auto automaticUniform = drawAutomaticUniformFactoryCollection.value(uniform->name())->create(uniform);
            m_drawAutomaticUniforms.push_back(automaticUniform);
        }
    }
}

void VShaderProgram::createVertexAttributes()
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();

    int numberOfAttributes = 0;
    gl->glGetProgramiv(programId(), GL_ACTIVE_ATTRIBUTES, &numberOfAttributes);

    int attributeNameMaxLength = 0;
    gl->glGetProgramiv(programId(), GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &attributeNameMaxLength);

    QVector<GLchar> attrNameBuffer(attributeNameMaxLength);

    m_vertexAttributes.clear();
    for (int i = 0; i < numberOfAttributes; ++i)
    {
        GLsizei attributeNameLength = 0;
        GLint attributeLength = 0;
        GLenum attributeType = 0;

        gl->glGetActiveAttrib(programId(), i, attributeNameMaxLength,
                              &attributeNameLength, &attributeLength, &attributeType, attrNameBuffer.data());

        const QByteArray attributeName(attrNameBuffer.data(), attributeNameLength);

        if (attributeName.startsWith("gl_"))
        {
            //
            // Names starting with the reserved prefix of "gl_" have a location of -1.
            //
            continue;
        }

        m_vertexAttributes.insert(attributeName, VShaderVertexAttribute(attributeName, attributeLocation(attributeName), VTypeConverterGL::ToShaderVertexAttributeType(attributeType), attributeLength));
    }
}

void VShaderProgram::createUniforms()
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();

    int numberOfUniforms = 0;
    gl->glGetProgramiv(programId(), GL_ACTIVE_UNIFORMS, &numberOfUniforms);

    int uniformNameMaxLength = 0;
    gl->glGetProgramiv(programId(), GL_ACTIVE_UNIFORM_MAX_LENGTH, &uniformNameMaxLength);

    //UniformCollection uniforms = new UniformCollection();
    QVector<GLchar> uniformNameBuffer(uniformNameMaxLength);
    for (int i = 0; i < numberOfUniforms; ++i)
    {
        int uniformNameLength = 0;
        int uniformSize = 0;
        GLenum uniformType = 0;
        gl->glGetActiveUniform(programId(), i, uniformNameMaxLength, &uniformNameLength, &uniformSize, &uniformType, uniformNameBuffer.data());

        const QByteArray uniformName(uniformNameBuffer.data(), uniformNameLength);
        if (uniformName.startsWith("gl_"))
        {
            //
            // Names starting with the reserved prefix of "gl_" have a location of -1.
            //
            continue;
        }

        if (uniformSize != 1)
        {
            // TODO:  Support arrays
            Q_ASSERT(false);
            continue;
        }

        const VIUniformPtr uniform = createUniform(uniformName, *this, uniformType);
        m_uniforms.insert(uniformName, uniform);
    }
}

QString VShaderProgram::modifySource(const QString &source) const
{
    const QString builtinConstants =
        QString("#version 330 \n") +
        "#extension GL_ARB_separate_shader_objects : enable \n" +
        "#define og_positionVertexLocation          " + QString::number(VVertexLocations::Position) + " \n" +
        "#define og_normalVertexLocation            " + QString::number(VVertexLocations::Normal) + " \n" +
        "#define og_textureCoordinateVertexLocation " + QString::number(VVertexLocations::TextureCoordinate) + + " \n" +
        "#define og_colorVertexLocation             " + QString::number(VVertexLocations::Color) + " \n" +
        "#define og_positionHighVertexLocation      " + QString::number(VVertexLocations::PositionHigh) + " \n" +
        "#define og_positionLowVertexLocation       " + QString::number(VVertexLocations::PositionLow) + " \n" +

        "const float og_E =                " + QString::number(M_E) + "; \n" +
        "const float og_pi =               " + QString::number(M_PI) + "; \n" +
        "const float og_oneOverPi =        " + QString::number(VTrig::OneOverPi) + "; \n" +
        "const float og_piOverTwo =        " + QString::number(VTrig::PiOverTwo) + "; \n" +
        "const float og_piOverThree =      " + QString::number(VTrig::PiOverThree) + "; \n" +
        "const float og_piOverFour =       " + QString::number(VTrig::PiOverFour) + "; \n" +
        "const float og_piOverSix =        " + QString::number(VTrig::PiOverSix) + "; \n" +
        "const float og_threePiOver2 =     " + QString::number(VTrig::ThreePiOver2) + "; \n" +
        "const float og_twoPi =            " + QString::number(VTrig::TwoPi) + "; \n" +
        "const float og_oneOverTwoPi =     " + QString::number(VTrig::OneOverTwoPi) + "; \n" +
        "const float og_radiansPerDegree = " + QString::number(VTrig::RadiansPerDegree) + "; \n" +
        "const float og_maximumFloat =     " + QString::number(std::numeric_limits<float>::max()) + "; \n" +
        "const float og_minimumFloat =     " + QString::number(std::numeric_limits<float>::min()) + "; \n";

    const QString builtinFunctions = VUtils::getFileSource(":/shaders/Shaders/BuiltinFunctions.glsl");

    return builtinConstants + builtinFunctions + source;
}

VIUniformPtr VShaderProgram::createUniform(const QString &name, VShaderProgram &shader, GLenum type) const
{
    switch (type) {
    case GL_FLOAT:
        return UniformFloatGL::instance(name, shader, VUniformType::Float);
    case GL_FLOAT_VEC3:
        return UniformFloatVec3GL::instance(name, shader, VUniformType::FloatVector3);
    case GL_FLOAT_VEC4:
        return UniformFloatVec4GL::instance(name, shader, VUniformType::FloatVector4);
    case GL_FLOAT_MAT4:
        return UniformFloatMat4GL::instance(name, shader, VUniformType::FloatMatrix44);
    case GL_SAMPLER_2D:
        return UniformIntGL::instance(name, shader, VUniformType::Sampler2D);
    }
    Q_ASSERT(false);
    return Q_NULLPTR;
}
