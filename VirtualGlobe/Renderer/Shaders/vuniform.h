#ifndef VUNIFORM_H
#define VUNIFORM_H

#include "vshaderprogram.h"

template <class T>
class VUniform
{
public:
    VUniform()
        : m_shader(Q_NULLPTR)
        , m_uniformLocation(0)
    {

    }
    VUniform(const QString& name, VShaderProgram& shader)
        : m_name(name)
        , m_shader(&shader)
    {
        m_uniformLocation = m_shader->uniformLocation(m_name);
    }

    void setValue(const T& type)
    {
        Q_CHECK_PTR(m_shader);
        m_shader->setUniformValue(m_uniformLocation, type);
    }
private:
    int m_uniformLocation;
    QString m_name;
    VShaderProgram* m_shader;
};

#endif // VUNIFORM_H
