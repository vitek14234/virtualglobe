#ifndef VLINKAUTOMATICUNIFORM_H
#define VLINKAUTOMATICUNIFORM_H

#include <venmus.h>
#include <vpointers.h>
#include <Renderer/Shaders/viuniform.h>

class VLinkAutomaticUniform
{
public:
    virtual QString name() const = 0;
    virtual void set(const VIUniformPtr& uniform) = 0;
};

class VTextureUniform : public VLinkAutomaticUniform
{
public:
    static QSharedPointer<VLinkAutomaticUniform> instance(int textureUnit)
    {
        return QSharedPointer<VTextureUniform>::create(textureUnit);
    }

    VTextureUniform(int textureUnit)
        : m_textureUnit(textureUnit)
    {
    }

    QString name() const Q_DECL_OVERRIDE
    {
        return QString("og_texture%1").arg(m_textureUnit);
    }

    void set(const VIUniformPtr& uniform) Q_DECL_OVERRIDE
    {
        static_cast<UniformIntGL*>(uniform.data())->setValue(m_textureUnit);
    }

    int m_textureUnit;
};

#endif // VLINKAUTOMATICUNIFORM_H
