#include "vshadervertexattribute.h"

VShaderVertexAttribute::VShaderVertexAttribute(const VShaderVertexAttribute &rhs)
    : m_name(rhs.m_name)
    , m_location(rhs.m_location)
    , m_type(rhs.m_type)
    , m_length(rhs.m_length)
{

}

VShaderVertexAttribute::VShaderVertexAttribute(const QString &name, int location, VShaderVertexAttributeType type, int length)
    : m_name(name)
    , m_location(location)
    , m_type(type)
    , m_length(length)
{

}

const QString &VShaderVertexAttribute::name() const
{
    return m_name;
}

int VShaderVertexAttribute::location() const
{
    return m_location;
}

VShaderVertexAttributeType VShaderVertexAttribute::datatype() const
{
    return m_type;
}

int VShaderVertexAttribute::length() const
{
    return m_length;
}
