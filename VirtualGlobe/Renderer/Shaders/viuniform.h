#ifndef VIUNIFORM_H
#define VIUNIFORM_H

#include <venmus.h>
#include <vpointers.h>
#include <Core/Vectors/vvector3.h>
#include <Core/Matrices/vmatrix4.h>
#include "vshaderprogram.h"

class VIUniform
{
protected:
    VIUniform(const QString& name, VUniformType type)
        : m_name(name)
        , m_type(type)
    {

    }
public:
    const QString& name() const
    {
        return m_name;
    }

    VUniformType datatype() const
    {
        return m_type;
    }
private:
    QString m_name;
    VUniformType m_type;
};

template <class T>
class VGLUniform : public VIUniform
{
public:
    static QSharedPointer<VIUniform> instance(const QString& name, VShaderProgram& shader, VUniformType type)
    {
        return QSharedPointer<VGLUniform<T>>::create(name, shader, type);
    }

    VGLUniform(const QString& name, VShaderProgram& shader, VUniformType type)
        : VIUniform(name, type)
        , m_shader(&shader)
    {
        m_uniformLocation = m_shader->uniformLocation(name);
    }

    void setValue(const T& type)
    {
        Q_CHECK_PTR(m_shader);
        m_shader->setUniformValue(m_uniformLocation, type);
    }
private:
    VShaderProgram* m_shader;
    int m_uniformLocation;
};

template <>
class VGLUniform<VVector3F> : public VIUniform
{
public:
    static QSharedPointer<VIUniform> instance(const QString& name, VShaderProgram& shader, VUniformType type)
    {
        return QSharedPointer<VGLUniform<VVector3F>>::create(name, shader, type);
    }

    VGLUniform(const QString& name, VShaderProgram& shader, VUniformType type)
        : VIUniform(name, type)
        , m_shader(&shader)
    {
        m_uniformLocation = m_shader->uniformLocation(name);
    }

    void setValue(const VVector3F& value)
    {
        Q_CHECK_PTR(m_shader);
        m_shader->setUniformValue(m_uniformLocation, value.toQVector3D());
    }
private:
    VShaderProgram* m_shader;
    int m_uniformLocation;
};

template <>
class VGLUniform<VMatrix4F> : public VIUniform
{
public:
    static QSharedPointer<VIUniform> instance(const QString& name, VShaderProgram& shader, VUniformType type)
    {
        return QSharedPointer<VGLUniform<VVector3F>>::create(name, shader, type);
    }

    VGLUniform(const QString& name, VShaderProgram& shader, VUniformType type)
        : VIUniform(name, type)
        , m_shader(&shader)
    {
        m_uniformLocation = m_shader->uniformLocation(name);
    }

    void setValue(const VMatrix4F& value)
    {
        Q_CHECK_PTR(m_shader);
        m_shader->setUniformValue(m_uniformLocation, value.toQMatrix4x4());
    }
private:
    VShaderProgram* m_shader;
    int m_uniformLocation;
};


using UniformFloatGL = VGLUniform<float>;
using UniformFloatVec3GL = VGLUniform<VVector3F>;
using UniformFloatVec4GL = VGLUniform<QVector4D>;
using UniformFloatMat4GL = VGLUniform<VMatrix4F>;
using UniformIntGL = VGLUniform<int>;

#endif // VIUNIFORM_H
