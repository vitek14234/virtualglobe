#ifndef VSHADERVERTEXATTRIBUTE_H
#define VSHADERVERTEXATTRIBUTE_H

#include <QString>
#include <venmus.h>

class VShaderVertexAttribute
{
public:
    VShaderVertexAttribute();
    VShaderVertexAttribute(const QString& name,
                           int location,
                           VShaderVertexAttributeType type,
                           int length);
    VShaderVertexAttribute(const VShaderVertexAttribute &rhs);

    const QString &name() const;
    int location() const;
    VShaderVertexAttributeType datatype() const;
    int length() const;
private:
    QString m_name;
    int m_location;
    VShaderVertexAttributeType m_type;
    int m_length;
};

#endif // VSHADERVERTEXATTRIBUTE_H
