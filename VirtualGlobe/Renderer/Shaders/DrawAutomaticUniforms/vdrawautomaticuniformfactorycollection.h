#ifndef VDRAWAUTOMATICUNIFORMFACTORYCOLLECTION_H
#define VDRAWAUTOMATICUNIFORMFACTORYCOLLECTION_H

#include <vpointers.h>

class VDrawAutomaticUniformFactoryCollection : public QHash<QString, VDrawAutomaticUniformFactoryPtr>
{
};

#endif // VDRAWAUTOMATICUNIFORMFACTORYCOLLECTION_H
