#ifndef VDRAWAUTOMATICUNIFORMFACTORY_H
#define VDRAWAUTOMATICUNIFORMFACTORY_H

#include <vpointers.h>
#include <Renderer/Shaders/DrawAutomaticUniforms/vdrawautomaticuniforms.h>

class VDrawAutomaticUniformFactory
{
public:
    virtual VDrawAutomaticUniformPtr create(const VIUniformPtr& uniform) = 0;
};

template <class T>
class VDrawAutomaticUniformFactoryImpl : public VDrawAutomaticUniformFactory
{
public:
    static VDrawAutomaticUniformFactoryPtr instance()
    {
        return QSharedPointer<VDrawAutomaticUniformFactoryImpl<T>>::create();
    }

    virtual VDrawAutomaticUniformPtr create(const VIUniformPtr& uniform) Q_DECL_OVERRIDE
    {
        return T::instance(uniform);
    }
};

using VCameraEyeUniformFactory = VDrawAutomaticUniformFactoryImpl<VCameraEyeUniform>;
using VSunPositionUniformFactory = VDrawAutomaticUniformFactoryImpl<VSunPositionUniform>;
using VModelViewPerspectiveMatrixUniformFactory = VDrawAutomaticUniformFactoryImpl<VModelViewPerspectiveMatrixUniform>;
using VLightPropertiesUniformFactory = VDrawAutomaticUniformFactoryImpl<VLightPropertiesUniform>;

#endif // VDRAWAUTOMATICUNIFORMFACTORY_H
