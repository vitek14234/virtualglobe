#ifndef VDRAWAUTOMATICUNIFORMS_H
#define VDRAWAUTOMATICUNIFORMS_H

#include "vpointers.h"
#include <Renderer/Scene/vscenestate.h>
#include <Renderer/Shaders/viuniform.h>

class VDrawAutomaticUniform
{
public:
    virtual void set(const VDrawingContextPtr& context, const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState) = 0;
};

class VCameraEyeUniform : public VDrawAutomaticUniform
{
public:
    //og_cameraEye
    static QSharedPointer<VDrawAutomaticUniform> instance(const VIUniformPtr& uniform)
    {
        return QSharedPointer<VCameraEyeUniform>::create(uniform);
    }

    VCameraEyeUniform(const VIUniformPtr& uniform)
        : m_uniform(uniform)
    {

    }

    virtual void set(const VDrawingContextPtr& context, const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState) Q_DECL_OVERRIDE
    {
        static_cast<UniformFloatVec3GL*>(m_uniform.data())->setValue(sceneState->camera().eye().toQVector3D());
    }
private:
    VIUniformPtr m_uniform;
};

class VSunPositionUniform : public VDrawAutomaticUniform
{
public:
    //og_sunPosition
    static QSharedPointer<VDrawAutomaticUniform> instance(const VIUniformPtr& uniform)
    {
        return QSharedPointer<VSunPositionUniform>::create(uniform);
    }

    VSunPositionUniform(const VIUniformPtr& uniform)
        : m_uniform(uniform)
    {

    }

    virtual void set(const VDrawingContextPtr& context, const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState) Q_DECL_OVERRIDE
    {
        static_cast<UniformFloatVec3GL*>(m_uniform.data())->setValue(sceneState->sunPosition().toQVector3D());
    }
private:
    VIUniformPtr m_uniform;
};

class VModelViewPerspectiveMatrixUniform : public VDrawAutomaticUniform
{
public:
    //og_sunPosition
    static QSharedPointer<VModelViewPerspectiveMatrixUniform> instance(const VIUniformPtr& uniform)
    {
        return QSharedPointer<VModelViewPerspectiveMatrixUniform>::create(uniform);
    }

    VModelViewPerspectiveMatrixUniform(const VIUniformPtr& uniform)
        : m_uniform(uniform)
    {

    }

    virtual void set(const VDrawingContextPtr& context, const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState) Q_DECL_OVERRIDE
    {
        static_cast<UniformFloatMat4GL*>(m_uniform.data())->setValue(VMatrix4F::fromMatrix(sceneState->modelViewPerspectiveMatrix()));
    }
private:
    VIUniformPtr m_uniform;
};

class VLightPropertiesUniform : public VDrawAutomaticUniform
{
public:
    //og_sunPosition
    static QSharedPointer<VLightPropertiesUniform> instance(const VIUniformPtr& uniform)
    {
        return QSharedPointer<VLightPropertiesUniform>::create(uniform);
    }

    VLightPropertiesUniform(const VIUniformPtr& uniform)
        : m_uniform(uniform)
    {

    }

    virtual void set(const VDrawingContextPtr& context, const VDrawStatePtr& drawState, const VSceneStatePtr& sceneState) Q_DECL_OVERRIDE
    {
        static_cast<UniformFloatVec4GL*>(m_uniform.data())->setValue(
                    QVector4D(sceneState->diffuseIntensity(),
                              sceneState->specularIntensity(),
                              sceneState->ambientIntensity(),
                              sceneState->shininess())
                    );
    }
private:
    VIUniformPtr m_uniform;
};

#endif // VDRAWAUTOMATICUNIFORMS_H
