#include "vglfactory.h"

#include "Renderer/Shaders/vshaderprogram.h"
#include <QOpenGLFunctions>

VShaderProgramPtr VGLFactory::createShader(const QString &vertexShaderSource, const QString &geometryShaderSource, const QString &fragmentShaderSource)
{
    return VShaderProgramPtr::create(vertexShaderSource, geometryShaderSource, fragmentShaderSource);
}

VShaderProgramPtr VGLFactory::createShader(const QString &vertexShaderSource, const QString &fragmentShaderSource)
{
    return VShaderProgramPtr::create(vertexShaderSource, fragmentShaderSource);
}

size_t VGLFactory::maximumNumberOfVertexAttributes()
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    int nrAttributes = 0;
    gl->glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
    return nrAttributes;
}
