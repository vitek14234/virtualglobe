#include "vvirtualglobe.h"

#include "Renderer/vglfactory.h"
#include "Renderer/vdevice.h"
#include "Renderer/Shaders/vuniform.h"
#include "Renderer/Scene/vscenestate.h"
#include "Renderer/Scene/vcameralookatpoint.h"
#include "Renderer/vclearstate.h"
#include "Renderer/vdrawstate.h"
#include "Renderer/DrawingContext/vdrawingcontext.h"
#include "Renderer/DrawingContext/Buffers/vindexbuffer.h"
#include "Renderer/DrawingContext/vtypeconvertergl.h"
#include "Renderer/Textures/vtextureunits.h"
#include "Renderer/Textures/vtexture2d.h"
#include "Renderer/VertexArray/vvertexarrayobject.h"

#include "Core/Geometry/vmesh.h"
#include "Core/Geometry/VertexAttributes/vvertexattribute.h"
#include "Core/Geometry/VertexAttributes/vvertexattributecollection.h"
#include "Core/Geometry/Indices/viindices.h"

#include "Core/Tessellation/vsubdivisionellipsoidtessellator.h"

#include "vutils.h"

#include <QImage>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QDebug>

VVirtualGlobe::VVirtualGlobe(QWidget *parent)
    : QOpenGLWidget(parent)
    , m_primitiveType(VPrimitiveType::Triangles)
{
    QSurfaceFormat format;
    format.setVersion(3, 3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
    format.setOption(QSurfaceFormat::DebugContext);
    setFormat(format);
}

void VVirtualGlobe::initializeGL()
{
    VDevice::init();
    m_sceneState = VSceneState::instance();
    m_clearState = VClearState::instance();
    m_drawingContext = VDrawingContext::instance(*this);

    m_shader = VGLFactory::createShader(VUtils::getFileSource(":/Imagery/Shaders/NightLights.vs"), VUtils::getFileSource(":/Imagery/Shaders/NightLights.fs"));
    m_shader->bind();
    const float blendDurationScale = 0.1f;
    VUniform<float>("u_blendDuration", *m_shader.get()).setValue(blendDurationScale);
    VUniform<float>("u_blendDurationScale", *m_shader.get()).setValue(1 / (2 * blendDurationScale));

    VMeshPtr mesh = VSubdivisionEllipsoidTessellator::compute(VEllipsoid::scaledWgs84(), 5, VSubdivisionEllipsoidVertexAttributes::Position);
    m_vao = VDevice::createVertexArrayObject(mesh, m_shader->vertexAttributes(), QOpenGLBuffer::StaticDraw);

    VRenderStatePtr renderState = VRenderState::instance();
    renderState->facetCulling.frontFaceWindingOrder = mesh->frontFaceWindingOrder();


    m_drawState = VDrawState::instance(renderState, m_shader, m_vao);

    m_dayTexture = VDevice::createTexture(QImage(":/Imagery/Imagery/world.topo.200412.3x5400x2700.jpg"));
    m_nightTexture = VDevice::createTexture(QImage(":/Imagery/Imagery/land_ocean_ice_lights_2048.jpg"));

    m_sceneState->setDiffuseIntensity(0.5f);
    m_sceneState->setSpecularIntensity(0.15f);
    m_sceneState->setAmbientIntensity(0.35f);

    m_sceneState->camera().zoomToTarget(1);
   // m_sceneState->camera().setEye(VVector3D(0.0, 0.0, 3.86));
   // m_sceneState->camera().setUp(VVector3D(0.0, 1.0, 0.0));

    m_shader->release();

    m_camera = VCameraLookAtPoint::instance(this, m_sceneState->camera(), VEllipsoid::scaledWgs84());

    m_logger = new QOpenGLDebugLogger( this );
    connect( m_logger, SIGNAL( messageLogged( QOpenGLDebugMessage ) ),
             this, SLOT( onMessageLogged( QOpenGLDebugMessage ) ),
             Qt::DirectConnection );
    if ( m_logger->initialize() ) {
        m_logger->startLogging( QOpenGLDebugLogger::SynchronousLogging );
        m_logger->enableMessages();
    }
}

void VVirtualGlobe::resizeGL(int w, int h)
{
    Q_ASSERT(m_drawingContext);
    if (m_drawingContext)
        m_drawingContext->setViewport(QRect(0, 0, w, h));

    Q_ASSERT(m_sceneState);
    if (m_sceneState)
        m_sceneState->camera().setAspectRatio(double(w) / h);
}

void VVirtualGlobe::paintGL()
{
    Q_ASSERT(m_drawingContext);
    if (!m_drawingContext)
        return;

    m_drawingContext->clear(m_clearState);
    m_drawingContext->textureUnits()->textureUnits()[0] = m_dayTexture;
    m_drawingContext->textureUnits()->textureUnits()[1] = m_nightTexture;
    m_drawingContext->draw2(m_primitiveType, m_drawState, m_sceneState);
}

void VVirtualGlobe::mouseMoveEvent(QMouseEvent *ev)
{
    m_camera->mouseMoveEvent(ev);
    QOpenGLWidget::mouseMoveEvent(ev);
    update();
}

void VVirtualGlobe::mousePressEvent(QMouseEvent *ev)
{
    m_camera->mousePressEvent(ev);
    QOpenGLWidget::mousePressEvent(ev);
    update();
}

void VVirtualGlobe::mouseReleaseEvent(QMouseEvent *ev)
{
    m_camera->mouseReleaseEvent(ev);
    QOpenGLWidget::mouseReleaseEvent(ev);
    update();
}

void VVirtualGlobe::onMessageLogged(QOpenGLDebugMessage message)
{
    qDebug() << message;
}
