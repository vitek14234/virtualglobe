#ifndef VVIRTUALGLOBE_H
#define VVIRTUALGLOBE_H

#include <QOpenGLWidget>
#include <QOpenGLDebugLogger>
#include "venmus.h"
#include "vpointers.h"

class VVirtualGlobe : public QOpenGLWidget
{
    Q_OBJECT
public:
    VVirtualGlobe(QWidget* parent = Q_NULLPTR);

protected:
    //QOpenGLWidget
    virtual void initializeGL() Q_DECL_OVERRIDE;
    virtual void resizeGL(int w, int h) Q_DECL_OVERRIDE;
    virtual void paintGL() Q_DECL_OVERRIDE;
    virtual void mouseMoveEvent(QMouseEvent *ev) Q_DECL_OVERRIDE;
    virtual void mousePressEvent(QMouseEvent *ev) Q_DECL_OVERRIDE;
    virtual void mouseReleaseEvent(QMouseEvent *ev) Q_DECL_OVERRIDE;
private slots:
    void onMessageLogged( QOpenGLDebugMessage message );
private:
    VSceneStatePtr m_sceneState;
    VClearStatePtr m_clearState;
    VDrawingContextPtr m_drawingContext;
    VTexture2DPtr m_dayTexture;
    VTexture2DPtr m_nightTexture;
    VDrawStatePtr m_drawState;
    VPrimitiveType m_primitiveType;
    QOpenGLDebugLogger* m_logger = Q_NULLPTR;
    VVertexArrayObjectPtr m_vao;
    VShaderProgramPtr m_shader;
    VCameraLookAtPointPtr m_camera;
};

#endif // VVIRTUALGLOBE_H
