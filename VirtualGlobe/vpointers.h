#ifndef VPOINTERS_H
#define VPOINTERS_H

#include <QSharedPointer>

using VMeshPtr = QSharedPointer<class VMesh>;
using VIVertexAttributePtr = QSharedPointer<class VIVertexAttribute>;
using VVertexAttributeCollectionPtr = QSharedPointer<class VVertexAttributeCollection>;
using VIIndicesPtr = QSharedPointer<class VIIndices>;
using VShaderProgramPtr = QSharedPointer<class VShaderProgram>;
using VSceneStatePtr = QSharedPointer<class VSceneState>;
using VDrawingContextPtr = QSharedPointer<class VDrawingContext>;
using VClearStatePtr = QSharedPointer<class VClearState>;
using VDrawStatePtr = QSharedPointer<class VDrawState>;
using VRenderStatePtr = QSharedPointer<class VRenderState>;
using VVertexArrayPtr = QSharedPointer<class VVertexArray>;
using VVertexArrayObjectPtr = QSharedPointer<class VVertexArrayObject>;
using VMeshBuffersPtr = QSharedPointer<class VMeshBuffers>;
using VVertexBufferPtr = QSharedPointer<class VVertexBuffer>;
using VIndexBufferPtr = QSharedPointer<class VIndexBuffer>;
using VVertexBufferAttributePtr = QSharedPointer<class VVertexBufferAttribute>;
using VVertexBufferAttributesPtr = QSharedPointer<class VVertexBufferAttributes>;
using VMeshVertexBufferAttributesPtr = QSharedPointer<class VMeshVertexBufferAttributes>;
using VTexture2DPtr = QSharedPointer<class VTexture2D>;
using VTextureUnitsPtr = QSharedPointer<class VTextureUnits>;
using VIUniformPtr = QSharedPointer<class VIUniform>;
using VLinkAutomaticUniformPtr = QSharedPointer<class VLinkAutomaticUniform>;
using VDrawAutomaticUniformPtr = QSharedPointer<class VDrawAutomaticUniform>;
using VDrawAutomaticUniformFactoryPtr = QSharedPointer<class VDrawAutomaticUniformFactory>;
using VCameraLookAtPointPtr = QSharedPointer<class VCameraLookAtPoint>;

#endif // VPOINTERS_H
